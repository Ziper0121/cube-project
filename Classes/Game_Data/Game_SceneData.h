#pragma once
#include<stdio.h>
#include "Game_Tools\Manager\LanguageManager.h"
#include "Game_Data\Game_Specimen_List.h"

enum SceneInterface {
	StarScene, ConScene, SetScene, TileScene,SaveScene,TalkScene, InforScene,ComputerScene,CubeGameScene
};


//플레이어의 씬의 정보를 저장한 클래스
struct Player_SceneData {

	//현재 씬위치
	SceneInterface pl_currentScene = SceneInterface::StarScene;

	//저장된 게임씬 정보
	GameMapPlace pl_stageMap = GameMapPlace::TestMap;
};
/*
std::string GetTileMapName(GameMapPlace MapKind) {
	std::string PlanName;
	switch (MapKind)
	{
	case Plan2TeamOffice:
		PlanName = TMX_ROUTE"Planning2Team.tmx";
		break;
	case Balcony:
		PlanName = TMX_ROUTE"Balcony.tmx";
		break;
	case TestMap:
		PlanName = TMX_ROUTE"TestOffice.tmx";
		break;
	default:
		break;
	}
	return PlanName;
}
*/
//출력할 타일맵을 선택한다.
std::vector<std::string> GetTileMapList(
	bool ReturnAll, GameMapPlace MapKind, std::string MapJsonName = "\0");