#include "Game_Data\Game_MapData.h"
#include "Game_Data\Game_MapData.h"

MapController* MapController::Instance = nullptr;

/*
Map_Data::Map_Data(int uniNum, const char* MapName, const GameMapPlace Place) {

	Unique_Number = uniNum;
	Name = MapName;
	switch (Place)
	{
	case GameMapPlace::Plan2TeamOffice:
		Body = TMX_ROUTE"AllPlanningOffice.tmx";
		break;
	case GameMapPlace::Balcony:
		Body = TMX_ROUTE"TestOffice.tmx";
		break;
	case GameMapPlace::TestMap:
		Body = TMX_ROUTE"TestOffice.tmx";
		break;
	default:
		break;
	}

	TileMap = TMXTiledMap::create(Body);
	TileMap->retain();
	MetaInfoLayer = TileMap->getLayer("MetaInfo");
	MapObjects = TileMap->getObjectGroup("Objects")->getObjects();

}
*/
/*
bool Map_Data::operator = (const GameMapPlace Place) {

	switch (Place)
	{
	case GameMapPlace::Plan2TeamOffice:
		Name = "AllPlanningOffice";
		break;
	case GameMapPlace::Balcony:
		Name = "TestOffice";
		break;
	case GameMapPlace::TestMap:
		Name = "TestOffice";
		break;
	default:
		break;
	}

	Unique_Number = (int)Place;
	Body = TMX_ROUTE + Name + "tmx";

	TileMap = TMXTiledMap::create(Body);
	TileMap->retain();
	MetaInfoLayer = TileMap->getLayer("MetaInfo");
	MapObjects = TileMap->getObjectGroup("Objects")->getObjects();

}
*/
/*
Map_Data::Map_Data(int uniNum, const char* MapName, const GameMapPlace Place) {
	
	Unique_Number = uniNum;
	Name = MapName;
	switch (Place)
	{
	case GameMapPlace::Plan2TeamOffice:
		Body = TMX_ROUTE"AllPlanningOffice.tmx";
		break;
	case GameMapPlace::Balcony:
		Body = TMX_ROUTE"TestOffice.tmx";
		break;
	case GameMapPlace::TestMap:
		Body = TMX_ROUTE"TestOffice.tmx";
		break;
	default:
		break;
	}

	TileMap = TMXTiledMap::create(Body);
	TileMap->retain();
	MetaInfoLayer = TileMap->getLayer("MetaInfo");
	MapObjects = TileMap->getObjectGroup("Objects")->getObjects();

}
*/

Map_Data MapController::m_settingMap(const char* MapName, const GameMapPlace Place) {
	Map_Data ReData;
	ReData.Unique_Number = (int)Place;
	ReData.Name = MapName;
	switch (Place)
	{
	case GameMapPlace::Plan2TeamOffice:
		ReData.Body = TMX_ROUTE"AllPlanningOffice.tmx";
		break;
	case GameMapPlace::Balcony:
		ReData.Body = TMX_ROUTE"TestFloor.tmx";
		break;
	case GameMapPlace::TestMap:
		ReData.Body = TMX_ROUTE"TestOffice.tmx";
		break;
	default:
		break;
	}

	ReData.TileMap = TMXTiledMap::create(ReData.Body);
	ReData.TileMap->retain();
	ReData.MetaInfoLayer = ReData.TileMap->getLayer("MetaInfo");
	ReData.MapObjects = ReData.TileMap->getObjectGroup("Objects")->getObjects();

	return ReData;
}

