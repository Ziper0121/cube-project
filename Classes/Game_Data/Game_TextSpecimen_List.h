#pragma once

#include "Game_Data\Game_Specimen_List.h" 

//대화할때의 정보들
struct Messege_Talk {

	//대화내용
	std::string TalkText = "\0";

	//대화하는 오브젝트의 얼굴
	std::string SayObjectTexture = "\0";

	//얼굴배치방향
	FaceEncho SayObjectEncho = FaceEncho::LeftFace;

};

//한번의 대화의 종류들
struct TalkWindow_Kind : public Messege_Talk {

	//버튼 유무
	bool OnButton = false;

	//얼굴표정
	EmotionKind SayObjectEmotion = EmotionKind::NoEmotion;
};

enum MessengerFrienKind {
	Cube_Intern, Computer_Manager
};
struct MessengerFrien : public Game_Item {
	MessengerFrienKind FriendKind;
};

// 메신저 대화방
struct MessengerRoom {
	std::vector<Messege_Talk> RoomTalk;
};