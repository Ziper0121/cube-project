
#include "Game_Data\Game_SceneData.h"
#include "Game_Data\Player_Data.h"

//출력할 타일맵을 선택한다.
std::vector<std::string> GetTileMapList(
	bool ReturnAll, GameMapPlace MapKind, std::string MapJsonName) {
	std::vector<std::string> ReturnString;

	//GetMapFileName을 ListfileName으로 바꾸자
	Document MapListPasing;
	if (MapJsonName.empty())
		MapListPasing = GetPaserDocument(GetMapFileName(MapKind));
	else
		MapListPasing = GetPaserDocument(MapJsonName.c_str());


	if (MapListPasing.HasParseError()) {
		CCLOG("Pasing Error Code%d", MapListPasing.GetParseError());
		assert(false);
	}
	auto MapListArray = MapListPasing["MapList"].GetArray();

	//이동 조건에 맞는 맵을 찾는다.
	for (auto i = MapListArray.begin(); i != MapListArray.end(); i++) {
		Quest_Limited MapLimited;
		if (ReturnAll) {
			ReturnString.push_back((*i)["Body"].GetString());
			continue;
		}
		if ((*i)["Limited"].Empty()) {
			if (ReturnString.size() == 0)
				ReturnString.push_back((*i)["Body"].GetString());
			ReturnString.at(0) = (*i)["Body"].GetString();
			continue;
		}
		for (auto k = (*i)["Limited"].Begin(); k < (*i)["Limited"].End(); k++) {
			for (auto m = (*k)["Ago_Quest"].Begin(); m < (*k)["Ago_Quest"].Begin(); m++) {
				NpcQuest_Num QuestNum;
				QuestNum.Npc = (*k)["Npc"].GetInt();
				QuestNum.Quest = (*k)["Quest"].GetInt();
				QuestNum.QuestFile = (*k)["QuestFile"].GetString();
				MapLimited.Ago_Quest.push_back(QuestNum);
			}
			for (auto m = (*k)["Item"].GetArray().Begin();
				m < (*k)["Item"].GetArray().end(); m++) {
				MapLimited.Item.push_back((*m).GetInt());
			}
			MapLimited.Rank = (Rank_List)(*k)["Rank"].GetInt();
		}
		if (!CheckLimitidFun(MapLimited)) {
			if (ReturnString.size() == 0)
				ReturnString.push_back((*i)["Body"].GetString());
			ReturnString.at(0) = (*i)["Body"].GetString();
		}

	}
	return ReturnString;
}