#pragma once
#include <cstdio>
#include <iostream>
#include <map>

#include "Game_Library\Game_FileRoute.h" 
#include "Game_Data\Game_Specimen_List.h" 
#include "Game_Data\Game_SceneData.h" 

#define TMX_TAG 1

using namespace cocos2d;

struct Map_Data : Game_Object {
public:

	//Map_Data(int uniNum, const char* MapName, const GameMapPlace Place);
	//bool operator = (const GameMapPlace Place);
	
	//타일 맵
	TMXTiledMap* TileMap;
	//충돌처리 레이어
	TMXLayer* MetaInfoLayer;
	//오브젝트들
	ValueVector MapObjects;

};

class MapController {
public:
	static MapController* createMapController() {
		if (Instance == nullptr) {
			Instance = new MapController;
		}
		return Instance;
	}
	
	std::vector<TMXTiledMap*> getAllTileMap(){
		std::vector<TMXTiledMap*> ReturnTileMap;
		for (auto i = m_allMap.begin(); i != m_allMap.end(); i++){
			ReturnTileMap.push_back((*i).second.TileMap);
		}
		return ReturnTileMap;
	}

	TMXTiledMap* getTileMap(GameMapPlace Place) { return m_allMap[Place].TileMap; };

	TMXLayer* getTileLayer(GameMapPlace Place) { return m_allMap[Place].MetaInfoLayer; };

	ValueVector getTileObject(GameMapPlace Place) { return m_allMap[Place].MapObjects; };
private:
	Map_Data m_settingMap(const char* MapName, const GameMapPlace Place);

	MapController() {
		m_allMap = 
		{{GameMapPlace::Plan2TeamOffice	
			, m_settingMap("Plan2TeamOffice", GameMapPlace::Plan2TeamOffice)},
			{ GameMapPlace::Balcony
			, m_settingMap("Balcony", GameMapPlace::Balcony) },
			{ GameMapPlace::TestMap
			, m_settingMap("TestMap", GameMapPlace::TestMap) }
		};
		for (auto i = m_allMap.begin(); i != m_allMap.end(); i++)
		{
			auto T_Map = i->second.TileMap;
			T_Map->setName((*i).second.Name);
			T_Map->setVisible(false);
			i->second.MetaInfoLayer = T_Map->getLayer("MetaInfo");
			i->second.MapObjects = T_Map->getObjectGroup("Objects")->getObjects();

			int num = 1;
			for (auto i = T_Map->getChildren().begin(); i < T_Map->getChildren().end(); i++) {
				(*i)->setZOrder(-1);
				std::string UpLayerName = "UpLayer" + std::to_string(num);
				if (dynamic_cast<TMXLayer*>(*i)->getLayerName().compare(UpLayerName) == 0) {
					(*i)->setZOrder(1);
					num++;
					CCLOG("Tmap Cound");
				}
			}
			for (int i = 0; i < T_Map->getLayerNum(); i++) {
				T_Map->getChildren().at(i)->setGLProgram(GLProgram::createWithByteArrays(
					ccPositionTextureColor_vert, ccPositionTexture_GrayScale_frag));
			}

		}
	}

	static MapController* Instance;

private:
	std::map<GameMapPlace, Map_Data> m_allMap;
};

/*

AllMap_Data.insert(make_pair(GameMapPlace::Balcony
, CreateMapData(1, "Balcony", GameMapPlace::Balcony)));
AllMap_Data.insert(make_pair(GameMapPlace::Plan2TeamOffice
, CreateMapData(2, "Plan2TeamOffice", GameMapPlace::Plan2TeamOffice)));
AllMap_Data.insert(make_pair(GameMapPlace::TestMap
, CreateMapData(3, "TestMap", GameMapPlace::TestMap)));

*/