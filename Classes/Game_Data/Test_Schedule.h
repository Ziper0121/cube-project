#pragma once

#include "Game_Data\Game_Specimen_List.h"

//Npc의 우선순위
enum CharPreference { Social, Love, Work };

/*
캐릭터의 움직임 상태를 몇개로 나눈다.
대화 : 인사, 대화
물건전달 : 가지고 오기, 가지고 가기
일 : 일하는중, 받은일, 일전달하기, 
*/

//NPC가 움직이는 이유
enum WorkKind { NoWork, SmallTalk, Bringing, Delivery, CompanyWork };

/*
물건을 전달할때 필요한 사람은 자신이 직접가던가 근처 누군가에게 부탁을한다.

*/

//부탁과 수락
enum TaskKind{ Request, Execution};

//그 사람이 관심있는 주제
enum TolpicKind { NOTolpic, WorkTolpic, LoveTolpic };

//게임에 Char들
enum CharKind { NOChar, Npc1, Npc2 };

//난이도 
enum WorkLevelKind { VeryEase,Ease,Nomal, Hard, VeryHard};

//시간
struct Time {
	int Hour;
	int Minutes;

	bool operator >(const Time& compare) {
		if (Hour == compare.Hour)
		{
			return Minutes > compare.Minutes;
		}
		return Hour > compare.Hour;
	}
	bool operator <(const Time& compare) {
		if (Hour == compare.Hour)
		{
			return Minutes < compare.Minutes;
		}
		return Hour < compare.Hour;
	}
	bool operator ==(const Time& compare) {
		return Hour == compare.Hour && Minutes == compare.Minutes;
	}
};

/*
npc가 해야하는 일들
전달, 가져오기, 일


npc가 가지고있는 정보
어떤 npc가 이런일을 하고있었다.
어디로 가고있었다.
누가 가지고있다.
*/

//캐릭터의 정보와 일정표의 표본
struct SixPrinciples {

	//누가
	CharKind Who;

	//언제
	Time When;

	//어디서
	GameMapPlace Where;

	//무엇을
	WorkKind What;

	//어떻게
	TaskKind How;

};

//해야하는 일들
struct ReceivedWord : public SixPrinciples {

	//일의 난이도
	WorkLevelKind WhatLevel;

	//건내줄 물건
	int WhatItem;

	//완수 정도
	int WhenEnd;
};

//목격장면 또는 
struct SightingScene : public SixPrinciples {
	
	//누구와 대화하였는가?
	CharKind With;

	//어떤 말을 들었는가?
	TolpicKind WhatSpeak;

};

