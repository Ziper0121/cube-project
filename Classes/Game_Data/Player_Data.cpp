﻿
#include "Game_Data\Player_Data.h" 



Game_Player* Game_Player::m_game_PlayerData = nullptr;

//player에 이름과 고유 번호를 초기화 
Game_Player::Game_Player() {
	playerSetting();
};
void Game_Player::playerSetting() {

	//Obeject값 초기화
	Unique_Number = 100;
	Body = PL_IMAGE;
	Name = PL_NAME;

	//움직임 초기화
	walk_Speed = 1;
}
void Game_Player::createPlayer() {
	//초기화를 먼저 하고 파일이 존재하면 덮어쓴다. 
	std::ifstream Put_GameData(PL_QUESTFILE, std::ios_base::in | std::ios_base::binary);
	if (Put_GameData.is_open() && Put_GameData.fail() ^ ::is_empty(Put_GameData))
		Put_GameData.read((char*)m_game_PlayerData, sizeof(*m_game_PlayerData));
	Put_GameData.close();
}

//--------------------------------파일함수--------------------------------

//재시작 함수
void Game_Player::replayerSetting() {

	pl_rank = Rank_List::Intern;

	//플레이어의 씬에 관한 정보
	pl_sceneData.pl_stageMap = GameMapPlace::Plan2TeamOffice;

	pl_item.clear();
	pl_file.clear();
	pl_messege.clear();
	pl_friend.clear();
	pl_icon.clear();
	pl_music.clear();

	playerSetting();

	GameMapPlace EnumMap[] = { Balcony ,Plan2TeamOffice};

	//NPC와 대화 기록을 초기화한다
	for (int i = 0; i < 2; i++) {
		auto FileNames = GetTileMapList(true, EnumMap[i]);
		for (auto k = FileNames.begin(); k < FileNames.end(); k++) {
			Document NPCParsing = GetPaserDocument(k->c_str());
			auto MapNpc = NPCParsing["MapNpc"].Begin();
			for (auto k = (*MapNpc)["Text"].Begin();
				k < (*MapNpc)["Text"].End(); k++)
				(*k)["Is_Talk"].SetBool(false);
			SaveDocument((*k).c_str(), NPCParsing);
		}
	}

	//미션클리어 기록을 초기화한다
	Document document = GetPaserDocument(MISSION_JSON);
	for (auto i = document["GameBusiness"].Begin(); i < document["GameBusiness"].End(); i++)
		(*i)["Clear"].SetBool(false);
	SaveDocument(MISSION_JSON, document);

	std::ifstream Put_GameData(PL_QUESTFILE);

	//캐릭터 파일이 없을경우 필요한 아이템을 준다
	if (!(fopen(PL_QUESTFILE, "r")) || Put_GameData.bad()) {
		addPLItem(GameObjectKind::ComputerIcon, 1);
	}
	else {
		Save_PlayerData();
	}


}

//세이브
void Game_Player::Save_PlayerData() {

	std::ofstream Put_GameData(PL_QUESTFILE, std::ios::binary);
	Put_GameData << Unique_Number << "\n";
	Put_GameData << Name << "\n";
	Put_GameData << Body << "\n";

	Put_GameData << walk_Speed << "\n";

	Put_GameData << pl_rank << "\n";
	Put_GameData << pl_sceneData.pl_stageMap << "\n";

	for (auto i = pl_item.begin(); i < pl_item.end(); i++) {
		Put_GameData << (*i).Name << "\n";
		Put_GameData << (*i).Unique_Number << "\n";
	}

	// 데이터간에 간격을 식별을 위해 한칸을 더 뛰어놓는다
	Put_GameData << "\n";
	//게임 내 컴퓨터 파일
	for (auto i = pl_file.begin(); i < pl_file.end(); i++) {
		Put_GameData << (*i).Name << "\n";
		Put_GameData << (*i).Unique_Number << "\n";
	}

	Put_GameData << "\n";
	for (auto i = pl_icon.begin(); i < pl_icon.end(); i++) {
		Put_GameData << (*i).Name << "\n";
		Put_GameData << (*i).Unique_Number << "\n";
	}

	Put_GameData << "\n";
	//메신저 친구목록
	for (auto i = pl_friend.begin(); i < pl_friend.end(); i++) {
		Put_GameData << (*i).FriendKind << "\n";
		Put_GameData << (*i).Unique_Number << "\n";
	}

	Put_GameData << "\n";
	//메신저 대화 방
	for (auto i = pl_messege.begin(); i < pl_messege.end(); i++) {
		for (auto k = (*i).RoomTalk.begin(); k < (*i).RoomTalk.end(); k++) {

			Put_GameData << (*k).SayObjectEncho << "\n";
			Put_GameData << (*k).SayObjectTexture << "\n";
			Put_GameData << (*k).TalkText << "\n";
		}
		Put_GameData << NEXT_TEXT << "\n";
	}

	Put_GameData << "\n";
	for (auto i = pl_music.begin(); i < pl_music.end(); i++) {

		Put_GameData << (*i).Name << "\n";
		Put_GameData << (*i).Unique_Number << "\n";
	}

	Put_GameData << "\n";
	//키보드 데이터 저장
	//m_gameSetting_Data.m_save_GameData();
	
	Put_GameData.close();

	//json데이터 저장

	//plclear은 직접 파일 주소를 저장한다.
	for (auto i = pl_clear.begin(); i < pl_clear.end(); i++) {
		const char* FileName = (*i).JsonFile.c_str();
		Document document = GetPaserDocument(FileName);
		switch ((*i).JsonKind)
		{
		case JsonKind::Map_Json: {
			auto NPCText = document["MapNpc"].Begin() 
				+ ((*i).JsonNumber.at(0) - 1);
			NPCText = (*NPCText)["Text"].Begin() + ((*i).JsonNumber.at(1) - 1);
			(*NPCText)["Is_Talk"].SetBool(true);
			break;
		}
		case JsonKind::Mission_Json: {
			auto ItemText = document["GameBusiness"].Begin() 
				+ ((*i).JsonNumber.back() - 1);
			(*ItemText)["Clear"].SetBool(true);
			break;
		}
		default:
			break;
		}
		SaveDocument(FileName, document);
	}
	//임시 저장소를 비워준다.
	pl_clear.clear();
}

//로드
bool Game_Player::m_loardPlayerData() {

	std::ifstream Put_GameData(PL_QUESTFILE);
	//Put_GameData.write((char*)m_game_PlayerData,sizeof(*m_game_PlayerData));

	//파일이 없을경우 오류
	if (!(fopen(PL_QUESTFILE,"r")) || Put_GameData.bad()) {
		CCLOG("File Error");
		return false;
	}
	
	//기본 정보들을 불러온다
	char CharNum[1000];
	Put_GameData.getline(CharNum, 1000);
	//파일이 비어있는지 확인한다.
	if (strcmp(CharNum,"") == 0)
		return false;
	Unique_Number = atoi(CharNum);

	Put_GameData.getline(CharNum, 1000);
	Name = CharNum;
	Put_GameData.getline(CharNum, 1000);
	Body= CharNum;

	Put_GameData.getline(CharNum, 1000);
	walk_Speed = atof(CharNum);
	
	Put_GameData.getline(CharNum, 1000);
	pl_rank = (Rank_List)atoi(CharNum);
	Put_GameData.getline(CharNum, 1000);
	pl_sceneData.pl_stageMap = (GameMapPlace)atoi(CharNum);
	
	pl_sceneData.pl_currentScene = SceneInterface::TileScene;

	//현재의 플레이어의 상태를 모두 지운다
	pl_item.clear() ;
	pl_file.clear();
	pl_messege.clear();
	pl_friend.clear();
	pl_icon.clear();
	pl_music.clear();

	//플레이어의 아이템이나 음악 등을 불러온다.
	//파일을 저장할때 이름부터 저장을 해서 Count를 해줘야한다 
	bool CountNum = false;
	while (true){
		Put_GameData.getline(CharNum, 1000);
		if (strcmp(CharNum,"") == 0)
			break;
		else if (!CountNum) {
			CountNum = true;
			continue;
		}

		pl_item.push_back(GetObjectItemData(atoi(CharNum)));
		CountNum = false;
	}

	while (true) {
		Put_GameData.getline(CharNum, 1000);
		if (strcmp(CharNum, "") == 0)
			break;
		else if (!CountNum) {
			CountNum = true;
			continue;
		}

		pl_file.push_back(GetAddFileData(atoi(CharNum)));
		CountNum = false;
	}

	while (true) {
		Put_GameData.getline(CharNum, 1000);
		if (strcmp(CharNum, "") == 0)
			break;
		else if (!CountNum) {
			CountNum = true;
			continue;
		}

		pl_icon.push_back(GetComputerIconData(atoi(CharNum)));
		CountNum = false;
	}

	while (true) {
		Put_GameData.getline(CharNum, 1000);
		if (strcmp(CharNum, "") == 0)
			break;
		MessengerFrien FriendData;
		FriendData.FriendKind = (MessengerFrienKind)atoi(CharNum);
		Put_GameData.getline(CharNum, 1000);
		FriendData.Unique_Number = atoi(CharNum);
		pl_friend.push_back(GetMessengerFrienData(
			FriendData.Unique_Number, FriendData.FriendKind));
	}

	//메세지 대화창 불러오기
	MessengerRoom TalkRoom;
	Messege_Talk Talking;
	int CountTalkNum = 0;
	while (true) {
		Put_GameData.getline(CharNum, 1000);

		if (strcmp(CharNum, NEXT_TEXT) == 0) {
			CountTalkNum = 0;
			pl_messege.push_back(TalkRoom);
			TalkRoom.RoomTalk.clear();
			continue;
		}
		else if (strcmp(CharNum, "") ==0)
			break;
		switch (CountTalkNum)
		{
		case 0:
			Talking.SayObjectEncho = (FaceEncho)atoi(CharNum);
			break;
		case 1:
			Talking.SayObjectTexture = CharNum;
			break;
		case 2:
			Talking.TalkText = CharNum;
			CountTalkNum = 0;
			continue;
		default:
			break;
		}
		CountTalkNum++;
	}

	while (true) {
		Put_GameData.getline(CharNum, 1000);
		if (strcmp(CharNum, "") == 0)
			break;
		else if (!CountNum) {
			CountNum = true;
			continue;
		}

		pl_music.push_back(GetMusicData(atoi(CharNum)));
		CountNum = false;
	}


	//m_gameSetting_Data.m_save_GameData();
	Put_GameData.close();

	return true;

}

//------------------------------더하는 함수------------------------------

//플레이어의 아이템 데이터 ItemGetArray[9]
void Game_Player::addPLItem(std::vector<RewardItem> GetItem) {
	for (auto i = GetItem.begin(); i < GetItem.end(); i++)
	addPLItem((*i).ItemKind, (*i).ItemNum);

}
void Game_Player::addPLItem(GameObjectKind ObjectKind, int ItemNum) {
	if (ItemNum == NULL)
		return;
	switch (ObjectKind)
	{
	case GameObjectKind::ObjectItem:
		if (pl_item.size() <25)
		pl_item.push_back(GetObjectItemData(ItemNum));
		break;
	case GameObjectKind::Cube_ExeFile:
		if (pl_file.size() <25)
			pl_file.push_back(GetAddFileData(ItemNum));
		break;
	case GameObjectKind::ComputerIcon:
		if (pl_icon.size() < 25)
			pl_icon.push_back(GetComputerIconData(ItemNum));
		break;
	default:
		break;
	}
	return;
}

//음악을 설정한다.
void Game_Player::addPLMusic(Game_Music MusicData) {
	pl_music.push_back(MusicData);
}
void Game_Player::addPLClear(Clear_Json ClearJson) {
	pl_clear.push_back(ClearJson);
}


//메신저를 만들때누느 메신저와 친구들 정보들을 최대치를 정해놓자.
//읽을 메시지를 저장한다.
void Game_Player::addMessege(int MessengerRoomNum
	, std::vector<Messege_Talk>& Messege) {
	
	if (pl_messege.size() <= MessengerRoomNum)
		pl_messege.resize(pl_messege.size());
	

	for (auto i = Messege.begin(); i < Messege.end(); i++)
		pl_messege.at(MessengerRoomNum).RoomTalk.push_back((*i));
}
void Game_Player::addMessegerFriend(MessengerFrienKind FriendKind, int FriendNum) {

	if (FriendNum == NULL)
		return;

	pl_friend.push_back(GetMessengerFrienData(FriendNum, FriendKind));
}

//플레이어 클리어 상황을 조건에 맞는 클리어 변수를 준다.
std::vector<Clear_Json> Game_Player::getHavClear(
	JsonKind Kind, const char* Name, int Num) {

	std::vector<Clear_Json> TalkClear;
	for (auto i = pl_clear.begin(); i < pl_clear.end(); i++) {
		if ((*i).JsonKind != Kind)continue;
		if ((*i).JsonFile.find(Name) == std::string::npos)continue;
		if ((*i).JsonNumber.at(0) != Num)continue;
		TalkClear.push_back(*i);
	}
	return TalkClear;
}

void Game_Player::setPlayMusicPoint(int num) {

	int MusicSize = pl_music.size();
	if (num >= MusicSize) {
		num = 0;
	}
	else if (num< 0) {
		num = MusicSize - 1;
	}

	pl_musicPoint = num;
}

//아이템을 사용한다.
void Game_Player::usePLItem(int ItemNum, bool RemoveItem) {
	ItemNum -= 1;
	//아이템을 사용할경우 없애자
	if (!RemoveItem)
		return;
	
	for (auto i = pl_item.begin(); i < pl_item.end(); i++) {
		//같은 아이템이 있는지 찾고 제거한다.
		if (ItemNum != (*i).Unique_Number)
			continue;
	
		//지속적으로 사용가능한 아이템일경우 소지하기있는다.
		Game_Item ChangeItem;
		(*i) = ChangeItem;

		//아이템배열을 정렬한다.
		for (auto k = i; k + 1< pl_item.end(); k++)
			*k = *(k + 1);
		pl_item.pop_back();
		return;
	}
}


//num에 해당하는 이벤트값을 준다.
void Game_Event::getTalkEvent(int num) {

	switch (num)
	{
	case TALK_SAVEEVENT:
		Game_Player::getInstance()->Save_PlayerData();
		break;
	default:
		break;
	}

}


bool is_empty(std::ifstream& pfile) {
	return pfile.peek() == std::ifstream::traits_type::eof();
}

char* GetMapFileName(GameMapPlace ChangeScene) {
	char* FileName;
	switch (ChangeScene)
	{
	case GameMapPlace::Plan2TeamOffice:
		FileName = PLANNIGOFFICE_MAP_JSON;
		break;
	case GameMapPlace::Balcony:
		FileName = BALCONY_MAP_JSON;
		break;
	default:
		FileName = PLANNIGOFFICE_MAP_JSON;//TEST_MAP_JSON;
		break;
	}
	return FileName;
}


Layout* AddItemBox(std::vector<Game_Object> objects) {

	Size WinSize = Game_Player::getInstance()->getGameSetting()->m_window_Size;

	ui::Layout* BoxLayout = ui::Layout::create();
	BoxLayout->setContentSize(Size(WinSize.width *0.3f, WinSize.height));
	BoxLayout->setAnchorPoint(Vec2::ANCHOR_BOTTOM_LEFT);
	BoxLayout->setGlobalZOrder(1.0f);
	BoxLayout->setLayoutType(Layout::Type::VERTICAL);
	BoxLayout->runAction(Sequence::create(DelayTime::create(2.0f), CallFuncN::create(
		std::bind([&](Node* node) {node->removeFromParent(); }, BoxLayout)), NULL));
	Director::getInstance()->getRunningScene()->addChild(BoxLayout);

	for (auto i = objects.begin(); i < objects.end(); i++) {
		Layout* ItemBox = Layout::create();
		ItemBox->setContentSize(
			Size(BoxLayout->getContentSize().width
				, BoxLayout->getContentSize().height * 0.1f));
		ItemBox->setBackGroundColorType(LayoutBackGroundColorType::SOLID);
		ItemBox->setLayoutType(Layout::Type::HORIZONTAL);
		ItemBox->setBackGroundColor(Color3B::GRAY);
		ItemBox->setLayoutType(Layout::Type::HORIZONTAL);
		BoxLayout->addChild(ItemBox);

		ui::ImageView* ItemImage = ui::ImageView::create((*i).Body);
		ItemBox->addChild(ItemImage);
		ItemImage->ignoreContentAdaptWithSize(false);
		ItemImage->setContentSize(
			Size(ItemBox->getContentSize().height
				, ItemBox->getContentSize().height));

		ui::Text* ItemName = ui::Text::create(
			(*i).Name, FONT_NAME, ItemBox->getContentSize().height * 0.5f);
		ItemName->setTextAreaSize(
			ItemBox->getContentSize() - ItemImage->getContentSize());
		ItemBox->addChild(ItemName);
	}
	return BoxLayout;
}

char* MapBackGroundMusic(GameMapPlace ChangeScene) {
	char* MusicName;
	switch (ChangeScene)
	{
	case Plan2TeamOffice:
		MusicName = BACKMUSIC_ROUTE"Heater.mp3";
		break;
	case Balcony:
		MusicName = BACKMUSIC_ROUTE"Keyboard.mp3";
		break;
	case TestMap:
		break;
	default:
		break;
	}
	return MusicName;
}

char* WalkingSound(GameMapPlace ChangeScene) {

	char* MusicName;
	switch (ChangeScene)
	{
	case Plan2TeamOffice:
		MusicName = EFFECT_ROUTE"Steps_OneTic.mp3";
		break;
	case Balcony:
		MusicName = EFFECT_ROUTE"CapetSteps_OneTic.mp3";
		break;
	case TestMap:
		break;
	default:
		break;
	}
	return MusicName;
}

//--------------------------------선행조건 함수--------------------------------

//아마 없앨것같다.
//선행조건에 걸리는지 확인하는 함수
bool CheckLimitidFun(Quest_Limited Limite) {

	bool Check_LimitedQuest = false;

	auto PlayerItem = Game_Player::getInstance()->getHavItem();

	//QusetLimited
	for (auto& i = Limite.Ago_Quest.begin(); i < Limite.Ago_Quest.end(); i++) {
		if (0 == (*i).Npc) {
			CCLOG("Max Quest_Limite");
			break;
		}
		//선행으로 대화해야하는 NPC가 있는지 확인한다.
		int NPCNum = (*i).Npc;
		const char* FileName = (*i).QuestFile.c_str();
		int QuestNum = (*i).Quest;

		//json에서 해당 대화 기록이있는지 확인한다
		std::vector<Clear_Json>TalkClear = Game_Player::getInstance()->
			getHavClear(JsonKind::Map_Json, FileName, NPCNum);

		//필요 퀘스트를 클리어했는가?
		if (0 < (*i).Npc) {
			Check_LimitedQuest = true;
			bool CheckClear = false;
			//대화를 하였는 내가 가진정보로 찾아본다.
			for (auto k = TalkClear.begin(); k < TalkClear.end(); k++) {
				if ((*k).JsonNumber.back() != QuestNum)continue;
				Check_LimitedQuest = false;
				CheckClear = true;
				break;
			}
			//대화를 하였는지 JSON에서 가지고 와 찾아본다.
			if (!CheckClear
				&& GetNPCData(NPCNum, FileName).Text.at(QuestNum - 1).is_Talk)
				Check_LimitedQuest = false;
		}
	}

	//ItemLimited
	for (auto& i = Limite.Item.begin(); i < Limite.Item.end(); i++) {

		//필요한 조건이 없는가?
		if (0 == (*i))
			break;
		//아이템있지 않을경우 for(K)루프 나가기위한 변수
		bool HaveItem = true;
		for (auto k = PlayerItem.begin(); k < PlayerItem.end(); k++) {
			if ((*i) != (*k).Unique_Number)
				continue;
			HaveItem = false;
			break;
		}
		//아이템 없을경우 for(K)루프 나가기
		if (HaveItem) {
			Check_LimitedQuest = true;
			break;
		}

	}
	//퀘스트에서 아이템을 없애버린다.
	for (auto& i = Limite.Item.begin();
		i< Limite.Item.end() && !Check_LimitedQuest; i++) {
		Game_Player::getInstance()->usePLItem((*i), true);
	}
	return Check_LimitedQuest;
}
