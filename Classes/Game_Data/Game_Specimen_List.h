#pragma once 
#include <cocos2d.h>

using namespace cocos2d;

struct TalkWindow_Kind;

//회사부서 종류 : 인사, 제무, 기획, 개발, 판매
enum GameMapPlace {
	Plan2TeamOffice = 0, Balcony, TestMap
};

//직급
enum Rank_List {
	Intern, Clerk, Senior_Clerk, Assistant_Manager, Manager, Deputy_General_Manager, General_Manager,
	Managing_Director, Execurive_Managing_Director, Vice_President, President
};

//아이템과 퀘스트의 등급
enum Object_Rating { Daily, Story, Hidden };

//아이템 종류
enum GameObjectKind { ObjectItem, Cube_ExeFile, ComputerIcon };


//캐릭터의 애니메이션 tga값
#define ANI_ACT 3
#define MOVE_ACT 2

//TileMap
#define TEXT_MAX 100
#define NAME_MAX 40
#define FAILL_NAME 50

//대화에 관련된 정보들
//감정
enum EmotionKind {NoEmotion =0, Angry, Happy, Sad, Doubt};
//얼굴 출력 위치
enum FaceEncho {LeftFace = 0,RightFace};

//음악의 종류
enum MusicKind { ALL,ExcitingMusic, MagnificentMusic,GrandMusic};

//퀘스트를 받는 방법
//NPC와 부딪힐경우 npc 번호를 받아서 npc번호에서 나의 상황과 맞는 퀘스트 정보를 받는다.
//npc를 상황에 맞게 배치해놓는다.
//퀘스트를 text와 보상과 퀘스트에 관련된 정보(이름, 번호)등을 가지고있는다.
//퀘스트와 대화와 나누는것을 원한다.
//퀘스트 수락과 퀘스트의 필요한조건

//퀘스트와 엔피씨의 번호
struct NpcQuest_Num {
	std::string QuestFile = "\0";
	int Npc = 0;
	int Quest = 0;
};

//선행조건
struct Quest_Limited {
	std::vector<NpcQuest_Num> Ago_Quest = std::vector<NpcQuest_Num>(0);
	Rank_List Rank = Rank_List::Intern;
	std::vector<int> Item = std::vector<int>(0);
};

//오브젝트의 종류를 나타는 오브젝트
struct ObjectKind {
	//begin번호이다
	int ItemNumber = 0;
	//Item의 종류
	GameObjectKind Kind = GameObjectKind::ObjectItem;
};
/*
Game_Item(
int UniNum, const char* ObjectName, const char* ObjectBody
, Object_Rating ItemRating, const char* ItemInfor, const bool ItemKeep)
: Game_Object(UniNum, ObjectName, ObjectBody)
, use(ItemRating),information(ItemInfor), Keep(ItemKeep){}
*/
//모든 캐릭터, 아이템에 관한 기본 오브젝트
struct Game_Object {
	//고유번호
	int Unique_Number = 0;
	//이름
	std::string Name= "\0";
	//sprit몸 파일이름
	std::string Body = "\0";

};

//음악
struct Game_Music : public Game_Object {
	std::string MusicFile = "\0";

	MusicKind Kind;
};

//아이템
struct Game_Item : public Game_Object{	
	//사용 등급
	Object_Rating use = Object_Rating::Daily;
	//설명 정보
	std::string information= "\0";

	bool Keep = false;

};

struct Game_AddFile : public Game_Item{

	//추가파일 정보들

	//추가파일 종류
	GameObjectKind AddFileKind = GameObjectKind::ObjectItem;

	//추가파일 번호
	int AddFileNum = 0;
};

/*
필요한 것이 대화
수락 거절
시간대 부탁, 지금 부탁, 
*/

//Char가 대화중에 전달되는 의견들
enum TalkKind{NoTalk,Question,Answer,Counter};

//기본 캐릭터
struct Game_Char: public Game_Object {
	
	Game_Char(int CharNum = 0, const char* CharName = "\0"
		, const char* CharBody = "\0"	, float CharSpeed = 1.0f) {
		Unique_Number = CharNum;
		Name = CharName;
		Body = CharBody;
		walk_Speed = CharSpeed;
	}
	//NPC의 Sprite몸
	Sprite* Char_Body;

	//움직이는 속도
	float walk_Speed = 0.0f;

	//Char가 가지고있는 아이템
	std::vector<Game_Item> Char_Item;
};


/*

퀘스트 번호
퀘스트 이름
시작 (npc, place,object)
종료 (..)
퀘스트를 받을수있는 직급
선행 퀘스트 여부 번호
보상[갯수]
퀘스트 내용

만들어야 할것
npc의 지역번호
*/
struct Game_Text {

	//---------------------퀘스트 정보---------------------//
	int unique_Number = 0;
	std::string name = "\0";

	bool is_Talk = false;

	//대화내용과 버튼의 유무를 가진 변수
	std::vector<TalkWindow_Kind> talkInteface;

	//선행 조건
	Quest_Limited limited;

	//---------------------보상---------------------//
	std::vector<ObjectKind > reward_Item = std::vector<ObjectKind >(0);
};

struct Test_Text {

	//---------------------퀘스트 정보---------------------//
	int unique_Number = 0;
	std::string name = "\0";

	bool is_Talk = false;

	//대화내용과 버튼의 유무를 가진 변수
	std::vector<EmotionKind> talkInteface;

	//지금 말하는 캐릭터
	std::vector<int> talkChar;

	//선행 조건
	Quest_Limited limited;

	//---------------------보상---------------------//
	std::vector<ObjectKind > reward_Item = std::vector<ObjectKind >(0);
};

//NPC의 맵에 배치되어있는 번호이다.
struct Game_NPC : public Game_Char {
	std::vector<Game_Text> Text;
};


//게임의 컷신에 관한 구조체
struct CutScene{
	
	//번호
	int unique_Number;
	//이름
	char* name;


};
