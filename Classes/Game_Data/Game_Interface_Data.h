#pragma once
#include <cocos2d.h>
#include <fstream>
#include <iostream>

#include "Game_Library\G_ConstLibrary\Interface_CCollection.h"	

/*
게임의 인터 페이스 값들을 가지고 있는 클래스이다.
데이터를 불러오고 저장하기도한다.

음악의 크기 설정
스크린 사이즈를 설정
키보드 설정
*/

//볼륨 최대치
#define SOUND_MAX 100

//볼륨의 퍼센트
#define VOLUME_PS 0.01

//***********파일이 손상되었을때를 대비하는 코드를 추가하자***********\\


using namespace cocos2d;
using namespace cocos2d::ui;

//지원 언어
enum PlayerLanguage { Korean, English };

//볼륨 및 옵션의 값들이 잘못저장되있을경우 다시 재설정한다
int CheckDelegate_Maximum(const char* DATA_NAME, const int MAXIMUM);

class Game_Interface {
public:

	Game_Interface();
	~Game_Interface() {};

	void m_save_GameData();

	//사용자 언어
	PlayerLanguage m_userLanguage = PlayerLanguage::Korean;

	//사운드 크기
	int m_background_Music = SOUND_MAX;
	int m_action_Sound = SOUND_MAX;
	int m_ui_Sound = SOUND_MAX;

	//윈도우 크기
	Size m_window_Size;
	//풀스크린
	bool m_fullScreen = false;

	//큐브를 잡는 키
	/*
	[1][*][*]
	[*][2][*]
	[*][*][3]
	*/


	EventKeyboard::KeyCode m_grab_Cube[3] = {
		EventKeyboard::KeyCode::KEY_Q
		,EventKeyboard::KeyCode::KEY_W
		,EventKeyboard::KeyCode::KEY_E };

	//정면의 큐브를 좌측이나 우측으로 돌리는 키
	/*1왼쪽회전   2우측회전
	[<][<][Λ]    [>][>][V]
	[V][*][Λ]    [Λ][*][V]
	[V][>][>]    [Λ][<][<]
	*/

	//캐릭터나 큐브 이동시키는 키
	//Right, Left, Bottom, Top
	EventKeyboard::KeyCode m_char_Cube_Move[4] = {
		EventKeyboard::KeyCode::KEY_RIGHT_ARROW
		,EventKeyboard::KeyCode::KEY_LEFT_ARROW
		,EventKeyboard::KeyCode::KEY_DOWN_ARROW
		,EventKeyboard::KeyCode::KEY_UP_ARROW
	};
};
