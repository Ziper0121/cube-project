
#include "Game_Library\Game_FileRoute.h" 
#include "Game_Data\Game_Interface_Data.h"

Game_Interface::Game_Interface() {
	auto UserData = UserDefault::getInstance();
	CCLOG("getXMLFilePath = %s", UserData->getXMLFilePath().c_str());
	if (UserData->isXMLFileExist() && UserData->getBoolForKey(FILE_EMPTY_TEXT)) {

		m_background_Music = CheckDelegate_Maximum(BACKGROUND_MUSIC_TEXT, SOUND_MAX);
		m_action_Sound = CheckDelegate_Maximum(ACTION_SOUND_TEXT, SOUND_MAX);
		m_ui_Sound = CheckDelegate_Maximum(UI_SOUND_TEXT, SOUND_MAX);

		//확인필요 업그라운을 하면 데이터 상태가 어떡해 되는지
		m_fullScreen = UserData->getBoolForKey(FULL_SCREEN_TEXT);

		//사용 언어
		m_userLanguage = (PlayerLanguage)UserData->getIntegerForKey(LANGUAGE_TEXT, PlayerLanguage::English);

		UserData->flush();
	}

	RECT rect;
	GetWindowRect(GetDesktopWindow(), &rect);

	MONITORINFOEX monitor;
	monitor.cbSize = sizeof(MONITORINFOEX);
	HMONITOR hmonitor = MonitorFromRect(&rect, MONITOR_DEFAULTTONEAREST);
	GetMonitorInfo(hmonitor, &monitor);

	m_window_Size.width = rect.bottom  *0.5f;
	m_window_Size.height = rect.bottom *0.5f;
	m_window_Size = Size(384, 384);
	GLViewImpl* view = (GLViewImpl*)Director::getInstance()->getOpenGLView();

	view->setWindowed(m_window_Size.width, m_window_Size.height);
	view->setDesignResolutionSize(
		m_window_Size.width, m_window_Size.height, ResolutionPolicy::SHOW_ALL);
	if (m_fullScreen) {
		view->setFullscreen();
	}
}
void Game_Interface::m_save_GameData() {
	auto UserData = UserDefault::getInstance();

	UserData->setBoolForKey(FILE_EMPTY_TEXT,true);

	UserData->setIntegerForKey(BACKGROUND_MUSIC_TEXT, m_background_Music);
	UserData->setIntegerForKey(ACTION_SOUND_TEXT, m_action_Sound);
	UserData->setIntegerForKey(UI_SOUND_TEXT, m_ui_Sound);

	UserData->setBoolForKey(FULL_SCREEN_TEXT, m_fullScreen);
	UserData->setIntegerForKey(LANGUAGE_TEXT, m_userLanguage);

	UserData->flush();
}


int CheckDelegate_Maximum(const char* DATA_NAME,const int MAXIMUM) {

	int Int_Delegate = UserDefault::getInstance()->getIntegerForKey(DATA_NAME);

	if (Int_Delegate < 0 && Int_Delegate > MAXIMUM) {
		UserDefault::getInstance()->setIntegerForKey(DATA_NAME, MAXIMUM);
		return MAXIMUM;
	}
	return Int_Delegate;
}
/*
0을 다른값으로 바꾸던가 
-1을 해서 집어넣을때는 1로

*/