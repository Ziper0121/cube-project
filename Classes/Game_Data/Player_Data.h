#pragma once

#include <fstream>
#include <iostream>
#include <json\document.h>
#include <json\istreamwrapper.h>
#include <ui\CocosGUI.h>
 
#include "Game_Tools\Manager\LanguageManager.h"
#include "Game_Library\G_ConstLibrary\PlayerData_CCollection.h"
#include "Game_Data\Game_Interface_Data.h"
#include "Game_Data\Game_TextSpecimen_List.h"
#include "Game_Data\Game_SceneData.h"
#include "Game_Library\G_ConstLibrary\Cube_CCollection.h"


#define NEXT_TEXT "_NEXT_"

using namespace cocos2d;
using namespace cocos2d::ui;

enum Computer_IconKind {
	Icon_Cube, Icon_Messenger
};
//플레이어의 
struct Game_ComputerIcon : public Game_Object{
	Computer_IconKind Icon_Kind;
};

//큐브 미션의 
struct RewardItem {
	GameObjectKind ItemKind;
	int ItemNum;
};

//플레이어의 큐브 미션
struct Cube_Misstion {
	std::vector<std::vector	<Mix_CubeMap>> Business;
	std::vector	<Mix_CubeMap> OriginalCube;
	bool BusinessClear = false;
	std::vector<RewardItem> Cube_Reward;
};

//json으로 저장하는 것의 종류
enum JsonKind { Map_Json, Mission_Json};
//
struct Clear_Json {
	JsonKind JsonKind;
	std::string JsonFile;
	std::vector<int> JsonNumber;
};

//싱글톤 플레이어 표본
class Game_Player : public Game_Char {
private:

	//초기화
	Game_Player();
	~Game_Player() {};

	static Game_Player* m_game_PlayerData;

	static void createPlayer();

	void playerSetting();

public:
	static Game_Player* getInstance() {
		if (m_game_PlayerData == nullptr) {
			m_game_PlayerData = new Game_Player();
			//createPlayer();
		}
		return m_game_PlayerData;
	};

	//--------------------------------파일함수--------------------------------

	//재시작
	void replayerSetting();

	//플레이어 데이터 저장
	void Save_PlayerData();

	//파일 로드
	bool m_loardPlayerData();

	//--------------------------------받는함수--------------------------------

	//플레이어의 저장된 진행상황을 받는 함수
	GameMapPlace getPlace() const { return pl_sceneData.pl_stageMap; };
	//플레이어 키보드값
	Game_Interface* getGameSetting() { return &m_gameSetting_Data; };
	//현재 화면값 받기
	Player_SceneData getSceneInterface() const { return pl_sceneData; };
	//플레이어의 직급을 받는다.
	Rank_List getPLRank() const { return pl_rank; };
	//플레이어의 메시지 내용을 받는다.
	std::vector<MessengerRoom> getMessege() const { return pl_messege; };
	//플레이어의 친구목록
	std::vector<MessengerFrien> getMessegerFreind() { return pl_friend; };
	
	//플레이어의 아이템 데이터 ItemGetArray[9]
	std::vector<Game_Item> getHavItem() { return pl_item; };
	//플레이어가 가지고있는 파일들의 데이터
	std::vector<Game_AddFile> getHavBusiness(){ return pl_file; };
	//플레이어가 가지고있는 파일들의 데이터
	std::vector<Game_ComputerIcon> getHavIcon() { return pl_icon; };
	//플레이어가 가지고있는 파일들의 데이터
	std::vector<Game_Music> getHavMusic() { return pl_music; };
	//플레이어 클리어 상황을 가지고있는 데이터
	std::vector<Clear_Json> getHavClear() { return pl_clear; };
	//플레이어 클리어 상황을 조건에 맞는 클리어 변수를 준다.
	std::vector<Clear_Json> getHavClear(
		JsonKind Kind,const char* Name, int Num);
	//지금 음악의 재생목록의 위치를 가지고 있는 데이터 
	void setPlayMusicPoint(int num);
	
	//플레이어의 포인터
	Point getMapPoint() { return pl_MapPoint; };
	//현재 틀어지는 음악 위치
	int getPlayMusicPoint(){ return pl_musicPoint;};
	//--------------------------------설정함수--------------------------------

	//플레이어의 포인터
	void setMapPoint(Point PlayerPoint) { pl_MapPoint = PlayerPoint; };
	//플레이어 진행상황을 변경하는 함수
	void setPlayerMap(GameMapPlace playerMap){ pl_sceneData.pl_stageMap = playerMap; };
		
	//플레이어 키보드값
	bool savePlayerKeySet(Game_Interface SaveInterfaceData){ return true; };
	//현재 화면값 바꾸기
	void setSceneInterface(SceneInterface setScene) {
		pl_sceneData.pl_currentScene = setScene; };

	//------------------------------더하는 함수------------------------------
	
	//많은 아이템 받는다
	void addPLItem(std::vector<RewardItem> GetItem);
	//하나의 아이템을 받는다
	void addPLItem(GameObjectKind ObjectKind, int ItemNum);
	//음악을 설정한다.
	void addPLMusic(Game_Music ClearJson);
	//클리어한 미션과 대화들 저장한다.
	void addPLClear(Clear_Json ClearJson);
	
	
	//읽을 메시지를 저장한다.
	void addMessege(int MessengerRoomNum, std::vector<Messege_Talk>& Messege);
	//읽을 메시지를 저장한다.
	void addMessegerFriend(MessengerFrienKind FriendKind,int FriendNum);
	
	//--------------------------------사용함수--------------------------------
	
	//사용한 아이템 , 사용한 아이템을 없애는지 여부
	void usePLItem(int ItemNum,bool RemoveItem);

private:
	
	//직급
	Rank_List pl_rank = Rank_List::Intern;

	//플레이어의 씬에 관한 정보
	Player_SceneData pl_sceneData;

	//플레이어의 포인트 정보
	Point pl_MapPoint = Point(0.0f,0.0f);

	//지금 플레이중인 음악 위치
	int pl_musicPoint = 0;

	//게임 설정과 키보드 정보
	Game_Interface m_gameSetting_Data;

	//아이템 이메일 업무
	//아이템(상호작용: 쿠폰, 카드키),(관상용: 사진, 텍스트 파일)

	//플레이어가 가지고있는 아이템
	std::vector<Game_Item> pl_item = std::vector<Game_Item>(0);
	//플레이어가 가지고있는 파일들
	std::vector<Game_AddFile> pl_file = std::vector<Game_AddFile>(0);
	//메신저 대화방
	std::vector<MessengerRoom > pl_messege = std::vector<MessengerRoom >(0);
	//메신저 친구들
	std::vector<MessengerFrien> pl_friend= std::vector<MessengerFrien>(0);
	//컴퓨터 아이콘 갯수
	std::vector<Game_ComputerIcon> pl_icon =std::vector<Game_ComputerIcon>(0);	
	//내가 클리어한 게임또는 대화한 NPC의 기록들
	std::vector<Clear_Json> pl_clear = std::vector<Clear_Json>(0);
	//플레이어 음악
	std::vector<Game_Music> pl_music = std::vector<Game_Music>(0);
};
#define TALK_SAVEEVENT 1

using namespace std;
using namespace cocos2d;
/*
씬 이벤트를 만들어서 하나로 묶을것인지
talkevent와 Scene이벤트를 따로 만들지
*/
class Game_Event {
public:
	//num에 해당하는 이벤트값을 준다.
	void getTalkEvent(int num);

private:

};

//파일이 비어있는지 확인한다
bool is_empty(std::ifstream&);


//맵의 file이름을 가지고온다.
char* GetMapFileName(GameMapPlace ChangeScene
	= Game_Player::getInstance()->getSceneInterface().pl_stageMap);

ui::Layout* AddItemBox(std::vector<Game_Object>);

char* MapBackGroundMusic(GameMapPlace ChangeScene
	= Game_Player::getInstance()->getSceneInterface().pl_stageMap);

char* WalkingSound(GameMapPlace ChangeScene
	= Game_Player::getInstance()->getSceneInterface().pl_stageMap);

//---------------------------확인함수---------------------------
//선행조건에 걸리는지 확인하는 함수
//대화 조건이 완수되었는지 확인한다
bool CheckLimitidFun(Quest_Limited Limite);
