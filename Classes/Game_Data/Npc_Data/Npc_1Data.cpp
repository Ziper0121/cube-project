#include "Game_Data\Npc_Data\Npc_1Data.h"


/*

NPC의 이름을 가지고 파일을 찾고
파일 안에서 주제에 맞는 파일을 또 찾는다.
NPC의 감정의 상태와 친한정도 받은 의견의 형태를 가지고 찾는다.

NPC에게 부탁을하면

관심있는 사람, NPC와 했던 이전 대화주제
감정, 정보, 주제, 들은 말

대화를 하는 이유

내가 궁금한것이 있어서
재미를 위해서
부탁을하고싶어서

*/
Npc_1::Npc_1() : Npc_Char(1, "Npc1", "Image/TestNpc_1.png", 1,Point(2,1)) {
	//캐릭터의 스케줄을 설정한다.
	ReceivedWord Npc_1Received;

	Npc_1Received.What = WorkKind::CompanyWork;
	DaySchedule.push_back(Npc_1Received);

	Npc_1Received.What = WorkKind::CompanyWork;
	DaySchedule.push_back(Npc_1Received);

	Npc_1Received.What = WorkKind::SmallTalk;
	DaySchedule.push_back(Npc_1Received);


	//캐릭터가 지금하고있는 행동을 나타낸다.
	Label* CharState = Label::create(getWorkKindString(
		DaySchedule.at(0).What), FONT_NAME, 4.0f);
	CharState->setAnchorPoint(Vec2(0.0f, 1.0f));
	Char_Body->addChild(CharState);
}
std::string Npc_1::getTalkTesture(std::vector<Npc_Char> CheckChar, TalkKind getTell) {

	//대화가 저장된 장소 자신의 이름의 파일이 있다
	std::string	TexturFileName = "Game_Data/Npc_Data/" + Name + "/";
	
	if (CheckChar.size() > 2) {

	}

	//대상과 여태 어떤 대화를 했는지 알아본다.
	CharKind FirstCharKind = (CharKind)CheckChar.at(1).Unique_Number;
	TolpicKind Tolpic;
	//대화 주제
	switch (Tolpic)
	{
	case TolpicKind::WorkTolpic:
		TexturFileName.append("WorkTopic.json");
		break;
	case TolpicKind::LoveTolpic:
		TexturFileName.append("LoveTopic.json");
		break;
	default:
		break;
	}

	//Document의 Key 값들

	//내가 말을 한 대상의 이름
	std::string NpcNameKey = CheckChar.at(1).Name;

	//내가 들은 의견의 문자열값 
	std::string TellKey = m_checkTalkKind(Tolpic, getTell);

	std::string String = TellKey;

	return String;
}

bool Npc_1::addNpcTaskWork(Npc_Char TaskNpc) {

	return false;
}
WorkKind Npc_1::checkNpcThink() {

	//캐릭터가 하고있는 일
	ReceivedWord NpcSchedule = DaySchedule.at(0);

	//캐릭터가 할일
	WorkKind CharWork = NpcSchedule.What;

	switch (NpcSchedule.What)
	{
	case NoWork:
		break;
	case SmallTalk:
		break;
	case Bringing:
		break;
	case Delivery:
		break;
	case CompanyWork:
		break;

	default:
		break;
	}

	/*
	float RequiredTime = NpcSchedule.WhenEnd / (WorkSkill *According);
	*/

	//캐릭터의 행동을 나타내는 문자를 변경한다.
	Label* CharStateText = (Label*)(Char_Body->getChildByTag(7));
	CharStateText->setString(getWorkKindString(NpcSchedule.What));

	return CharWork;

}
std::string Npc_1::m_checkTalkKind(TolpicKind tolpic, TalkKind getTell) {
	switch (getTell)
	{
	case Question:
		getTell = Answer;
		break;
	case Answer:
		getTell = Counter;
		break;
	case Counter:
		getTell = Question;
		break;
	default:
		break;
	}

	return getTalkKindString(getTell);
}



Npc_2::Npc_2() : Npc_Char(2, "Npc2", "Image/TestNpc_2.png", 2, Point(3, 6)) {
	//캐릭터의 스케줄을 설정한다.
	ReceivedWord Npc_1Received;

	Npc_1Received.What = WorkKind::CompanyWork;
	DaySchedule.push_back(Npc_1Received);

	Npc_1Received.What = WorkKind::CompanyWork;
	DaySchedule.push_back(Npc_1Received);

	Npc_1Received.What = WorkKind::SmallTalk;
	DaySchedule.push_back(Npc_1Received);


	//캐릭터가 지금하고있는 행동을 나타낸다.
	Label* CharState = Label::create(getWorkKindString(
		DaySchedule.at(0).What), FONT_NAME, 4.0f);
	CharState->setAnchorPoint(Vec2(0.0f, 1.0f));
	Char_Body->addChild(CharState);
}
std::string Npc_2::getTalkTesture(std::vector<Npc_Char> CheckChar, TalkKind getTell) {

	//대화가 저장된 장소 자신의 이름의 파일이 있다
	std::string	TexturFileName = "Game_Data/Npc_Data/" + Name + "/";

	if (CheckChar.size() > 2) {

	}

	//대상과 여태 어떤 대화를 했는지 알아본다.
	CharKind FirstCharKind = (CharKind)CheckChar.at(1).Unique_Number;
	TolpicKind Tolpic;
	//대화 주제
	switch (Tolpic)
	{
	case TolpicKind::WorkTolpic:
		TexturFileName.append("WorkTopic.json");
		break;
	case TolpicKind::LoveTolpic:
		TexturFileName.append("LoveTopic.json");
		break;
	default:
		break;
	}

	//Document의 Key 값들

	//내가 말을 한 대상의 이름
	std::string NpcNameKey = CheckChar.at(1).Name;

	//내가 들은 의견의 문자열값 
	std::string TellKey = m_checkTalkKind(Tolpic, getTell);

	std::string String = TellKey;

	return String;
}

bool Npc_2::addNpcTaskWork(Npc_Char TaskNpc) {

	return false;
}
WorkKind Npc_2::checkNpcThink() {

	//캐릭터가 하고있는 일
	ReceivedWord NpcSchedule = DaySchedule.at(0);

	//캐릭터가 할일
	WorkKind CharWork = NpcSchedule.What;

	switch (NpcSchedule.What)
	{
	case NoWork:
		break;
	case SmallTalk:
		break;
	case Bringing:
		break;
	case Delivery:
		break;
	case CompanyWork:
		break;

	default:
		break;
	}

	/*
	float RequiredTime = NpcSchedule.WhenEnd / (WorkSkill *According);
	*/

	//캐릭터의 행동을 나타내는 문자를 변경한다.
	Label* CharStateText = (Label*)(Char_Body->getChildByTag(7));
	CharStateText->setString(getWorkKindString(NpcSchedule.What));

	return CharWork;

}
std::string Npc_2::m_checkTalkKind(TolpicKind tolpic, TalkKind getTell) {
	switch (getTell)
	{
	case Question:
		getTell = Answer;
		break;
	case Answer:
		getTell = Counter;
		break;
	case Counter:
		getTell = Question;
		break;
	default:
		break;
	}

	return getTalkKindString(getTell);
}



Npc_3::Npc_3() :Npc_Char(3, "Npc3", "Image/TestNpc_3.png", 3, Point(5, 5)) {

	//캐릭터의 스케줄을 설정한다.
	ReceivedWord Npc_1Received;

	Npc_1Received.What = WorkKind::CompanyWork;
	DaySchedule.push_back(Npc_1Received);

	Npc_1Received.What = WorkKind::Bringing;
	DaySchedule.push_back(Npc_1Received);

	//캐릭터가 지금하고있는 행동을 나타낸다.
	Label* CharState = Label::create(getWorkKindString(
		DaySchedule.at(0).What), FONT_NAME, 4.0f);
	CharState->setAnchorPoint(Vec2(0.0f, 1.0f));
	Char_Body->addChild(CharState);

}
std::string Npc_3::getTalkTesture(std::vector<Npc_Char> CheckChar, TalkKind getTell) {

	return "String";
}


/*
check Tink는 자신이 의뢰를 받을경우 그것을 DaySchedule에 저장한다.
저장 방식
일을 받을경우 누군가에게 부탁을 하는 상황을 판별한다.

*/

WorkKind Npc_3::checkNpcThink() {

	/*
	행동중에 다른 행동이 하고싶은지 확인한다.
	행동 : 대화, 일, 전달, 배달
	부탁 : 만남, 일, 전달, 배달
	*/

	//캐릭터가 하고있는 일
	ReceivedWord NpcSchedule = DaySchedule.at(0);

	//캐릭터가 할일
	WorkKind CharWork = NpcSchedule.What;

	switch (NpcSchedule.What)
	{
	case SmallTalk:
		/*
		대화하는 동안 분당 체크한다.
		대화할때 마다 WhenEnd를 줄어든다.
		앞으로의 일정을 확인한다
		*/
		if (NpcSchedule.WhenEnd < 0) {
			CharWork = DaySchedule.at(1).What;
		}
		break;
	case Bringing:
		/*

		*/
		Time EndTime;
		EndTime.Hour = 0;
		EndTime.Minutes = 10;

		NpcSchedule.When > EndTime;
		break;
	case Delivery:
		/*
		누군가 우직이는지도 확인해야한다.
		다른 사람에게 받은일을 전달해야할때도 확인한다.
		가지고 가던중 또는 누군가 어디를 갈경우 
		부탁을한다.
		*/
		break;
	case CompanyWork:
		/*
		내가일이 얼만큼 남았는지 확인하고 누군가에게 부탁한다.
		*/
		break;
	default:
		break;
	}
	
	/*
	float RequiredTime = NpcSchedule.WhenEnd / (WorkSkill *According);
	*/
	
	if (NpcSchedule.What != CharWork) {
		//캐릭터의 행동을 나타내는 문자를 변경한다.
		Label* CharStateText = (Label*)(Char_Body->getChildByTag(7));
		CharStateText->setString(getWorkKindString(NpcSchedule.What));
	}

	return CharWork;

}

bool Npc_3::addNpcTaskWork(Npc_Char TaskNpc) {

	/*
	그 사람이 부탁을 해야지 들어주는 것이다.
	*/
	/* 약속의 종류
	몇시에 만나자
	몇시까지 이것좀 해줘
	몇시에 이것좀 갔다 놔
	몇시에 이것좀 가지고 와
	
	다양한 말들을 다 enum화를 시켜야하는가?


	이 시간대에 돼?
	같은 일인지?



	내가 부탁을 들어주고 싶은가?
	호감도 상하관계
	내가 부탁을 들어줄 상황인가?
	0.내가 지금 일을 하고있는지?	상황
	1.내가 할일이 많은지?			상황
	2.시간대에 약속이 있는지?		시간
	3.다른시간이 가능한지?			시간
	4.다른약속을 뺄수있는지?		상황
	*지금 하는일과 같이 할수있는 일인지?	*/

	//Npc의 값이 없거나 가지고있는 스케줄이 없을경우
	if (TaskNpc.DaySchedule.empty())
		return false;

	//NPc에 대한 내 호감도
	int FaveoriteNpc = 5;
	//부탁을 하는 사람의 대한 호감도
	const CharKind TaskNpcKind = (CharKind)TaskNpc.Unique_Number;
	if (FavoriteChar.find(TaskNpcKind) != FavoriteChar.end())
		FaveoriteNpc = FavoriteChar[TaskNpcKind];

	//0은 싫어하는 사람 100은 좋아하는 사람이다. 사람마다 들어주는 수준이 다르다.


	//승락 여부
	bool CheckConsent = false;
	
	//호감도에 따른 승락 조건들
	if (FaveoriteNpc >= 50 && !CheckConsent) {
		CheckConsent = DaySchedule.at(0).What == WorkKind::NoWork;
	}
	if (FaveoriteNpc >= 60 && !CheckConsent) {
		CheckConsent = DaySchedule.size() < 3;
	}
	if (FaveoriteNpc >= 70 && !CheckConsent) {
		for (auto i : DaySchedule) {
			if (i.When == TaskNpc.DaySchedule.at(0).When) {
				if(i.What == WorkKind::NoWork)
				CheckConsent = true;
				break;
			}
		}
	}
	if (FaveoriteNpc >= 80 && !CheckConsent) {
		for (auto i : DaySchedule) {
			if (i.What == WorkKind::NoWork) {
				CheckConsent = true;
				break;
			}
		}
	}
	if (FaveoriteNpc >= 90 && !CheckConsent) {

	}
	//받은 일
	ReceivedWord RequestWork = TaskNpc.DaySchedule.at(0);
	//받은 일의 종류
	WorkKind QuestWorkKind = RequestWork.What;

	switch (QuestWorkKind)
	{
	case SmallTalk:
		break;
	case Bringing:
		break;
	case Delivery:
		break;
	case CompanyWork:
		break;
	default:
		break;
	}
}

std::string Npc_3::m_checkTalkKind(TolpicKind tolpic, TalkKind getTell) {
	switch (getTell)
	{
	case Question:
		getTell = Answer;
		break;
	case Answer:
		getTell = Counter;
		break;
	case Counter:
		getTell = Question;
		break;
	default:
		break;
	}

	return getTalkKindString(getTell);
}
