#pragma once

#include "Game_Data\Npc_Data\NPC_Data.h"
#include <cstdio>

class Npc_1 : public Npc_Char {
public:
	Npc_1();

	//NPC와 대화를 Texture을 준다.
	virtual std::string getTalkTesture
	(std::vector<Npc_Char> CheckChar, TalkKind getTell);

	//Npc가 Task를 받았을경우
	virtual bool addNpcTaskWork(Npc_Char TaskNpc);
private:
	//npc의 상태를 확인한다.
	virtual WorkKind checkNpcThink();

	//NPC와 대화에 Key값을 준다.
	std::string m_checkTalkKind(TolpicKind tolpic, TalkKind getTell);
};

class Npc_2 : public Npc_Char {
public:
	Npc_2();

	//NPC와 대화를 Texture을 준다.
	virtual std::string getTalkTesture
	(std::vector<Npc_Char> CheckChar, TalkKind getTell);

	//Npc가 Task를 받았을경우
	virtual bool addNpcTaskWork(Npc_Char TaskNpc);
private:
	
	//npc의 상태를 확인한다.
	virtual WorkKind checkNpcThink();

	//NPC와 대화에 Key값을 준다.
	std::string m_checkTalkKind(TolpicKind tolpic, TalkKind getTell);
};

class Npc_3 : public Npc_Char {
public:
	Npc_3();

	//NPC와 대화를 Texture을 준다.
	virtual std::string getTalkTesture
	(std::vector<Npc_Char> CheckChar, TalkKind getTell);
private:

	//npc의 상태를 확인한다.
	virtual WorkKind checkNpcThink();

	virtual bool addNpcTaskWork(Npc_Char TaskNpc);

	//NPC와 대화에 Key값을 준다.
	std::string m_checkTalkKind(TolpicKind tolpic, TalkKind getTell);
};
