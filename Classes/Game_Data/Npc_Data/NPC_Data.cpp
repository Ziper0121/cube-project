#include "NPC_Data.h"
#include "Game_Data\Game_MapData.h"

Npc_Char::Npc_Char(int uniNum, const char* charName, const char* bodyName
	, int walkSpeed, Point NpcMapPoint)
	: Game_Char(uniNum, charName, bodyName, walkSpeed) {
	//캐릭터 포인트
	MapPoint = NpcMapPoint;
	
	//캐릭터 Sprite몸체
	auto BodyTexture
		= Director::getInstance()->getTextureCache()->addImage(bodyName);
	Char_Body = Sprite::createWithTexture(
		BodyTexture, Rect(Vec2(0.0f, 0.0f), Size(16.0f, 16.0f)));
	Char_Body->setPosition(MapPoint * 16.0f);
	Char_Body->setAnchorPoint(Vec2::ANCHOR_BOTTOM_LEFT);
	Char_Body->retain();

	MapController::createMapController()->
		getTileMap(MapPlace)->addChild(Char_Body);

}


//Emotion을 문자열값으로 변경하는 함수
std::string getEmotionString(EmotionKind charEmotion) {
	std::string EmotionString;
	switch (charEmotion)
	{
	case NoEmotion:
		EmotionString = "Nomal";
		break;
	case Angry:
		EmotionString = "Angry";
		break;
	case Happy:
		EmotionString = "Happy";
		break;
	case Sad:
		EmotionString = "Sad";
		break;
	case Doubt:
		EmotionString = "Doubt";
		break;
	default:
		break;
	}
	return EmotionString;
}

//TalkKind을 문자열값으로 변경하는 함수
std::string getTalkKindString(TalkKind talkKind) {
	std::string TalkString;
	switch (talkKind)
	{
	case NoTalk:
		TalkString = "NoTalk";
		break;
	case Question:
		TalkString = "Question";
		break;
	case Answer:
		TalkString = "Answer";
		break;
	case Counter:
		TalkString = "Counter";
		break;
	default:
		TalkString = "";
		break;
	}
	return TalkString;
}

//WorkKind을 문자열값으로 변경하는 함수.
std::string getWorkKindString(WorkKind workKind) {
	std::string TalkString;
	switch (workKind)
	{
	case NoWork:
		TalkString = "NoWork";
		break;
	case SmallTalk:
		TalkString = "SmallTalk";
		break;
	case Bringing:
		TalkString = "Bringing";
		break;
	case Delivery:
		TalkString = "Delivery";
		break;
	case CompanyWork:
		TalkString = "CompanyWork";
		break;
	default:
		break;
	}
	return TalkString;
}
