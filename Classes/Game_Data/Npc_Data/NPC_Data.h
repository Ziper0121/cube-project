#pragma once
/*
NPC의 zOrder값은 맵 윗쪽부터 Y값으로 하자
*/
#include "Game_Data\Game_Specimen_List.h"
#include "Game_Data\Game_SceneData.h"
#include "Game_Data\Test_Schedule.h"

using namespace cocos2d;

//NPC의 할일
class NpcRequest {
	//목적
	WorkKind Purpose;
	//목적 오브젝트
	std::vector<int> PurposeObjNum;
	//최종목적지 번호
	int PassObjNum;
};
//완전히 나누지 않은 이유는 모든 정보를 get set으로 조정해야하기 때문이다.

//Npc의 기본틀
class Npc_Char : public Game_Char {
public:
	Npc_Char(int uniNum, const char* charName, const char* bodyName
		, int walkSpeed,Point NpcMapPoint);

	//스크래티지? 그냥 비슷한것같다.
	virtual std::string getTalkTesture
	(std::vector<Npc_Char> CheckChar, TalkKind getTell) { return "Null"; };

	//npc의 상태를 확인한다.
	virtual WorkKind checkNpcThink() { return WorkKind::NoWork; };

	virtual bool addNpcTaskWork(Npc_Char TaskNpc) { return true; };

	//---------------------NPC 상태 표시---------------------

	int WorkSkill = 0;

	//---------------------이동 관련---------------------

	//맵위치
	GameMapPlace MapPlace = GameMapPlace::TestMap;
	//맵 포인터(1,3)
	Vec2 MapPoint = Vec2(1.0f, 1.0f);
	//움직일 포인트
	std::vector<Vec2> MovingPoint = std::vector<Vec2>(0);
	//Npc가 주는 퀘스트

	//---------------------대화 관련 함수---------------------

	//NPC가 좋아하는 캐릭터
	std::map<CharKind, int> FavoriteChar;
	
	//해야하는 일
	std::vector<ReceivedWord> DaySchedule;

	//목격 상황
	std::vector<SightingScene> MemorySighting;



};

//Emotion을 문자열값으로 변경하는 함수
std::string getEmotionString(EmotionKind charEmotion);

//TalkKind을 문자열값으로 변경하는 함수
std::string getTalkKindString(TalkKind talkKind);

//WorkKind을 문자열값으로 변경하는 함수.
std::string getWorkKindString(WorkKind workKind);