﻿#include "JsonParse.h"
#include <json\document.h>
#include <json\istreamwrapper.h>
#include <json\ostreamwrapper.h>
#include <json\prettywriter.h>
#include <json\writer.h>
#include <fstream>
using namespace rapidjson;

Scene* JsonParsing::createScene(){
	auto scene = Scene::create();
	auto layer = JsonParsing::create();
	scene->addChild(layer);
	return scene;
}

bool JsonParsing ::init(){
	if (!Layer::init()) {
		return false;
	}
	m_ParseOutPut();
	return true;
}

void JsonParsing ::m_parseSimple(){
	char* json;
	json = "{\"name\":\"Tina\",\"age\":25}";

	Data fileData = FileUtils::getInstance()->getDataFromFile("Test_Json.json");
	unsigned char* bytes = fileData.getBytes();
	ssize_t  byte_count = fileData.getSize();

	std::string json_string = std::string((char*)bytes, 0, byte_count);

	Document d;

	d.Parse(json_string.c_str());
	bool parseError = d.HasParseError();
	if (parseError)CCLOG("Parse error : %d", parseError);

	std::string name = d["name"].GetString();
	int age = d["age"].GetInt();

	CCLOG("name : %s", name.c_str());
	CCLOG("age : %d", age);
}
void JsonParsing ::m_parseAdvnce() {
	using namespace rapidjson;

	Data fileData = FileUtils::getInstance()->getDataFromFile("ItemData.json");
	unsigned char* bytes = fileData.getBytes();
	ssize_t  byte_count = fileData.getSize();
	std::string json_string = std::string((char*)bytes, 0, byte_count);
	
	//std::ifstream file("ItemData.json");
	//IStreamWrapper Rjson(file);
	CCLOG("readed: %s", json_string.c_str());

	StringStream ss = StringStream(json_string.c_str());
	Document d;
	d.ParseStream(ss);
	if (d.HasParseError()) {
		ParseErrorCode error = d.GetParseError();
		CCLOG("ParseErrorCode:%d", error);
	}
	return;

	auto i = d["TestNpc"].Begin();
	i += 1;
	const char* name = (*i)["name"].GetString();
	const int start = (*i)["start"].GetInt();
	const bool end = (*i)["end"].GetBool();
	
	CCLOG("Name : %s, Start : %d, End : %s", name, start, end ? "true" : "false");
	return;
	for (auto i = d["TestNpc"].Begin(); i <d["TestNpc"].End(); i++)
	{
		const char* name = (*i)["name"].GetString();
		const int start = (*i)["start"].GetInt();
		const bool end	 = (*i)["end"].GetBool();

		CCLOG("Name : %s, Start : %d, End : %s", name, start, end ? "true" : "false");
	} 
	
}
void JsonParsing ::m_ParseOutPut() {
	using namespace rapidjson;
	
	std::ifstream is("ItemData.json");
	IStreamWrapper iw(is);

	Document d;
	d.ParseStream(iw);

	auto Beg = d["GameItem"].Begin();
	(*Beg)["Name"].SetString("BBBB");
	(*Beg)["UseRank"].SetInt(5);

	//StringBuffer buf;
	//Writer<StringBuffer> write(buf);
	
	//d.Accept(write);
	//CCLOG("%s", buf.GetString());

	std::ofstream ofs("ItemData.json");
	OStreamWrapper osw(ofs);

	//write<OStreamWrapper> pw(osw); 직열저장
	PrettyWriter<OStreamWrapper> pw(osw);
	
	d.Accept(pw);
}