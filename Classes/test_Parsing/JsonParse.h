#pragma once
#include <cocos2d.h>

using namespace cocos2d;

class JsonParsing : public Layer {
public:
	static Scene* createScene();
	virtual	bool init();
	CREATE_FUNC(JsonParsing);
private:
	void m_parseSimple();
	void m_parseAdvnce();

	void m_ParseOutPut();
};