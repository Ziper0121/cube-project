#pragma once
#include <cocos2d.h>
#pragma execution_character_set("utf-8")

//file들의 파일위치
#define IMAGE_ROUTE "Image/"
#define OBJECTFACE_ROUTE	IMAGE_ROUTE"Face/"

//Item의 외형 파일 위치
#define ITEM_ROUTE IMAGE_ROUTE"Item/"

#define MUSICIMAGE_ROUTE IMAGE_ROUTE"MusicImage/"

//폰트 파일 파일위치
#define FONT_ROUTE "Fonts/"
//기본 인터페이스 폰트
#define FONT_NAME FONT_ROUTE"Joseon.ttf"

//데이터 파일들
#define GAMEDATA_ROUTE "Gaem_Data/"
#define ITEMDATA_ROUTE		GAMEDATA_ROUTE"Item_Data/"
#define	MAPNPCDATA_ROUTE		GAMEDATA_ROUTE"MapAndNpc_Data/"
#define MISSTIONDATA_ROUTE		GAMEDATA_ROUTE"Misstion_Data/"
#define PLAYERDATA_ROUTE		GAMEDATA_ROUTE"Player_Data/"
#define LANGUAGEDATA_ROUTE		GAMEDATA_ROUTE"Language_Data/"
#define MESSENGER_ROUTE			GAMEDATA_ROUTE"Messenger_Data/"
#define MESSENGER_FRIEN_ROUTE  GAMEDATA_ROUTE"Friend_Data/"
#define MUSIC_ROUTE  GAMEDATA_ROUTE"Music_Data/"


//음악관련 파일위치
#define SOUND_ROUTE "Sound/"
#define EFFECT_ROUTE SOUND_ROUTE "Effect_Sound/"
#define BACKMUSIC_ROUTE SOUND_ROUTE "BackGroundMusic/"

//맵관련 파일위치
#define TILEMAP_ROUTE "TileMap/"

//TMX맵관련 파일위치
#define TMX_ROUTE TILEMAP_ROUTE"TMX/"

//가장 많이 쓰는 비상용 이미지
#define WHITEBLOCK_PNG	IMAGE_ROUTE"WhiteBlock.png"

//윈도우 기본크기

#define WINSIZE Vec2(384.0f,384.0f)