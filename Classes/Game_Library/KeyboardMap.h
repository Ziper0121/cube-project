//
//  KeyboardMap.hpp
//  explottens
//
//  Created by Bilal Mirza on 24/01/2018.
//

#ifndef KeyboardMap_hpp
#define KeyboardMap_hpp

#include <stdio.h>
#include "cocos2d.h"

USING_NS_CC;

//KeyCode를 텍스트로 매치해놓은 class
class KeyBoardMap
{
public:
    static KeyBoardMap *getInstance();

    std::vector<std::string> keys;
    void addtoVector(){
       
        keys.push_back("NONE");
        keys.push_back("PAUSE");
        keys.push_back("SCROLL LOCK");
        keys.push_back("PRINT");
        keys.push_back("SYSREQ");
        keys.push_back("BREAK");
        keys.push_back("BACK");
        keys.push_back("BACKSPACE");
        keys.push_back("TAB");
        keys.push_back("BACKTAB");
        keys.push_back("RETURN");
        keys.push_back("CAPS LOCK");
        keys.push_back("LEFT SHIFT");
        keys.push_back("RIGHT SHIFT");
        keys.push_back("LEFT CTRL");
        keys.push_back("RIGHT CTRL");
        keys.push_back("LEFT ALT");
        keys.push_back("RIGHT ALT");
        keys.push_back("MENU");
        keys.push_back("HYPER");
        keys.push_back("INSERT");
        keys.push_back("HOME");
        keys.push_back("PG_UP");
        keys.push_back("DELETE");
        keys.push_back("END");
        keys.push_back("PG_DOWN");
        keys.push_back("L_ARROW");
        keys.push_back("R_ARROW");
        keys.push_back("UP_ARROW");
        keys.push_back("D_ARROW");
        keys.push_back("NUM LOCK");
        keys.push_back("PLUS");
        keys.push_back("MINUS");
        keys.push_back("MULTIPLY");
        keys.push_back("DIVIDE");
        keys.push_back("ENTER");
        keys.push_back("HOME");
        keys.push_back("UP");
        keys.push_back("PG UP");
        keys.push_back("LEFT");
        keys.push_back("FIVE");
        keys.push_back("RIGHT");
        keys.push_back("END");
        keys.push_back("DOWN");
        keys.push_back("PG DOWN");
        keys.push_back("INSERT");
        keys.push_back("DELETE");
        keys.push_back("F1");
        keys.push_back("F2");
        keys.push_back("F3");
        keys.push_back("F4");
        keys.push_back("F5");
        keys.push_back("F6");
        keys.push_back("F7");
        keys.push_back("F8");
        keys.push_back("F9");
        keys.push_back("F10");
        keys.push_back("F11");
        keys.push_back("F12");
        keys.push_back("SPACE");
        keys.push_back("EXCLAM");
        keys.push_back("QUOTE");
        keys.push_back("NUMBER");
        keys.push_back("DOLLAR");
        keys.push_back("PERCENT");
        keys.push_back("CIRCUMFLEX");
        keys.push_back("AMPERSAND");
        keys.push_back("APOSTROPHE");
        keys.push_back("LEFT_PARENTHESIS");
        keys.push_back("RIGHT_PARENTHESIS");
        keys.push_back("ASTERISK");
        keys.push_back("PLUS");
        keys.push_back("COMMA");
        keys.push_back("MINUS");
        keys.push_back("PERIOD");
        keys.push_back("SLASH");
        keys.push_back("0");
        keys.push_back("1");
        keys.push_back("2");
        keys.push_back("3");
        keys.push_back("4");
        keys.push_back("5");
        keys.push_back("6");
        keys.push_back("7");
        keys.push_back("8");
        keys.push_back("9");
        keys.push_back("COLON");
        keys.push_back("SEMICOLON");
        keys.push_back("LESS_THAN");
        keys.push_back("EQUAL");
        keys.push_back("GREATER_THAN");
        keys.push_back("QUESTION");
        keys.push_back("AT");
        keys.push_back("CAPITAL_A");
        keys.push_back("CAPITAL_B");
        keys.push_back("CAPITAL_C");
        keys.push_back("CAPITAL_D");
        keys.push_back("CAPITAL_E");
        keys.push_back("CAPITAL_F");
        keys.push_back("CAPITAL_G");
        keys.push_back("CAPITAL_H");
        keys.push_back("CAPITAL_I");
        keys.push_back("CAPITAL_J");
        keys.push_back("CAPITAL_K");
        keys.push_back("CAPITAL_L");
        keys.push_back("CAPITAL_M");
        keys.push_back("CAPITAL_N");
        keys.push_back("CAPITAL_O");
        keys.push_back("CAPITAL_P");
        keys.push_back("CAPITAL_Q");
        keys.push_back("CAPITAL_R");
        keys.push_back("CAPITAL_S");
        keys.push_back("CAPITAL_T");
        keys.push_back("CAPITAL_U");
        keys.push_back("CAPITAL_V");
        keys.push_back("CAPITAL_W");
        keys.push_back("CAPITAL_X");
        keys.push_back("CAPITAL_Y");
        keys.push_back("CAPITAL_Z");
        keys.push_back("LEFT_BRACKET");
        keys.push_back("BACK_SLASH");
        keys.push_back("RIGHT_BRACKET");
        keys.push_back("UNDERSCORE");
        keys.push_back("GRAVE");
        keys.push_back("A");
        keys.push_back("B");
        keys.push_back("C");
        keys.push_back("D");
        keys.push_back("E");
        keys.push_back("F");
        keys.push_back("G");
        keys.push_back("H");
        keys.push_back("I");
        keys.push_back("J");
        keys.push_back("K");
        keys.push_back("L");
        keys.push_back("M");
        keys.push_back("N");
        keys.push_back("O");
        keys.push_back("P");
        keys.push_back("Q");
        keys.push_back("R");
        keys.push_back("S");
        keys.push_back("T");
        keys.push_back("U");
        keys.push_back("V");
        keys.push_back("W");
        keys.push_back("X");
        keys.push_back("Y");
        keys.push_back("Z");
        keys.push_back("LEFT_BRACE");
        keys.push_back("BAR");
        keys.push_back("RIGHT_BRACE");
        keys.push_back("TILDE");
        keys.push_back("EURO");
        keys.push_back("POUND");
        keys.push_back("YEN");
        keys.push_back("MIDDLE_DOT");
        keys.push_back("SEARCH");
        keys.push_back("DPAD_LEFT");
        keys.push_back("DPAD_RIGHT");
        keys.push_back("DPAD_UP");
        keys.push_back("DPAD_DOWN");
        keys.push_back("DPAD_CENTER");
        keys.push_back("ENTER");
        keys.push_back("PLAY");

        
    }
    
    std::string getKeyCodeInStr(EventKeyboard::KeyCode key){
        log("%d ",static_cast<unsigned int>(key));
        log("%s",keys.at(static_cast<unsigned int>(key)).c_str());
        return keys.at(static_cast<unsigned int>(key));
    }
   
};

#endif /* KeyboardMap_hpp */
