#pragma once

//게임에 데이터들을 해더파일들을 가지고있다.
#define DATA_ROUTE "Game_Data\"
//게임 인터페이스의 정보들
#define INTERFACE_H  "Game_Data\Game_Interface_Data.h"
//맵 정보들
#define MAPDATA_H "Game_Data\Game_MapData.h"
//씬 정보들
#define SCENEDATA_H "Game_Data\Game_SceneData.h"
//게임내의 기본 정보들
#define SPECIMENLIST_H "Game_Data\Game_Specimen_List.h"
//게임내 텍스트들의 정보들
#define TEXTSPECIMENLIST_H "Game_Data\Game_TextSpecimen_List.h"
//앤피씨의 정보들
#define NPCDATA_H "Game_Data\NPC_Data.h"
//플레이어의 정보들
#define PLAYERDATA_H "Game_Data\Player_Data.h"
//대화창의 정보들
#define TALKOBJECTDATA_H "Game_Data\TalkObject_Data.h"


//게임내 단순 자료들의 해더파일들을 가지고있다.
#define LIBRARY_ROUTE "Game_Library\"
//임펙트 자료들을 가진 해더파일
#define ALLEFFECT_H "Game_Library\AllEffect.h"
//게임의 기본 데이터를 가진 해더파일
#define GAMECONST_H "Game_Library\Game_ConstData.h"
//게임의 아이콘 데이터를 가진 해더파일
#define ICONCONST_H "Game_Library\IconImage_ConstData.h"
//키보드를 가진 해더파일
#define KEYBOARD_H "Game_Library\KeyboardMap.h"

//다른 해더파일의 단순 자료들의 해더파일들을 가지고있다.
#define CONSTLIBRARY_ROUTE "Game_Library\G_ConstLibrary\"
//컴퓨터의 자료를 가진 해더파일
#define COMPUTER_CONST_H "Game_Library\G_ConstLibrary\Computer_CCollection.h"
//큐브 자료를 가진 해더파일
#define CUBE_CONST_H "Game_Library\G_ConstLibrary\Cube_CCollection.h"
//인터페이스 자료를 가진 해더파일
#define INTERFACE_CONST_H "Game_Library\G_ConstLibrary\Interface_CCollection.h"
//키보드 자료를 가진 해더파일
#define KEYBOARD_CONST_H "Game_Library\G_ConstLibrary\KeyBoard_CCollection.h"
//언어의 자료를 가진 해더파일
#define LANGUAGE_CONST_H "Game_Library\G_ConstLibrary\LanguageManager_CCollection.h"
//메인메뉴 자료를 가진 해더파일
#define MAINMENU_CONST_H "Game_Library\G_ConstLibrary\MainMenu_CCollection.h"
//플레이어의 가방 자료를 가진 해더파일
#define PL_WINDOW_CONST_H "Game_Library\G_ConstLibrary\PL_DataWindow_CCollection.h"
//플레이어의 자료를 가진 해더파일
#define PLAYDR_CONST_H "Game_Library\G_ConstLibrary\PlayerData_CCollection.h"
//대화 자료를 가진 해더파일
#define TALK_CONST_H "Game_Library\G_ConstLibrary\TalkWindow_CCollction.h"
//타일 맵 자료를 가진 해더파일
#define TILEMAP_CONST_H "Game_Library\G_ConstLibrary\TileMap_CCollection.h"

//실제 화면을 구성하는 해더파일을 저장한다.
#define PROJECT_ROUTE "Game_Project\"
//메인화면
#define MAINMENU_H "Game_Project\MainMenu_Screen.h"

//게임을 실질적으로 구동시키는 해더파일의 파일들을 저장한다.
#define TOOL_ROUTE "Game_Tools\"

//게임내의 컴퓨터를 구동시키는 해더파일들을 저장한다.
#define COMPUTER_ROUTE "Game_Tools\Computer\"
//컴퓨터를 구동시키는 해더파일
#define COMPUTER_COM_H "Game_Tools\Computer\Modify_Computer.h"
//큐브를 구동시키는 해더파일
#define CYBE_COM_H "Game_Tools\Computer\Modify_Cube.h"
//메일을 구동시키는 해더파일
#define MAILL_COM_H "Game_Tools\Computer\Modify_Mail.h"
//메시지를 구동시키는 해더파일
#define MASSEGE_COM_H "Game_Tools\Computer\Modify_Massenger.h"

//게임내에 제작에 관련된 해더파일들을 저장한다.
#define MAKING_ROUTE "Game_Tools\Making\"
//인터페이스를 만드는 해더파일
#define INTERFACE_MAKING_H "Game_Tools\Making\Interface_Making.h"
//엔피씨를 만드는 해더파일
#define NPC_MAKING_H "Game_Tools\Making\Npc_Making.h"
//맵를 만드는 해더파일
#define MAP_MAKING_H "Game_Tools\Making\Map_Making.h"
//플레이어의 아이템창을 만드는 해더파일
#define PLDATA_MAKING_H "Game_Tools\Making\PL_DataWindow_Making.h"
//대화창을 만드는 해더파일
#define TLALK_MAKING_H "Game_Tools\Making\TalkeWindow_Making.h"
//타일맵을 만드는 해더파일
#define TILEMAP_MAKING_H "Game_Tools\Making\TileMap_Making.h"

//게임의 이벤트들의 해더파일들을 저장한다.
#define MANGER_ROUTE "Game_Tools\Manager\"
//관리하는 해더파일
#define EVENT_MANAGER_H "Game_Tools\Manager\Event_Manager.h"
//관리하는 해더파일
#define LANGUAGE_MANAGER_H "Game_Tools\Manager\LanguageManager.h"
