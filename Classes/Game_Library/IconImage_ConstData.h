#pragma once

#include "Game_Library\Game_FileRoute.h"

#define ICON_ROUTE IMAGE_ROUTE"Icon/"

//Computer Icon
#define EMAIL_PNG	ICON_ROUTE"Email.png"
#define CUBE_PNG	ICON_ROUTE"Cube.png"
#define MESSAGE_PNG ICON_ROUTE"Messege.png"
#define FORDER_PNG	ICON_ROUTE"Forder.png"
#define SETTING_PNG ICON_ROUTE"Setting.png"

#define WINOPERATION_PNG ICON_ROUTE"WindowOperation.png"
#define WINCLOSE_PNG ICON_ROUTE"WindowClose.png"
#define WINPOWER_PNG ICON_ROUTE"WindowPower.png"
#define WINBACK_PNG  ICON_ROUTE"WinBackGround.png"

//computer UI
#define MOUSE_PNG	ICON_ROUTE"Mouse.png"

//Computer BackGround
#define MONITOR_PNG		ICON_ROUTE"Monitor.png"
//일단 화이트 박스로 대체한다.
#define MONITORSCREEN_PNG	WHITEBLOCK_PNG
