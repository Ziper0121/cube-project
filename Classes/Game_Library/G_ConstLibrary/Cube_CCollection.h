#pragma once

#include "Game_Library\Game_FileRoute.h" 

//    [1]
// [0][2][5][3]
//    [4]
#define CUBECOLOR_SIZE 6

//[0,0][0,1][0,2]
//[1,0][1,1][1,2]
//[2,0][2,1][2,2]
//큐브의 한면의 픽셀 길이
#define CUBE_STOOL 3

#define CUBEPIECE_SIZE 0.3f
	
//키보드 값
//큐브 잡는키 크기
#define GRABKEY_SIZE 3
//방향키 크기
#define MOVEKEY_SIZE 4

//X,Y의 최대 길이
#define X_MAX CUBE_STOOL
#define Y_MAX CUBE_STOOL

//CubeColor
#define COLOR_RED 0
#define COLOR_BLUE 1
#define COLOR_GREEN 2
#define COLOR_YELLOW 3
#define COLOR_WHITE 4
#define COLOR_PINK 5

//큐브에 마주본 숫자의 합
#define CUBEADDMAX_NUMBER 5

//큐브의 좌, 하 색깔 저장위치값
#define CENTER_COLOR 0
#define LEFT_COLOR 1
#define BOTTOM_COLOR 2

//전체 tag값
//큐브 tag
#define CUBE_TAG 1

//뮤직 모드
//큐브와 액션 tag값
#define MUSICCUBE_TAG CUBE_TAG
//큐브로가는 노트의 레일
#define MUSICRAIL_TAG 2
//출력되는 애니메이션 tag값
#define MUSICANI_TAG 3

//큐브 모드
//큐브의 액션과 Layout tag의 번호
#define CUBELAYER_TAG CUBE_TAG
//ui 액션과 Layout tag의 번호
#define QUIZMISSION_TAG 2
//ui 액션과 Layout tag의 번호
#define QUIZUI_TAG 3

//부속 tag값
//큐브Layout의 missioncube들의 tag값
#define CUBECUBER_TAG 1
//UILayout의 인터페이스들의 tag값
#define UITIMEOUT_TAG 1
#define UIBUTTON_TAG 2


enum Grab_Key{	GRAB_LT = 0	,GRAB_C	,GRAB_RB};
enum Cube_EnchoMove { RIGHT = 0, TOP };
enum CubeGameMode { CountClear = 0, Explanation };

struct Mix_CubeMap {
	Grab_Key Grab = Grab_Key::GRAB_LT;
	Cube_EnchoMove Encho = Cube_EnchoMove::RIGHT;
	int NumTime = 0;
};
