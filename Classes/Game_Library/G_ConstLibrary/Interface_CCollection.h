#pragma once

#include "Game_Library\Game_FileRoute.h"
#include "Game_Library\G_ConstLibrary\KeyBoard_CCollection.h" 

#define FILE_EMPTY_TEXT "File_Empty"

#define BACKGROUND_MUSIC_TEXT "Background_Music"
#define ACTION_SOUND_TEXT  "Action_Sound"
#define UI_SOUND_TEXT  "UI_Sound"

#define LANGUAGE_TEXT  "Use_Language"

#define WINDOW_SIZE_TEXT  "Window_Size"
#define FULL_SCREEN_TEXT  "Full_Screen"

#define GRAB_CUBE_LU_TEXT  "Grab_Cube_LT"
#define GRAB_CUBE_C_TEXT  "Grab_Cube_C"
#define GRAB_CUBE_DR_TEXT  "Grab_Cube_DR"
				
#define TURN_CUBE_R_TEXT  "Trun_Cube_R"
#define TURN_CUBE_L_TEXT  "Trun_Cube_L"

#define MOVE_R_TEXT  "Char_CubeMove_R"
#define MOVE_L_TEXT  "Char_CubeMove_L"
#define MOVE_D_TEXT  "Char_CubeMove_D"
#define MOVE_U_TEXT  "Char_CubeMove_T"
