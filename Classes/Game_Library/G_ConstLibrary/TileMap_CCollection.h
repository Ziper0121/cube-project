#pragma once

#include "Game_Library\Game_FileRoute.h"

#define M_ONE -1.0f
#define P_ONE 1.0f

#define ARR_FILL m_pressedArrow[m_emptyArrow - 1]

#define PRESS_ARR_FILL m_emptyArrow - 2

//움직이는 방향키 최대갯수
#define ARROW_KEY 4

//tile vluae
#define T_WALL 49
#define T_ITEM 50

//가장 늦게 누른 키보드값
#define FNISH_PREESE 3


#define DOWN_ANI 0
#define UP_ANI 1
#define LEVEL_ANI 2

#define TILE_PER (m_tileScale / m_winScale / 10)

//타일셋의 종류들
enum TileSetKind { Spawn_Tile = 1, Save_Tile, Npc_Tile, Event_Tile, Portal_Tile };

//Json map의 x y를 나타내는 것이다.
#define X_TEXT "x"
#define Y_TEXT "y"

//오브젝트 타일의 정보들
#define TILECODE_TEXT "TileCode"

#define NPCCODE_TEXT "NpcCode"

#define PTLLCODE_TEXT "PortallCode"
#define PTLLEXIT_TEXT "PortallExit"
#define PTLLMAP_TEXT "PortallMap"

#define NEXTTILE_TEXT "NextCode"

//캐릭터 카메라의 움직인 퍼센트 값
#define CHARVIEW_OVERPER 0.75f
#define CHARVIEW_UNDERPER  0.25f 

//TileSize
#define TILE_SIZE 16.0f
//TileMapScale
#define TILE_SCALE 3.0f

//tag값들
#define INFORMATION_TAG 2
#define PLAYER_TAG 1
#define TMX_TAG 1
#define TMXITEM_TAG 2
#define COMPUTER_TAG 3
#define NPC_TAG 3
#define EVENT_TAG 4
#define TALK_TAG 5
