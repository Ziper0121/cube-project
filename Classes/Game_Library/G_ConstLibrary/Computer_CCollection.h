#pragma once

#include "Game_Data\Player_Data.h" 
#include "Game_Library\Game_FileRoute.h" 

#define BUT_TAGNAME "Icon_TagName"

#define ICON_WIDTH 5


#define BACK_LAYOUT_NUM 1
#define MENU_LAYOUT_NUM 0

#define LABEL_SIZE 0.2f

#define ICON_TAGNAME "is Button"

#define ICON_LAYOUT "Icon_Layout"

//컴퓨터 버튼들 enum ComputerWinodw의 캐릭터화한것들

#define FIRSTSCENE_TEXT "FirstButton"
#define CUBEFIRSTSECNC_TEXT "CubeFirstButton"
#define BUSINISSCENE_TEXT "BusinessButton"
#define MESSEGESCENE_TEXT "MessegeButton"
#define FILESCENE_TEXT "FileButton"

#define OPERTATE_CUBE "OperatateCube"