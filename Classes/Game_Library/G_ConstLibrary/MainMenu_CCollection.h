#pragma once

#include "Game_Library\Game_FileRoute.h" 
#include "KeyBoard_CCollection.h"
//윈도우 최대사이즈

#define WINDOW_MAXSIZE 5 

//키보드
#define CUBE_LT_UINUM  0
#define CUBE_C_UINUM  1
#define CUBE_RB_UINUM  2

#define CUBETURN_R_UINUM  3
#define CUBETURN_L_UINUM  4

#define MOVEECHO_R_UINUM  5
#define MOVEECHO_L_UINUM  6	
#define MOVEECHO_D_UINUM  7
#define MOVEECHO_U_UINUM  8

//Main Scene 
#define MULTI_LAYER 1
#define TOOLS_CHECK 2

//Option
#define BASE_LAYOUT 3

//option window  정보들
#define SLIDER_ADDTAG 100

#define LAYOUT_ITEM 4
#define LAYOUT_TEXT 4

#define BACKMUSIC_TEXT "BackGround"
#define ACTSOUND_TEXT "Action"
#define UISOUND_TEXT "UI"

#define FULLSCREEN_TEXT "FullScreen"
#define BACK_TEXT "Back"
