#pragma once
#include <stdio.h>


#include "Game_Library\Game_FileRoute.h"
#include "Game_Data\Game_SceneData.h"

#define NON_EFFECT 0x0
#define TYPING_EFFECT 0x1
#define SCALE_EFFECT 0x2
#define TINT_EFFECT 0x4

#define SETLAYOUT_TAG 1
#define TALKTEXT_TAG 1
#define FACELAYOUT_TAG 2

typedef int WordEffect;

enum WordSegmentEvent{
	DelayEvent,ScaleEvent, SpeedEvent,
};

struct SegmentEvent {
	int FirstSegment;
	int EndSegment;
	WordSegmentEvent EventKind;
	float EventVlaue;
};
