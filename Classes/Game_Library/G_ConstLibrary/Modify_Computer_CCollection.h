#pragma once
#include"Game_Data\Player_Data.h"
#include "Game_Library\IconImage_ConstData.h"
#include "Game_Library\G_ConstLibrary\TileMap_CCollection.h"

#define BUT_TAGNAME "Icon_TagName"

#define CLICK_SOUND EFFECT_ROUTE"ButtonClick.mp3"
#define MOUSE_SOUND EFFECT_ROUTE"MouseMove.mp3"

#define ICON_WIDTH 5


#define BACK_LAYOUT_NUM 1
#define MENU_LAYOUT_NUM 0

#define LABEL_SIZE 0.2f

#define ICON_TAGNAME "is Button"

#define ICON_LAYOUT "Icon_Layout"
