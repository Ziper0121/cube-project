#pragma once

//****************************파일 경로****************************//
//file들의 파일위치
#define IMAGE_ROUTE "Image/"
#define OBJECTFACE_ROUTE	IMAGE_ROUTE"Face/"

//Item의 외형 파일 위치
#define ITEM_ROUTE IMAGE_ROUTE"Item/"

#define MUSICIMAGE_ROUTE IMAGE_ROUTE"MusicImage/"

//폰트 파일 파일위치
#define FONT_ROUTE "Fonts/"
//기본 인터페이스 폰트
#define FONT_NAME FONT_ROUTE"Joseon.ttf"

//데이터 파일들
#define GAMEDATA_ROUTE "Gaem_Data/"
#define ITEMDATA_ROUTE		GAMEDATA_ROUTE"Item_Data/"
#define	MAPNPCDATA_ROUTE		GAMEDATA_ROUTE"MapAndNpc_Data/"
#define MISSTIONDATA_ROUTE		GAMEDATA_ROUTE"Misstion_Data/"
#define PLAYERDATA_ROUTE		GAMEDATA_ROUTE"Player_Data/"
#define LANGUAGEDATA_ROUTE		GAMEDATA_ROUTE"Language_Data/"
#define MESSENGER_ROUTE			GAMEDATA_ROUTE"Messenger_Data/"
#define MESSENGER_FRIEN_ROUTE  GAMEDATA_ROUTE"Friend_Data/"
#define MUSIC_ROUTE  GAMEDATA_ROUTE"Music_Data/"


//음악관련 파일위치
#define SOUND_ROUTE "Sound/"
#define EFFECT_ROUTE SOUND_ROUTE "Effect_Sound/"
#define BACKMUSIC_ROUTE SOUND_ROUTE "BackGroundMusic/"

//맵관련 파일위치
#define TILEMAP_ROUTE "TileMap/"

//TMX맵관련 파일위치
#define TMX_ROUTE TILEMAP_ROUTE"TMX/"

//******************************************************************//


//*****************************대체 파일****************************//

//가장 많이 쓰는 대체용 이미지
#define WHITEBLOCK_PNG	IMAGE_ROUTE"WhiteBlock.png"

//****************************타일맵 파일****************************//

//----------------------------Json 파일----------------------------//

//대화 이벤트 
#define TALKEVENT_JSON "TalkEvent.json"

//----------------------------맵 파일----------------------------//

//테스트용 tmx
#define TILEMAP_TMX  TMX_ROUTE"TestOffice.tmx"

//----------------------------캐릭터 파일----------------------------//

//플레이어 png
#define PLAYER_PNG	 IMAGE_ROUTE"CubeItern.png"

//-----------------------------음악 파일-----------------------------//

//배경음악
#define MAPBACK_SOUND EFFECT_ROUTE"Steps_OneTic.mp3"
//tilemap 걷는 소리
#define PLSTEP_SOUND EFFECT_ROUTE"Steps_OneTic.mp3"

//******************************************************************//



//****************************컴퓨터 파일*****************************//
//아이콘 루트
#define ICON_ROUTE IMAGE_ROUTE"Icon/"

//-----------------------------이미지 파일-----------------------------//

//프로그램 창 이미지
#define FORDERBACK_PNG IMAGE_ROUTE"ComForderBack.png"

//어플 이미지들
#define EMAIL_PNG	ICON_ROUTE"Email.png"
#define CUBE_PNG	ICON_ROUTE"Cube.png"
#define MESSAGE_PNG ICON_ROUTE"Messege.png"
#define FORDER_PNG	ICON_ROUTE"Forder.png"
#define SETTING_PNG ICON_ROUTE"Setting.png"

//컴퓨터 내부 유아이들
#define WINOPERATION_PNG ICON_ROUTE"WindowOperation.png"
#define WINCLOSE_PNG ICON_ROUTE"WindowClose.png"
#define WINPOWER_PNG ICON_ROUTE"WindowPower.png"
#define WINBACK_PNG  ICON_ROUTE"WinBackGround.png"

//마우스 
#define MOUSE_PNG	ICON_ROUTE"Mouse.png"

//컴퓨터 화면
#define MONITOR_PNG		ICON_ROUTE"Monitor.png"
//일단 화이트 박스로 대체한다.
#define MONITORSCREEN_PNG	WHITEBLOCK_PNG

//-----------------------------음악 파일-----------------------------//

//UI소리
#define CLICK_SOUND EFFECT_ROUTE"ButtonClick.mp3"
#define MOUSE_SOUND EFFECT_ROUTE"MouseMove.mp3"

//큐브 돌리는 소리
#define CUBETURN_SOUND EFFECT_ROUTE"CubeTurn.mp3"

//큐브 클리어소리
#define CUBECLEAR_SOUND EFFECT_ROUTE"CubeClear.mp3"

//----------------------------큐브게임 파일----------------------------//

//큐브 피스 이미지
#define CUBE_PIECE_PNG IMAGE_ROUTE"CubePiece.png"
//큐브 뒷배경
#define CUBE_BACK_PNG IMAGE_ROUTE"CubeWindow.png"
//큐브 미션의 뒷배경
#define MIS_BACK_PNG IMAGE_ROUTE"MisstionBack.png"

//******************************************************************//

//******************************MPC 파일******************************//

//음악파일 틀
#define PLBACK_PNG IMAGE_ROUTE"WindowOperation.png"

//음악 이동버튼
#define MOVEMUSIC_PNG MUSICIMAGE_ROUTE"MoveMusic.png"
//음악 실행 버튼
#define PLAYBUTTON_PNG MUSICIMAGE_ROUTE"MusicPlay.png"
//음악 반복 재생
#define REPEATEBUTTON_PNG MUSICIMAGE_ROUTE"RepeatePlay.png"
//음악 프로필 사진들
#define CUBEPROFILE_PNG MUSICIMAGE_ROUTE"Cube.png"
#define EMAILPROFILE_PNG MUSICIMAGE_ROUTE"Email.png"

//******************************************************************//


//****************************대화 파일****************************//

//목소리
#define TALK_SOUND EFFECT_ROUTE"TemporaryTalk.mp3"

//******************************************************************//

//******************************UI 파일******************************//

//시스템 UI 
#define SLIDER_BAR_PNG ICON_ROUTE"SliderTrack.png"
#define SLIDE_THUMB_PNG ICON_ROUTE"SliderThumb.png"
#define CHECKBOX_ACT_PNG ICON_ROUTE"check_box_active_press.png"
#define CHECKBOX_NORMAL_PNG ICON_ROUTE"check_box_normal.png"

//UI png파일
#define SLIDER_BAR_PNG ICON_ROUTE"SliderTrack.png"
#define SLIDE_THUMB_PNG ICON_ROUTE"SliderThumb.png"
#define CHECKBOX_ACT_PNG ICON_ROUTE"check_box_active_press.png"
#define CHECKBOX_NORMAL_PNG ICON_ROUTE"check_box_normal.png"

//******************************************************************//

//******************************데이터 파일******************************//

//플레이어의 데이터
#define PL_QUESTFILE PLAYERDATA_ROUTE"PlayerGame_Data.txt"
#define PL_IMAGE IMAGE_ROUTE"CubeItern.png"

//데이터가 저장된 json 파일과 경로들
#define PLANNIGOFFICE_MAP_JSON		MAPNPCDATA_ROUTE"Plan2TeamOfficeMap_List.json"
#define BALCONY_MAP_JSON	MAPNPCDATA_ROUTE"BalconyMap_List.json"
#define TEST_MAP_JSON		MAPNPCDATA_ROUTE"Test_Map.json"


//게임내 데이터들
#define ITEM_JSON		ITEMDATA_ROUTE"ItemList.json"
#define FILE_JSON		ITEMDATA_ROUTE"FileList.json"
#define ADDFILE_JSON	ITEMDATA_ROUTE"AddFile.json"
#define ICON_JSON		ITEMDATA_ROUTE"ComputerIcon.json"
#define MISSION_JSON	MISSTIONDATA_ROUTE"MisstionList.json"
#define MESSENGER_JSON	MESSENGER_ROUTE"Messenger.json"
#define MUSIC_JSON	MUSIC_ROUTE"MusicList.json"

//언어
#define KO_JSON  LANGUAGEDATA_ROUTE"Ko.json"
#define EN_JSON	 LANGUAGEDATA_ROUTE"En.json"

//**********************************************************************//

//테스트
//******************************텍스쳐 파일******************************//

//말풍선 그림
#define BUBBLE_PNG IMAGE_ROUTE"Speech_Bubble.png"

//**********************************************************************//