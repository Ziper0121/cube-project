#pragma once
#include <cocos2d.h>
#include <ui\CocosGUI.h>

#include "Game_Data\Player_Data.h"

using namespace cocos2d;
using namespace cocos2d::ui;

#define MAILIST_TAG 0

class Email_Tool {
public:
	//뒷배경 리턴
	Layout* getBackGround();
private:
	//배경 만들기
	void m_createBackGround();
	void m_settingUI();
private:
	//메일의 가징 기본인 layout
	Layout* m_backGroundemail;

	//내가 선택한 메일
	int m_MailPoint;
};