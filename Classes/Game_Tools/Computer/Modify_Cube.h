#pragma once
#include <cocos2d.h>
#include <AudioEngine.h>

#include "Game_Data\Player_Data.h"
#include "Game_Library\G_ConstLibrary\Cube_CCollection.h" 
#include "Game_Tools\Manager\LanguageManager.h"

//#defube MUSIC_AND_QUIZEQUBE 

enum CubeToolScene { CubePlayScene, CubeOptionScene,CubeEndScene};

using namespace cocos2d;

struct CubeModule {
	int CubeColor[Y_MAX][X_MAX];
};
class Cube_Tool {
public:
	//------------------------------------------전달함수------------------------------------------

	//이메일을 만든다.

	//툴에 뒷배경을 받아온다. vecMap은  맞춰야하는 퍼즐이다.
	Layout* getBackGround(int ItemNum);

	//------------------------------------------조작 함수------------------------------------------

	//큐브의 키보드값을 조작하는 함수
	void cubeKeyPressed(EventKeyboard::KeyCode kcode);

private:
	//------------------------------------------생성함수------------------------------------------

	//큐브툴 생성
	void m_createCubeTool();

	//------------------------------------------설정함수------------------------------------------

	//큐브툴의 값들을 설정한다.
	void m_settingCubeValue();

	//Cube_Map
	bool m_setCubeMap(const std::vector<Mix_CubeMap>);

	//파리미터값 PreKey 눌린 위치, moveEncho 움직이는 위치
	void m_actionMoveCube(const int ConstGrab, const int moveKey);
	
	//아이템을 클리어할경우 저장한다.
	void m_saveItemClear();
	//------------------------------------------생성함수------------------------------------------

	//퀴즈 게임 생성
	void m_createQuizeModeGame();
	
	//큐브의 피스들의을 배치하는 함수
	void m_createCubeBox();

	//큐브의 좌우상하의 색깔들을 알려주는 라인을 그린다..
	void m_drawCubeLine();

	//큐브의 인터페이서 생성
	void m_createQuizUI();
	//큐브 미션들을 생성하는 함수
	void m_createQuizMission();

	//세팅( 재시작, 나가기, 취소 )
	void m_createOptionWindow();
	//------------------------------------------액션함수------------------------------------------

	//큐브를 움직일경우 다음 큐브를 생성하는 함수
	Sprite* m_actionReplaceCube(Vec3 CubeRotate, const int COLORNUM);

	//큐브액션 체크
	bool m_overlap_CubeActionCheck(int GrabConst, int MoveEncho);

	//옵션 포인트를 이동시킨다.
	bool m_cubeKeyBoardEvent(EventKeyboard::KeyCode kcode);

	//옵션의 움직이는 위치를 찾는다
	void m_optionPointMove(int Encho);
	//------------------------------------------미션함수------------------------------------------

	//미션을 클리어 여부를 true리턴
	bool m_checkCubeMission();

	//게임완료 화면
	void m_missionCompleteLayout();

	//게임 타임어택
	void m_cubeCountTime(float dt);

	//------------------------------------------제거함수------------------------------------------

	//게임종료
	void m_end_CubeTool();

	//게임을 종료시키는 함수
	void m_endSceneEvent(Ref*);

	//게임을 재시작하다.
	void m_restartCubeTool(Ref*);

	//큐브라인을 없앤다.
	void m_removeCubeLine();

private:
	Game_Player* m_playerData;

	//------------------------게임 시스템(모드)------------------------

	//큐브를 돌리는 액션없이 색깔만 변경하는 모드
	bool m_noActionMode;
	//게임 장면
	CubeToolScene m_toolScene = CubeToolScene::CubePlayScene;
	//현재 게임의 정보들
	Cube_Misstion m_cubeGameData;

	int m_exeFileNum = 0;
	//------------------------미션------------------------

	//미션의 완료 여부확인
	bool m_cubeMissionConplete = false;
	//게임 목표 [Y_MAX][X_MAX]
	std::vector<CubeModule> m_cubeAllMission;
	//현재 미션 위치
	int m_cubeMissionCount = 0;

	//------------------------시간------------------------

	//게임의 시간제한
	float m_missionTimeout = 0.0f;
	//사용한 시간
	float m_missionUsTime = 0.0f;

	//------------------------tool전체의 Layout------------------------

	//큐브 툴의 모든 Node들 가지고있는 Layout
	Layout* m_cubeToolLayout;

	//큐브 세팅배경
	Layout* m_optionLayout;

	//세팅할경우 마우스
	Sprite* m_optionPoint;
	//마우스 위치
	int m_PointNum = 0;
	//------------------------큐브 외형------------------------

	//큐브틀 변수
	Node* m_cubeFrame;

	//큐브 조각들의 변수 
	// [0,0][0,1][0,2]
	// [1,0][1,1][1,2]
	// [2,0][2,1][2,2]
	Sprite* m_cubeAllPiece[Y_MAX][X_MAX];


	//큐브 텍스쳐 이미지 
	Texture2D* m_cubePieceTexture;

	bool m_onCubeLine = false;
	//------------------------큐브의 색깔------------------------
	//    [3]      기본형태      
	// [1][0][4][5]	     
	//    [2] 큐브의 전체 색깔
	Color3B m_basicCubeColor[CUBECOLOR_SIZE];


	//큐브의 왼쪽과 아래쪽의 색깔 번호를 가지고있는 변수
	//0 중앙, 1 왼쪽, 2 아래쪽
	int m_cubeGetEncho[Y_MAX][X_MAX][CUBE_STOOL];

	//------------------------큐브 이동------------------------

	//이전 큐브의 AnchoPoint값
	Point m_earlyPoint[Y_MAX][X_MAX];

	//이전 값들
	//이전에 움직인방향의 첫번째 값을 저장 RIGHT = 0, LEFT = 1, BOTTOM = 2, TOP = 3
	int m_beforMove_X = 0;
	int m_beforMove_Y = 0;

	//이전에 잡아놓은 그랩 값을 저장 q = 0, w = 1, e = 2
	Grab_Key m_beforGrabe = Grab_Key::GRAB_LT;

	//키보드 검사를 위한 값들
	const EventKeyboard::KeyCode* const m_playerGrabeKey
		= Game_Player::getInstance()->getGameSetting()->m_grab_Cube;
	const EventKeyboard::KeyCode* const m_playerMoveKey
		= Game_Player::getInstance()->getGameSetting()->m_char_Cube_Move;

};

//vecMix로 큐브모듈값을 받는다.
std::vector<CubeModule> GetCubeModule(std::vector<Mix_CubeMap> vecMix);

//이동한 큐브의 외형을 바꿔주는함수
void CallActCubeCover(Sprite*);

//해당 번호의 샐깔을 정해주는 함수
Color3B getNumberColor(int color);
