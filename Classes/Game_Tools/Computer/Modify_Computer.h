#pragma once
#include <cocos2d.h>
#include <ui\CocosGUI.h>
#include <AudioEngine.h>


#include "Game_Library\G_ConstLibrary\Computer_CCollection.h" 
#include "Game_Tools\Manager\LanguageManager.h"
#include "Game_Tools\Making\Interface_Making.h" 
#include "Game_Tools\Computer\Modify_Cube.h" 

//컴퓨터의 아이콘 위치
enum ComputerWindow {
	FirstScene, CubeFirstScene, BusinessScene,
	MessegeScene, FileScene
};

using namespace cocos2d;

class Computer_Tool {
public:
	//컴퓨터의 backGround를 받아온다.
	Layout* getBackGround();

	//컴퓨터 키보드 이벤트
	void computerKeyPressed(EventKeyboard::KeyCode kcode);

private:

	//----------------------------기본씬 생성 함수----------------------------
	
	//컴퓨터 만들기
	void m_createComputer();

	//----------------------------제거함수----------------------------

	//툴 제거
	void m_removeTool();

	//----------------------------Scene만드는 함수----------------------------

	//아이콘과 마우스를 만든다.
	void m_iconAndMouseSetting(std::vector<Game_ComputerIcon> vecIcon
		, std::vector<Layout*> HorizonIcon);

	//----------------------------컴퓨터에 있는 씬 함수들----------------------------

	//바탕화면에 바로가기 창 만들기
	void m_createShortcutsWindow();
	//큐브 프로그램 화면
	void m_createCubeWindow();
	//받은 게임파일
	void m_createGetFileWindow();
	//큐브 프로그램 화면
	void m_createCubePlay();
	//컴퓨터에 저장된 파일들
	void m_createDownLoadFileWinodw();
	
	//----------------------------Button씬 이동 함수----------------------------

	//게임파일을 실행한다.
	void m_createBisinessCubeWindow(int NodeTag);

	//클릭한 버튼의 씬으로 이동
	void m_changeWindow(Ref*);

	//뒤로 씬 이동
	void m_backWindow();

	//프로그램 닫기
	void m_closeWindow();

	//----------------------------Keyboard Event----------------------------

	//데이터를 불러오면 자동으로 게임이 시작되는 방식, 게임을 시작하고 데이터를 받는 방식
	void m_ComputerKeyEvent(EventKeyboard::KeyCode kcode);
public:

	//모니터 백그라운드
	Layout* m_moniterBackground;
	//모니터
	Layout* m_windowOperate;
	//모니터 화면
	Layout* m_moniterScreen;

	//마우스
	Sprite* m_shortcutsPoint;

	//큐브툴
	Cube_Tool m_cubeWindow;
	
	//컴퓨터 창
	ComputerWindow m_computerWindow = FirstScene;

	//플레이어 데이타
	Game_Player* m_playerData;

	Size m_iconSize = Size(0.0f, 0.0f);
	//computer Sceen의 icon 위치
	int m_shortXCount = 0;

	int m_shortYCount = 0;
	int m_windowTag = 0;
};
