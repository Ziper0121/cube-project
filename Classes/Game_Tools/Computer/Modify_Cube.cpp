#include <ui\CocosGUI.h>

#include <json\ostreamwrapper.h>
#include <json\prettywriter.h>


#include "Game_Library\KeyboardMap.h"
#include "Game_Tools\Computer\Modify_Cube.h" 

//talkeWindow는 생성자에 Layer를 받는데 cube는 getLayout으로 전달을 한다.

using namespace cocos2d;
//-------------------------------------------CubeTool-------------------------------------------

//------------------------------------------외부전달함수------------------------------------------

//큐브툴을 전체 노드를 리턴한다.
Layout* Cube_Tool::getBackGround(int ItemNum) {
	m_cubeGameData = GetMissionData(ItemNum);
	m_createCubeTool();
	m_exeFileNum = ItemNum;
	return m_cubeToolLayout;
}


//게임 클리어 화면을 만든다.
void Cube_Tool::m_end_CubeTool() {
	
	Size ToolSize = m_cubeToolLayout->getContentSize();

	Sprite* ToolLayoutCube = Sprite::create(WHITEBLOCK_PNG);
	ToolLayoutCube->setPosition3D(Vec3(ToolSize.width, ToolSize.height, 0.0f));
	ToolLayoutCube->setAnchorPoint(Vec2::ANCHOR_TOP_LEFT);
	ToolLayoutCube->setRotation3D(Vec3(0.0f , 90.0f, 0.0f));
	ToolLayoutCube->setColor(Color3B::RED);
	ToolLayoutCube->setContentSize(ToolSize);
	m_cubeToolLayout->addChild(ToolLayoutCube);

	Sprite* LayoutCubeCube = Sprite::create(WHITEBLOCK_PNG);
	LayoutCubeCube->setAnchorPoint(Vec2::ANCHOR_TOP_LEFT);
	LayoutCubeCube->setRotation3D(Vec3(90.0f,0.0f, 0.0f));
	LayoutCubeCube->setColor(Color3B::BLUE);
	LayoutCubeCube->setContentSize(ToolSize);
	ToolLayoutCube->addChild(LayoutCubeCube);

	Text* FinishiText = Text::create("Finish", FONT_NAME, 25.0f);
	FinishiText->setAnchorPoint(Vec2(0.5f, 0.5f));
	FinishiText->setPosition(ToolSize *0.5f);
	FinishiText->setTextColor(Color4B::BLACK);
	LayoutCubeCube->addChild(FinishiText);
	
	m_cubeToolLayout->runAction(Sequence::create(DelayTime::create(0.5f)
		,RotateBy::create(1.0f, Vec3( 0.0f, -90.0f, 0.0f)), NULL));
	ToolLayoutCube->runAction(Sequence::create(DelayTime::create(1.5f)
		, RotateBy::create(1.0f, Vec3(-90.0f,0.0f, 0.0f))
		, CallFunc::create([&]() {m_toolScene = CubeToolScene::CubeEndScene;
	cocos2d::experimental::AudioEngine::stopAll();
	})
		, NULL));
	cocos2d::experimental::AudioEngine::setVolume(
	cocos2d::experimental::AudioEngine::play2d(CUBECLEAR_SOUND,true)
		,m_playerData->getGameSetting()->m_action_Sound * VOLUME_PS);
}

//게임을 종료시키는 함수
void Cube_Tool::m_endSceneEvent(Ref*) {
	m_playerData->setSceneInterface(SceneInterface::ComputerScene);
	m_cubeToolLayout->removeFromParent();
	m_optionPoint->release();
	m_cubeAllMission.clear();
	if (GetMissionData(m_exeFileNum).BusinessClear)
		return;
	m_saveItemClear();
}

//------------------------------------------조작 함수------------------------------------------

/*
하나의 메인게임와 여러가지 서브게임을 갖는다.
메인게임과 탐정물을 섞는다.

피하는 게임(리듬게임이 아니여도 된다.)
맞으러 가는 게임
원터치 게임
그냥 노트 맞추를 타이임에 맞추는 게임
원버튼으로 특정 규칙들의 맞추는 게임
위치 변경 [+][-][-] -> spacebar -> [-][+][-];
*/

void Cube_Tool::cubeKeyPressed(EventKeyboard::KeyCode kcode) {
	static int ConstGrabNum;
	if (m_cubeKeyBoardEvent(kcode))
		return;
	if (m_cubeMissionConplete) {
		if (m_toolScene == CubeToolScene::CubeEndScene) {
			m_endSceneEvent(nullptr);
		}
		return;
	}
	if (m_onCubeLine) {
		m_removeCubeLine();
		m_onCubeLine = false;
	}
	else if (kcode == EventKeyboard::KeyCode::KEY_H ) {
		m_drawCubeLine();
		m_onCubeLine = true;
	}
	
for (int i = 0; i < GRABKEY_SIZE; i++) {
		if (m_playerGrabeKey[i] != kcode)
			continue;
		CCLOG("KeyPressed(%s)", KeyBoardMap::getInstance()->getKeyCodeInStr(kcode).c_str());
		ConstGrabNum = i;
		return;
	}
	for (int i = 0; i < MOVEKEY_SIZE; i++) {
		if (m_playerMoveKey[i] != kcode)
			continue;
		CCLOG("KeyPressed(%s)", KeyBoardMap::getInstance()->getKeyCodeInStr(kcode).c_str());
		m_actionMoveCube(ConstGrabNum, i);
		return;
	}
}
//------------------------------------------설정함수------------------------------------------

//큐브툴의 설정값들을 초기화한다.
void Cube_Tool::m_settingCubeValue() {

	m_cubeMissionConplete = false;

	m_toolScene = CubeToolScene::CubePlayScene;

	m_noActionMode = false;

	m_cubeMissionCount = 0;

	m_missionTimeout = 0.0f;
	m_missionUsTime = 0.0f;

	//큐브 키보드 값 초기화
	//기본 키 세팅을 해놓는다.
	cubeKeyPressed(EventKeyboard::KeyCode::KEY_Q);
	m_beforGrabe = Grab_Key::GRAB_LT;
	m_beforMove_X = 0;
	m_beforMove_Y = 0;

	for (int y = 0; y < CUBE_STOOL; y++)
		for (int x = 0; x < CUBE_STOOL; x++)
			m_earlyPoint[y][x] = Vec2(0.0f, 0.0f);
}

//아이템을 클리어할경우 저장한다.
void Cube_Tool::m_saveItemClear() {

	Clear_Json CubeClear;
	CubeClear.JsonKind = JsonKind::Mission_Json;
	CubeClear.JsonFile = MISSION_JSON;
	CubeClear.JsonNumber.push_back(m_exeFileNum);

	m_playerData->addPLClear(CubeClear);
}
//------------------------------------------생성함수------------------------------------------

//큐브툴 생성함수
void Cube_Tool::m_createCubeTool() {

	/*
	Layout으로 만들고 모든 object를 ui로 만든다.  큐브의 블럭들을 어떡해 만들것인가?

	*/
	m_playerData = Game_Player::getInstance();
	Size WinSize = m_playerData->getGameSetting()->m_window_Size;

	cocos2d::experimental::AudioEngine::preload(CUBECLEAR_SOUND);

	cocos2d::experimental::AudioEngine::preload(CUBETURN_SOUND);
		m_cubePieceTexture = Director::getInstance()->getTextureCache()->
		addImage(CUBE_PIECE_PNG);

	m_settingCubeValue();

	//현재 큐브 색상을 결정한다.
	for (int i = 0; i < CUBECOLOR_SIZE; i++) {
		m_basicCubeColor[i] = getNumberColor(i);
	}

	//툴의 레이어
	m_cubeToolLayout = Layout::create();
	m_cubeToolLayout->setBackGroundImage(CUBE_BACK_PNG);
	m_cubeToolLayout->setBackGroundImageScale9Enabled(true);
	//m_cubeToolLayout = Node::create();
	m_cubeToolLayout->setContentSize(WinSize);
	m_cubeToolLayout->setAnchorPoint(Vec2::ANCHOR_BOTTOM_LEFT);

	m_optionPoint = Sprite::create(MOUSE_PNG);
	m_optionPoint->setAnchorPoint(Vec2::ANCHOR_BOTTOM_LEFT);
	m_optionPoint->retain();

	m_createQuizeModeGame();

}

//퀴즈 게임 생성
void Cube_Tool::m_createQuizeModeGame() {
	
	//미션을 알려주는 함수
	auto MissionLayout = Layout::create();
	m_cubeToolLayout->addChild(MissionLayout, 2);
	MissionLayout->setBackGroundImageScale9Enabled(true);
	MissionLayout->setBackGroundImage(MIS_BACK_PNG);
	MissionLayout->setContentSize(Size(m_cubeToolLayout->getContentSize().width*0.25f
		, m_cubeToolLayout->getContentSize().height));
	MissionLayout->setTag(QUIZMISSION_TAG);
	MissionLayout->setLayoutType(Layout::Type::VERTICAL);
	MissionLayout->setAnchorPoint(Vec2::ANCHOR_TOP_RIGHT);
	MissionLayout->setPosition(m_cubeToolLayout->getContentSize());

	//큐브 ([y][x]) 중앙에 배치한다.
	auto CubeFrameLayout = Node::create();
	CubeFrameLayout->setTag(CUBELAYER_TAG);
	CubeFrameLayout->setAnchorPoint(Vec2(0.5f, 0.5f));
	CubeFrameLayout->setContentSize(m_cubeToolLayout->getContentSize()* 0.45f);
	CubeFrameLayout->setPosition(m_cubeToolLayout->getContentSize() *0.5f);
	m_cubeToolLayout->addChild(CubeFrameLayout, 1);

	//인터페이스 (타이머)
	auto InterfaceLayout = Node::create();
	InterfaceLayout->setContentSize(
		Size(m_cubeToolLayout->getContentSize().width*0.1f, m_cubeToolLayout->getContentSize().height));
	InterfaceLayout->setTag(QUIZUI_TAG);
	InterfaceLayout->setColor(Color3B::BLACK);
	InterfaceLayout->setAnchorPoint(Vec2(0.0f, 0.0f));
	InterfaceLayout->setPosition(Vec2(0.0f, 0.0f));
	m_cubeToolLayout->addChild(InterfaceLayout, 3);

	m_createCubeBox();
	m_createQuizUI();
	m_createQuizMission();

}

//큐브를 생성하는함수
void Cube_Tool::m_createCubeBox() {

	Node* CubeLayer = m_cubeToolLayout->getChildByTag(CUBE_TAG);
	//큐브 틀 생성
	m_cubeFrame = Sprite::createWithTexture(m_cubePieceTexture);
	m_cubeFrame->setContentSize(CubeLayer->getContentSize());
	m_cubeFrame->setAnchorPoint(Vec2::ANCHOR_BOTTOM_LEFT);
	m_cubeFrame->setColor(Color3B::BLACK);
	CubeLayer->addChild(m_cubeFrame, 3);


	auto CubeMap = GetCubeModule(m_cubeGameData.OriginalCube);
	//큐브값 설정
	for (int i = 0; i < CUBE_STOOL; i++) {
		for (int y = 0; y < CUBE_STOOL; y++) {
			for (int x = 0; x < CUBE_STOOL; x++) {
				m_cubeGetEncho[y][x][i] = CubeMap.at(i).CubeColor[y][x];
			}
		}
	}

	Size CubeBackGroundSize = m_cubeFrame->getContentSize() * CUBEPIECE_SIZE;
	for (int y = 0; y < 3; y++) {
		int NodeHeight = 2 - y;
		for (int x = 0; x < 3; x++) {

			Vec2 NodeMargin = Vec2(CubeBackGroundSize.width *0.08f * (x + 1)
				, CubeBackGroundSize.height *0.08f * (NodeHeight + 1));

			Vec2 CubePosition = Vec2(x* CubeBackGroundSize.width, NodeHeight* CubeBackGroundSize.height);

			//큐브 조각 생성
			m_cubeAllPiece[y][x] = Sprite::createWithTexture(m_cubePieceTexture);
			m_cubeAllPiece[y][x]->setPosition(CubePosition + NodeMargin);
			m_cubeAllPiece[y][x]->setContentSize(CubeBackGroundSize);
			m_cubeAllPiece[y][x]->setColor(m_basicCubeColor[m_cubeGetEncho[y][x][CENTER_COLOR]]);
			m_cubeAllPiece[y][x]->setAnchorPoint(Vec2(0.0f, 0.0f));
			m_earlyPoint[y][x] = Vec2(0.0f, 0.0f);

			m_cubeFrame->addChild(m_cubeAllPiece[y][x], 1);
		}
	}
}

//큐브의 좌우상하의 색깔들을 알려주는 라인을 그린다..
void Cube_Tool::m_drawCubeLine() {
	for (int y = 0; y < Y_MAX; y++) {
		for (int x = 0; x < X_MAX; x++) {
			Vec2 CubePosition = m_cubeAllPiece[y][x]->getContentSize();
			Vec2 CuveOrginLine[4] = { Vec2(0.2f,0.5f), Vec2(0.5f,0.2f) 
				, Vec2(0.8f,0.5f),Vec2(0.5f,0.8f)};
			
			Color3B CubeLineColor[4] = {
			getNumberColor(m_cubeGetEncho[y][x][LEFT_COLOR])
			,getNumberColor(m_cubeGetEncho[y][x][BOTTOM_COLOR])
			,getNumberColor(CUBEADDMAX_NUMBER - m_cubeGetEncho[y][x][LEFT_COLOR])
			,getNumberColor(CUBEADDMAX_NUMBER - m_cubeGetEncho[y][x][BOTTOM_COLOR])
			};
			DrawNode* CubeLine = DrawNode::create();
			for (int i = 0; i < 4; i++) {
				CCLOG("Line : %f %f", CuveOrginLine[i].x * CubePosition.x
					, CuveOrginLine[i].y * CubePosition.y);
				CubeLine->drawLine(	Vec2(CuveOrginLine[i].x * CubePosition.x
						, CuveOrginLine[i].y * CubePosition.y), CubePosition*0.5f
					, Color4F(CubeLineColor[i]));
				CubeLine->setLineWidth(4.0f);
			}
			m_cubeAllPiece[y][x]->addChild(CubeLine);
		}
	}
}

void Cube_Tool::m_removeCubeLine() {
	for (int y = 0; y < Y_MAX; y++) {
		for (int x = 0; x < X_MAX; x++) {
			m_cubeAllPiece[y][x]->removeAllChildren();
		}
	}
}

//큐브의 인터페이서 생성(타이머)(리셋버튼)
void Cube_Tool::m_createQuizUI() {
	auto InterfaceLayer = m_cubeToolLayout->getChildByTag(QUIZUI_TAG);

	Size WinSize = Director::getInstance()->getWinSize();

	char CurrentTime[8] = { 0 };
	sprintf(CurrentTime, "%4.1f", m_missionTimeout);

	auto TimeoutFont = Label::create(CurrentTime, FONT_NAME
		, InterfaceLayer->getContentSize().width * 0.4);
	TimeoutFont->setPosition(Vec2(0.0f, WinSize.height));
	TimeoutFont->setAnchorPoint(Vec2(0.0f, 1.0f));
	TimeoutFont->setTextColor(Color4B::WHITE);
	TimeoutFont->setTag(UITIMEOUT_TAG);
	InterfaceLayer->addChild(TimeoutFont);
}
//큐브의 미션 생성
void Cube_Tool::m_createQuizMission() {
	//미션레이어
	auto MissionLayout = m_cubeToolLayout->getChildByTag(QUIZMISSION_TAG);

	//큐브틀의 사이즈
	Size CubeSize = MissionLayout->getContentSize();
	CubeSize.height = CubeSize.width;

	//큐브조각 사이즈
	Size CubePieceSize = CubeSize * 0.6f;

	const int CubeHeight = 2;
	Vec2 DrawBoxPosition = CubeSize* 0.025f;
	for (auto i = m_cubeGameData.Business.begin(); i < m_cubeGameData.Business.end(); i++) 
		m_cubeAllMission.push_back(GetCubeModule(*i).at(CENTER_COLOR));
	

	for (int i = 0; i < m_cubeAllMission.size(); i++) {

		ImageView* MissionCubeFrame = ImageView::create(WHITEBLOCK_PNG);
		MissionLayout->addChild(MissionCubeFrame);
		MissionCubeFrame->ignoreContentAdaptWithSize(false);
		MissionCubeFrame->setContentSize(CubePieceSize);
		MissionCubeFrame->setAnchorPoint(Vec2::ANCHOR_MIDDLE_TOP);
		MissionCubeFrame->setTag(i);
		MissionCubeFrame->setColor(Color3B::BLACK);

		LinearLayoutParameter* MissionLayout = LinearLayoutParameter::create();
		MissionLayout->setGravity(LinearGravity::CENTER_HORIZONTAL);
		MissionLayout->setMargin(Margin(0.0f, CubeSize.height * 0.1f,0.0f, CubeSize.height * 0.1f));
		MissionCubeFrame->setLayoutParameter(MissionLayout);

		Sprite* MissionCubeCover = Sprite::createWithTexture(m_cubePieceTexture);
		MissionCubeFrame->addChild(MissionCubeCover, 1);
		MissionCubeCover->setAnchorPoint(Vec2::ANCHOR_BOTTOM_LEFT);
		MissionCubeCover->setContentSize(MissionCubeFrame->getContentSize());
		MissionCubeCover->setColor(Color3B::BLACK);
		MissionCubeCover->setOpacity(0.0f);
		MissionCubeCover->setTag(CUBECUBER_TAG);

		if (0 < i) {
			MissionCubeCover->setOpacity(150.0f);
		}

		DrawBoxPosition = CubePieceSize* 0.025f;


		DrawNode* DrawPiece = DrawNode::create();
		MissionCubeFrame->addChild(DrawPiece, 0);

		for (int y = 0; y < Y_MAX; y++) {
			for (int x = 0; x < X_MAX; x++) {
				DrawPiece->drawSolidRect(DrawBoxPosition
					, DrawBoxPosition + CubePieceSize * CUBEPIECE_SIZE
					, Color4F(m_basicCubeColor[m_cubeAllMission.at(i).CubeColor[CubeHeight - y][x]]));
				
				DrawBoxPosition.x += CubePieceSize.width * 0.325f;
			}
			DrawBoxPosition.x = CubePieceSize.width * 0.025f;
			DrawBoxPosition.y += CubePieceSize.height*0.325f;
		}
	}
}

//세팅( 재시작, 나가기, 취소 )
void Cube_Tool::m_createOptionWindow() {
	m_toolScene = CubeOptionScene;
	Size BackSize = m_cubeToolLayout->getContentSize();
	m_optionLayout = Layout::create();
	m_cubeToolLayout->addChild(m_optionLayout, 3);
	m_optionLayout->setPosition(BackSize *0.5f);
	m_optionLayout->setAnchorPoint(Vec2::ANCHOR_MIDDLE);
	m_optionLayout->setLayoutType(Layout::Type::HORIZONTAL);
	m_optionLayout->setBackGroundColorType(LayoutBackGroundColorType::SOLID);
	m_optionLayout->setBackGroundColor(Color3B::GRAY);
	m_optionLayout->setBackGroundImageScale9Enabled(true);
	m_optionLayout->setContentSize(Size(BackSize.width*0.4f, 1.0f));

	std::vector<std::string> ButtonName = { u8"닫기",u8"재시작",u8"종료" };
	std::vector<std::function<void(Ref*)>> ButtonClickEvent = { [&](Ref*) {
		m_optionLayout->removeFromParent();
		m_toolScene = CubeToolScene::CubePlayScene;
	},std::bind(&Cube_Tool::m_restartCubeTool,this,nullptr)
		,std::bind(&Cube_Tool::m_endSceneEvent,this,nullptr)};
	for (int i = 0; i < 3; i++){
		Button* optionButtons = Button::create(CUBE_PIECE_PNG);
		m_optionLayout->addChild(optionButtons);
		optionButtons->ignoreContentAdaptWithSize(false);
		optionButtons->setContentSize(
			Size(m_optionLayout->getContentSize().width*0.4f
				, m_optionLayout->getContentSize().width*0.4f));
		optionButtons->setTitleFontName(FONT_NAME);
		optionButtons->setTitleText(ButtonName[i]);
		optionButtons->setTitleColor(Color3B::BLACK);
		optionButtons->setTag(i);
		optionButtons->addClickEventListener(ButtonClickEvent[i]);
	}
	m_optionLayout->getChildByTag(0)->addChild(m_optionPoint);
}

//------------------------------------------액션함수------------------------------------------

//큐브를 움직이는 함수
void Cube_Tool::m_actionMoveCube(const int ConstGrab, const int moveKey) {

	//액션 관련 변수

	// [Y][x]에 Y값
	int y = 0, x = 0;
	//변수에 증가값
	int Max_X = 0, Max_Y = 0;

	//Cube가 움직일 액션 값
	Vec3 CubeMoveTrun = Vec3(0.0f, 0.0f, 0.0f);

	//Rotate값이 anchoPoint의 값에 영향을 받기 때문에 변경해줘야한다.
	Vec2 CubeAnchoPoint = Vec2(0.0f, 0.0f);
	//Rotate값을 변경하였으니 그만큼 node에 Position값을 변경한다.
	Size CubeConstSize = Size(0.0f, 0.0f);
	//큐브의 애니메이션은 Z값을 가지고 움직이기에 변경한다.
	const float CUBETRUENAGO = 90.0f;

	//이전의 색깔번호를 기억하는 변수
	int CubeCenterColorNum;

	if (m_overlap_CubeActionCheck(ConstGrab, moveKey))
		return;

	/*
	w,e를 누른상태에서 상하를 누를경우 색깔변화가없다.
	*/

	int MoveColorEncho = 0;
	int OppositeEncho[2] = { 0,0 };

	if (moveKey < 2 && moveKey >= 0) {
		Max_X = 1;
		y = ConstGrab;
		CubeMoveTrun = Vec3(0.0f, CUBETRUENAGO, 0.0f);

		MoveColorEncho = LEFT_COLOR;
	}
	else {
		//액션 for루프 변수
		Max_Y = 1;
		x = ConstGrab;
		CubeMoveTrun = Vec3(CUBETRUENAGO, 0.0f, 0.0f);

		MoveColorEncho = BOTTOM_COLOR;
	}

	OppositeEncho[0] = m_cubeGetEncho[y][x][MoveColorEncho] * 2;
	OppositeEncho[1] = m_cubeGetEncho[y][x][CENTER_COLOR] * 2;

	switch (moveKey)
	{
	case 0:
		OppositeEncho[1] = CUBEADDMAX_NUMBER;
		CubeAnchoPoint = Vec2(1.0f, 1.0f);
		CubeConstSize = m_cubeFrame->getContentSize() *CUBEPIECE_SIZE;
		break;
	case 1:
		OppositeEncho[0] = CUBEADDMAX_NUMBER;
		CubeMoveTrun.y *= -1;
		CubeAnchoPoint = Vec2(0.0f, 0.0f);
		CubeConstSize = m_cubeFrame->getContentSize()*-CUBEPIECE_SIZE;
		break;
	case 2:
		OppositeEncho[0] = CUBEADDMAX_NUMBER;
		CubeAnchoPoint = Vec2(0.0f, 0.0f);
		CubeConstSize = m_cubeFrame->getContentSize()*-CUBEPIECE_SIZE;
		break;
	case 3:
		CubeMoveTrun.x *= -1;
		OppositeEncho[1] = CUBEADDMAX_NUMBER;
		CubeAnchoPoint = Vec2(1.0f, 1.0f);
		CubeConstSize = m_cubeFrame->getContentSize() *CUBEPIECE_SIZE;
		break;
	default:
		break;
	}

	float dt = 0.25f;

	m_beforMove_X = Max_X;
	m_beforMove_Y = Max_Y;
	RotateBy* CubeRotate = RotateBy::create(dt, CubeMoveTrun);

	for (; x < Max_X*CUBE_STOOL || y < Max_Y*CUBE_STOOL;
		x += Max_X, y += Max_Y) {

		OppositeEncho[0] == 5 ? OppositeEncho[1] = m_cubeGetEncho[y][x][CENTER_COLOR] * 2 : OppositeEncho[0] = m_cubeGetEncho[y][x][MoveColorEncho] * 2;

		CubeCenterColorNum = OppositeEncho[0] - m_cubeGetEncho[y][x][MoveColorEncho];
		m_cubeGetEncho[y][x][MoveColorEncho]
			= OppositeEncho[1] - m_cubeGetEncho[y][x][CENTER_COLOR];
		m_cubeGetEncho[y][x][CENTER_COLOR] = CubeCenterColorNum;

		if (m_noActionMode) {
			m_cubeAllPiece[y][x]->setColor(
				m_basicCubeColor[m_cubeGetEncho[y][x][CENTER_COLOR]]);
			continue;
		}

		if (m_earlyPoint[y][x] != CubeAnchoPoint) {
			m_cubeAllPiece[y][x]->setPosition(m_cubeAllPiece[y][x]->getPosition() + CubeConstSize);
			m_cubeAllPiece[y][x]->setAnchorPoint(CubeAnchoPoint);
			//이동하기전의 엔초포인트 값을 받는다.
			m_earlyPoint[y][x] = CubeAnchoPoint;
		}

		m_cubeAllPiece[y][x]->addChild(
			m_actionReplaceCube(CubeMoveTrun, m_cubeGetEncho[y][x][CENTER_COLOR]));

		CallFunc* CubeSwapFun = CallFunc::create(std::bind(&CallActCubeCover, m_cubeAllPiece[y][x]));
		Sequence* CubeAllAction = Sequence::create(CubeRotate->clone(), CubeSwapFun, NULL);
		CubeAllAction->setTag(CUBELAYER_TAG);
		m_cubeAllPiece[y][x]->runAction(CubeAllAction);
		cocos2d::experimental::AudioEngine::setVolume(
		cocos2d::experimental::AudioEngine::play2d(CUBETURN_SOUND)
			, m_playerData->getGameSetting()->m_action_Sound*VOLUME_PS);
	}


	//미션을 클리했는지 확인
	if (!m_checkCubeMission()) 
		return;
	
	m_end_CubeTool();
}
//큐브 중복액션 체크
bool Cube_Tool::m_overlap_CubeActionCheck(int GrabConst, int MoveEncho) {

	int x = 0, y = 0;
	m_beforMove_X < m_beforMove_Y ? y = GrabConst : x = GrabConst;

	//같은 그랩위치일경우를 검사
	if (m_beforGrabe == GrabConst) {
		if (m_cubeAllPiece[GrabConst][GrabConst]->getActionByTag(CUBELAYER_TAG)) {
			CCLOG("CubePiece[%d][%d] is play Action", y, x);
			m_beforGrabe = (Grab_Key)GrabConst;
			return true;
		}
	}
	//서로 엇갈리는 방향일경우를 검사. 그리고 befor은 전의 액션이기때문에 반전을해줘야한다.
	//LEFT RIGHT = 0,1 = false | BOTTOM, TOP = 2,3 = true
	else if (m_beforMove_X < m_beforMove_Y ? 1 : 2 !=
		MoveEncho < 2 ? 1 : 2) {
		for (; x < m_beforMove_Y *CUBE_STOOL || y < m_beforMove_X*CUBE_STOOL
			; x += m_beforMove_Y, y += m_beforMove_X) {
			if (m_cubeAllPiece[y][x]->getActionByTag(CUBELAYER_TAG)) {
				CCLOG("CubePiece[%d][%d] Crose Action", y, x);
				return true;
			}
		}
	}

	
	return false;
}
//큐브가 전환하는 반대쪽 큐브픽스를 생성하는 함수
Sprite* Cube_Tool::m_actionReplaceCube(Vec3 CubeRotate, const int COLORNUM) {

	Vec2 CubeAnchoPoint = Vec2(0.0f, 0.0f);

	Size CubePieceSize = m_cubeAllPiece[0][0]->getContentSize();
	Vec3 Cube3DPosition = Vec3(0.0f, 0.0f, -CubePieceSize.width);

	if (CubeRotate.y < 0 || CubeRotate.x > 0) {
		CubeAnchoPoint = Vec2(1.0f, 1.0f);
		Cube3DPosition.x = CubePieceSize.width;
		Cube3DPosition.y = CubePieceSize.height;
	}

	CubeRotate *= -1;

	Sprite* AnimationSprite = Sprite::createWithTexture(m_cubePieceTexture);
	AnimationSprite->setPosition3D(Cube3DPosition);
	AnimationSprite->setContentSize(CubePieceSize);
	AnimationSprite->setColor(m_basicCubeColor[COLORNUM]);
	AnimationSprite->setAnchorPoint(CubeAnchoPoint);
	AnimationSprite->setRotation3D(CubeRotate);

	return AnimationSprite;
}

//옵션 포인트를 이동시킨다.
bool Cube_Tool::m_cubeKeyBoardEvent(EventKeyboard::KeyCode kcode) {

	switch (m_toolScene)
	{
	case CubePlayScene:
		if (kcode == EventKeyboard::KeyCode::KEY_X) {
			m_createOptionWindow();
			return true;
		}
		return false;
	case CubeOptionScene:
		switch (kcode)
		{
		case EventKeyboard::KeyCode::KEY_LEFT_ARROW:
			m_optionPointMove(-1);
			break;
		case EventKeyboard::KeyCode::KEY_RIGHT_ARROW:
			m_optionPointMove(+1);
			break;
		case EventKeyboard::KeyCode::KEY_Z:
			m_optionLayout->removeFromParent();
			switch (m_PointNum)
			{
			case 0:
				m_toolScene = CubeToolScene::CubePlayScene;
				break;
			case 1:
				m_restartCubeTool(nullptr);
				break;
			case 2:
				m_endSceneEvent(nullptr);
				break;
			default:
				break;
			}
			break;
		case EventKeyboard::KeyCode::KEY_X:
			m_toolScene = CubeToolScene::CubePlayScene;
			m_optionLayout->removeFromParent();
			break;
		default:
			return false;
		}
		break;
	case CubeEndScene:
		CCLOG("cube End");
		m_endSceneEvent(nullptr);
		break;
	default:
		break;
	}
	return true;
}
//옵션의 움직이는 위치를 찾는다
void Cube_Tool::m_optionPointMove(int Encho) {
	m_PointNum += Encho;
	int ButtonCount = m_optionLayout->getChildrenCount();
	if (m_PointNum < 0) m_PointNum += ButtonCount;
	else if (m_PointNum >= ButtonCount)m_PointNum -= ButtonCount;
	if (m_PointNum < 0 && m_PointNum >= ButtonCount)
		m_PointNum = ButtonCount - 1;

	m_optionPoint->removeFromParent();
	m_optionLayout->getChildByTag(m_PointNum)->addChild(m_optionPoint);

}

//------------------------------------------미션관련함수------------------------------------------

//큐브의 구조를 배치하는 함수
bool Cube_Tool::m_setCubeMap(const std::vector<Mix_CubeMap> CUBE_MAP) {

	//모든 큐브가 정면을 보게 초기화
	for (int y = 0; y < CUBE_STOOL; y++) {
		for (int x = 0; x < CUBE_STOOL; x++) {
			for (int i = 0; i < CUBE_STOOL; i++) {
				m_cubeGetEncho[y][x][i] = i;
			}
		}
	}

	if (CUBE_MAP.empty())
		return false;

	const int X = 0;
	const int Y = 1;

	int CubeCenterColorNum = 0;

	for (auto CMap = CUBE_MAP.begin(); CMap < CUBE_MAP.end(); CMap++)
	{
		switch (CMap->Encho)
		{
		case Cube_EnchoMove::RIGHT:
			for (int i = 0; i < CMap->NumTime; i++) {
				for (int x = 0; x < X_MAX; x++) {
					CubeCenterColorNum = m_cubeGetEncho[CMap->Grab][x][LEFT_COLOR];
					m_cubeGetEncho[CMap->Grab][x][LEFT_COLOR] = CUBEADDMAX_NUMBER - m_cubeGetEncho[CMap->Grab][x][CENTER_COLOR];
					m_cubeGetEncho[CMap->Grab][x][CENTER_COLOR] = CubeCenterColorNum;
				}
			}
			break;
		case Cube_EnchoMove::TOP:
			for (int i = 0; i < CMap->NumTime; i++) {
				for (int y = 0; y < Y_MAX; y++) {
					CubeCenterColorNum = m_cubeGetEncho[y][CMap->Grab][BOTTOM_COLOR];
					m_cubeGetEncho[y][CMap->Grab][BOTTOM_COLOR] = CUBEADDMAX_NUMBER - m_cubeGetEncho[y][CMap->Grab][CENTER_COLOR];
					m_cubeGetEncho[y][CMap->Grab][CENTER_COLOR] = CubeCenterColorNum;
				}
			}
			break;
		default:
			break;
		}
	}

	m_cubeGetEncho;

	return true;
}
//미션을 완료 여부를 true리턴
bool Cube_Tool::m_checkCubeMission() {

	m_cubeMissionConplete = false;

	//미션 클리어 여부 확인
	for (int y = 0; y < Y_MAX; y++) {
		for (int x = 0; x < X_MAX; x++) {
			if (m_cubeAllMission.at(m_cubeMissionCount).CubeColor[y][x]
				!= m_cubeGetEncho[y][x][CENTER_COLOR]) {
				return m_cubeMissionConplete;
			}
		}
	}

	//클리어한 미션은 삭제한다.
	m_cubeToolLayout->getChildByTag(QUIZMISSION_TAG)->
		removeChildByTag(m_cubeMissionCount);

		m_cubeMissionCount++;
	if (m_cubeAllMission.size() > m_cubeMissionCount) {
		auto One_MissionComplete = m_cubeToolLayout->getChildByTag(QUIZMISSION_TAG)
			->getChildByTag(m_cubeMissionCount)->getChildByTag(CUBECUBER_TAG);
		One_MissionComplete->runAction(FadeOut::create(2.0f));

		return m_cubeMissionConplete;
	}

	if (!m_cubeGameData.BusinessClear){
		m_playerData->addPLItem(m_cubeGameData.Cube_Reward);
		std::vector<Game_Object> ObjectItem = {
			m_playerData->getHavItem().back() };
		AddItemBox(ObjectItem);
	}

	m_cubeMissionConplete = true;
	CCLOG("Cube Mission ALLLLL is Complete");
	return m_cubeMissionConplete;

}
//미션을 클리어하면 나오는 화면을 만드는 함수
/*
void Cube_Tool::m_missionCompleteLayout() {

	m_toolScene = CubeToolScene::CubeEndScene;
	Layout* BackGroundLayout = Layout::create();
	BackGroundLayout->setLayoutType(LayoutType::VERTICAL);
	BackGroundLayout->setContentSize(m_cubeToolLayout->getContentSize());
	BackGroundLayout->setBackGroundColorType(Layout::BackGroundColorType::SOLID);
	BackGroundLayout->setBackGroundColor(Color3B::GRAY);
	BackGroundLayout->setAnchorPoint(Vec2::ANCHOR_BOTTOM_LEFT);
	m_cubeToolLayout->getChildByTag(QUIZUI_TAG)->addChild(BackGroundLayout, 2, 2);

	Text* FinishiText = Text::create("Finish", FONT_NAME, 25.0f);
	FinishiText->setAnchorPoint(Vec2(0.5f, 0.5f));
	BackGroundLayout->addChild(FinishiText);

	LinearLayoutParameter* LayoutUIParameter = LinearLayoutParameter::create();
	LayoutUIParameter->setGravity(LinearGravity::CENTER_VERTICAL);

	FinishiText->setLayoutParameter(LayoutUIParameter);

}
*/
void Cube_Tool::m_missionCompleteLayout(){}
//------------------------------------------제거함수------------------------------------------

//큐브 재시작
void Cube_Tool::m_restartCubeTool(Ref*) {
	m_toolScene = CubeToolScene::CubePlayScene;

	m_cubeToolLayout->removeAllChildren();

	m_cubeAllMission.clear();
	m_settingCubeValue();
	m_createQuizeModeGame();

}

//--------------------------------------CubueTool 외부사용 함수--------------------------------------

//클리어시간을 정해놓고 차감하는 방식
void Cube_Tool::m_cubeCountTime(float dt) {
	if (m_missionTimeout < m_missionUsTime || m_missionTimeout == 0.0f || m_cubeMissionConplete)
		return;

	m_missionUsTime += dt;

	if (m_missionTimeout < m_missionUsTime)
		m_missionCompleteLayout();

	char RimainTime[7] = { 0 };
	sprintf(RimainTime, "%4.1f", m_missionTimeout - m_missionUsTime);

	auto Timeout = (Label*)m_cubeToolLayout->getChildByTag(QUIZUI_TAG)->getChildByTag(UITIMEOUT_TAG);
	Timeout->setString(RimainTime);
}
//------------------------------------------외부함수------------------------------------------

//vecMix로 큐브모듈값을 받는다.
std::vector<CubeModule> GetCubeModule(std::vector<Mix_CubeMap> vecMix) {
	
	int CubeCenterColorNum = 0;
	std::vector<CubeModule> TransCubes = std::vector<CubeModule>(3);
	for (int i = 0; i < CUBE_STOOL; i++) {
		for (int y = 0; y < CUBE_STOOL; y++) {
			for (int x = 0; x < CUBE_STOOL; x++) {
				TransCubes.at(i).CubeColor[y][x] = i;
			}
		}
	}
	for (auto CMap = vecMix.begin(); CMap < vecMix.end(); CMap++)
	{
		switch (CMap->Encho)
		{
		case Cube_EnchoMove::RIGHT:
			for (int i = 0; i < CMap->NumTime; i++) {
				for (int x = 0; x < X_MAX; x++) {
					CubeCenterColorNum = TransCubes.at(LEFT_COLOR).CubeColor[CMap->Grab][x];
					TransCubes.at(LEFT_COLOR).CubeColor[CMap->Grab][x]
						= CUBEADDMAX_NUMBER
						- TransCubes.at(CENTER_COLOR).CubeColor[CMap->Grab][x];
					TransCubes.at(CENTER_COLOR).CubeColor[CMap->Grab][x] = CubeCenterColorNum;
				}
			}
			break;
		case Cube_EnchoMove::TOP:
			for (int i = 0; i < CMap->NumTime; i++) {
				for (int y = 0; y < Y_MAX; y++) {
					CubeCenterColorNum = TransCubes.at(BOTTOM_COLOR).CubeColor[y][CMap->Grab];
					TransCubes.at(BOTTOM_COLOR).CubeColor[y][CMap->Grab]
						= CUBEADDMAX_NUMBER
						- TransCubes.at(CENTER_COLOR).CubeColor[y][CMap->Grab];
					TransCubes.at(CENTER_COLOR).CubeColor[y][CMap->Grab] = CubeCenterColorNum;
				}
			}
			break;
		default:
			break;
		}
	}
	return TransCubes;
}

//이동한 큐브의 외형을 바꿔주는함수
void CallActCubeCover(Sprite* OriginNode) {
	OriginNode->setColor(OriginNode->getChildren().back()->getColor());
	OriginNode->setRotation3D(Vec3(0.0f, 0.0f, 0.0f));
	OriginNode->removeAllChildren();
}
Color3B getNumberColor(int color) {
	switch (color)
	{
	case COLOR_RED:
		CCLOG("Node Color Red");
		return Color3B::RED;
	case COLOR_BLUE:
		CCLOG("Node Color Blue");
		return Color3B::BLUE;
	case COLOR_GREEN:
		CCLOG("Node Color Green");
		return Color3B::GREEN;
	case COLOR_YELLOW:
		CCLOG("Node Color Yellow");
		return Color3B::YELLOW;
	case COLOR_WHITE:
		CCLOG("Node Color white");
		return Color3B::WHITE;
	case COLOR_PINK:
		CCLOG("Node Color Pink");
		return Color3B(255.0f,105.0f,180.0f);
	default:
		CCLOG("Node Color Black");
		return Color3B::BLACK;
	}
}
