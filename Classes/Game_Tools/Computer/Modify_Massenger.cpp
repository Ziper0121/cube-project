
#include  "Game_Tools\Computer\Modify_Massenger.h" 


//메신저를 리턴한다.
Layout* Massenger_Tool::getMaseengerBackGround() {
	m_createBackground();

	return m_background;
}

//키보드 이벤트
void Massenger_Tool::MassengerKeyboardEvent(EventKeyboard::KeyCode KCode) {

}
//메신저 뒷배경만들기
void Massenger_Tool::m_createBackground() {
	m_playerData = Game_Player::getInstance();
	Size WinSize = m_playerData->getGameSetting()->m_window_Size * 0.4f;

	m_background = Layout::create();
	m_background->setContentSize(Size(WinSize.width* 0.7f, WinSize.height));

	Layout* MessengeBakc = Layout::create();
	m_background->addChild(MessengeBakc);
	MessengeBakc->setContentSize(m_background->getContentSize());
	MessengeBakc->setLayoutType(Layout::Type::HORIZONTAL);
	MessengeBakc->setAnchorPoint(Vec2::ANCHOR_BOTTOM_LEFT);
	MessengeBakc->setBackGroundColorType(LayoutBackGroundColorType::SOLID);
	MessengeBakc->setBackGroundColor(Color3B::WHITE);

	char* ButtonTitle[2] = { "친구","대화" };
    Vec2 MassengerBackAncho[2] ={ 
		Vec2::ANCHOR_TOP_LEFT ,Vec2::ANCHOR_TOP_RIGHT };
	Size BackGroundSize = MessengeBakc->getContentSize();
	for (int i = 0; i < 2; i++) {
		//친구목록을 볼지 대화방을 볼지 선택하는 버튼
		Button* ChoiseWinodw = Button::create(WHITEBLOCK_PNG);
		MessengeBakc->addChild(ChoiseWinodw);
		ChoiseWinodw->ignoreContentAdaptWithSize(false);
		ChoiseWinodw->setTitleText(ButtonTitle[i]);
		ChoiseWinodw->setContentSize(Size(
			BackGroundSize.width*0.5f, BackGroundSize.height*0.05f));

		LinearLayoutParameter* ButtonLayoutParameter;
		ButtonLayoutParameter->setGravity(LinearGravity::TOP);
		ChoiseWinodw->setLayoutParameter(ButtonLayoutParameter);

		m_profileBack.push_back(Layout::create());
		ChoiseWinodw->addChild(m_profileBack.back());
		m_profileBack.back()->setContentSize(Size(
			BackGroundSize.width, BackGroundSize.height*0.95));
		m_profileBack.back()->setAnchorPoint(MassengerBackAncho[i]);
		m_profileBack.back()->setLayoutType(Layout::Type::VERTICAL);
		m_profileBack.back()->setBackGroundColorType(LayoutBackGroundColorType::SOLID);
		m_profileBack.back()->setBackGroundColor(Color3B::WHITE);
	}
	m_createTalkRoomWinodw();
	m_createFirstWinodw();
}

//메신저를 친구목록
void Massenger_Tool::m_createFirstWinodw(){
	m_massengerWinodw = MassengerWinodw::FriendsWindow;
	auto FreindList =m_playerData->getMessegerFreind();
	
	Size BoxSize = m_profileBack.at(0)->getContentSize();
	BoxSize.height =BoxSize.height * 0.2;
	for (auto i = FreindList.begin(); i < FreindList.end(); i++){
		Layout* ProfileBox = Layout::create();
		m_profileBack.at(0)->addChild(ProfileBox);
		ProfileBox->setContentSize(BoxSize);
		ProfileBox->setLayoutType(Layout::Type::HORIZONTAL);
		ProfileBox->setBackGroundColor(Color3B::WHITE);
		ProfileBox->setBackGroundColorType(Layout::BackGroundColorType::SOLID);

		ImageView* ProfileFace = ImageView::create((*i).Body);
		ProfileBox->addChild(ProfileFace);
		ProfileFace->ignoreContentAdaptWithSize(false);
		ProfileFace->setContentSize(Size(BoxSize.height , BoxSize.height));
		
		for (int k = 0; k < 2; k++) {
			Text* ProfileName = Text::create(
				(*i).Name, FONT_NAME, BoxSize.height * 0.5f);
			ProfileBox->addChild(ProfileName);
			//ProfileName->ignoreContentAdaptWithSize(false);
			ProfileName->setTextAreaSize(Size(
				BoxSize.width - BoxSize.height, BoxSize.height*0.5f));

			Text* ProfileInformation= Text::create(
				(*i).Name, FONT_NAME, BoxSize.height * 0.3f);
			ProfileName->addChild(ProfileInformation);
			ProfileInformation->setAnchorPoint(Vec2::ANCHOR_TOP_LEFT);
			//ProfileInformation->ignoreContentAdaptWithSize(false);
			ProfileInformation->setTextAreaSize(Size(
				BoxSize.width - BoxSize.height, BoxSize.height*0.5f));

		}

	}

}

//누군가와 대화창
void Massenger_Tool::m_createTalkRoomWinodw() {
	m_massengerWinodw = MassengerWinodw::FriendsWindow;
	auto MessegeList = m_playerData->getMessege();

	Size BoxSize = m_profileBack.at(0)->getContentSize();
	BoxSize.height = BoxSize.height * 0.2;
	for (auto i = MessegeList.begin(); i < MessegeList.end(); i++) {
		Layout* ProfileBox = Layout::create();
		m_profileBack.at(0)->addChild(ProfileBox);
		ProfileBox->setContentSize(BoxSize);
		ProfileBox->setLayoutType(Layout::Type::HORIZONTAL);
		ProfileBox->setBackGroundColor(Color3B::WHITE);
		ProfileBox->setBackGroundColorType(Layout::BackGroundColorType::SOLID);

		ImageView* ProfileFace 
			= ImageView::create((*i).RoomTalk.back().SayObjectTexture);
		ProfileBox->addChild(ProfileFace);
		ProfileFace->ignoreContentAdaptWithSize(false);
		ProfileFace->setContentSize(Size(BoxSize.height, BoxSize.height));

		for (int k = 0; k < 2; k++) {
			Text* ProfileName = Text::create(
				(*i).RoomTalk.back().TalkText, FONT_NAME, BoxSize.height * 0.4f);
			ProfileBox->addChild(ProfileName);
			//ProfileName->ignoreContentAdaptWithSize(false);
			ProfileName->setTextAreaSize(Size(
				BoxSize.width - BoxSize.height, BoxSize.height));
		}

	}


}


//프로필 상세 정보
void Massenger_Tool::m_createProfileInformation(int FriendNum) {
	if (FriendNum >= m_playerData->getMessegerFreind().size())
		assert(false);

	Size BackSize = m_background->getContentSize();
	auto FreindList = m_playerData->getMessegerFreind().at(FriendNum);
	
	Layout* InformationBack = Layout::create();
	m_background->addChild(InformationBack);
	InformationBack->setContentSize(BackSize);
	InformationBack->setLayoutType(Layout::Type::HORIZONTAL);
	InformationBack->setBackGroundColor(Color3B::WHITE);
	InformationBack->setAnchorPoint(Vec2::ANCHOR_BOTTOM_LEFT);

	ImageView* ProfileFace = ImageView::create(FreindList.Body);
	InformationBack->addChild(ProfileFace);
	ProfileFace->ignoreContentAdaptWithSize(false);
	ProfileFace->setContentSize(Size(BackSize.width	, BackSize.width));
	
	
	const char* FreindInformation[2]
		= { FreindList.Name.c_str(), FreindList.Body.c_str() };
	float FontSize[2] = { BackSize.height*0.1f, BackSize.height*0.05f };
	for (int i = 0; i < 2; i++) {
		Text* ProfileData = Text::create(FreindList.information, FONT_NAME
			, FontSize[i]);
		InformationBack->addChild(ProfileData);
		ProfileData->setTextColor(Color4B::BLACK);
		ProfileData->setTextHorizontalAlignment(TextHAlignment::CENTER);

		LinearLayoutParameter* ProfileMargin = LinearLayoutParameter::create();
		ProfileData->setLayoutParameter(ProfileMargin);
		ProfileMargin->setMargin(
			Margin(0.0f, BackSize.height*0.02f, 0.0f, BackSize.height*0.02f));
		ProfileMargin->setGravity(LinearGravity::CENTER_HORIZONTAL);
	}
	InformationBack->getChildren().back()->setContentSize(
		Size(BackSize.width, BackSize.width - BackSize.height));

}