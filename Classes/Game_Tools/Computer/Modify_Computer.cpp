
#include "Game_Tools\Computer\Modify_Computer.h"

//컴퓨터의 backGround를 받아온다.
Layout* Computer_Tool::getBackGround() {
	m_createComputer();

	return m_moniterBackground;
}

//----------------------------키보드 이벤트----------------------------

//keyboard값
void Computer_Tool::computerKeyPressed(EventKeyboard::KeyCode kcode) {
	if (m_playerData->getSceneInterface().pl_currentScene == CubeGameScene) {
		m_cubeWindow.cubeKeyPressed(kcode);
		return;
	}
	return;
	int ShortCount = m_shortcutsPoint->getReferenceCount();
	auto ShortParent = m_shortcutsPoint->getParent();
	m_shortcutsPoint->removeFromParent();

	Node* NodeChil = m_moniterBackground->getChildren().back();
	std::vector<Node*> IconLayout;
	if (m_moniterBackground->getChildrenCount() > 0) {
		//아이콘이 나열된 Layout의 childent들의 갯수들의 사이즈를 잰다.
		for (auto i = NodeChil->getChildren().begin(); i < NodeChil->getChildren().end(); i++) {
			CCLOG("Layout Name : %s", (*i)->getName().c_str());
			if ((*i)->getName().compare(ICON_LAYOUT) == 0) {
				for (auto k = (*i)->getChildren().begin(); k < (*i)->getChildren().end(); k++) {
					IconLayout.push_back((*k));
				}
				break;
			}
		}
	}

	
	switch (kcode)
	{
	case EventKeyboard::KeyCode::KEY_LEFT_ARROW:
		m_shortXCount--;
		break;
	case EventKeyboard::KeyCode::KEY_RIGHT_ARROW:
		m_shortXCount++;
		break;
	case EventKeyboard::KeyCode::KEY_UP_ARROW:
		m_shortYCount--;
		break;
	case EventKeyboard::KeyCode::KEY_DOWN_ARROW:
		m_shortYCount++;
		break;
	case EventKeyboard::KeyCode::KEY_Z:
		CCLOG("shortCount : %d", m_shortXCount);
		if (ShortCount > 1)
			m_changeWindow(ShortParent);
		return;
	case EventKeyboard::KeyCode::KEY_X:
		m_backWindow();
		return;
	default:
		return;
	}

	if (IconLayout.size() == 0)
		return;

	if (m_shortYCount < 0)
		m_shortYCount = IconLayout.size() - 1;
	else if (m_shortYCount >= IconLayout.size())
		m_shortYCount = 0;
	if (m_shortYCount < 0 || m_shortYCount >= IconLayout.size())
		assert(false);

	//최대(소)사이즈를 넘을경우
	if (m_shortXCount < 0) {
		--m_shortYCount;
		if (m_shortYCount < 0)
			m_shortYCount = IconLayout.size() - 1;
		m_shortXCount = IconLayout.at(m_shortYCount)->getChildrenCount() -1;
	}
	else if (m_shortXCount >= IconLayout.at(m_shortYCount)->getChildrenCount()) {
		++m_shortYCount;
		if (m_shortYCount >= IconLayout.size())
			m_shortYCount = 0;
		m_shortXCount = 0;
	}
	if (m_shortXCount < 0 || m_shortXCount >= IconLayout.at(m_shortYCount)->getChildrenCount())
		assert(false);
	cocos2d::experimental::AudioEngine::setVolume(
	cocos2d::experimental::AudioEngine::play2d(MOUSE_SOUND)
	, Game_Player::getInstance()->getGameSetting()->m_ui_Sound*VOLUME_PS);
	IconLayout.at(m_shortYCount)->getChildren().at(m_shortXCount)
		->addChild(m_shortcutsPoint);
}

//----------------------------Button씬 이동 함수----------------------------

//클릭한 버튼의 씬으로 이동
void Computer_Tool::m_changeWindow(Ref* ref) {
	//다른 장면일경우 못들어가게한다.
	if (m_playerData->getSceneInterface().pl_currentScene != ComputerScene)
		return;

	auto ClickButton = ((Button*)ref);
	char* CompareName = "\0";
	
	/*
	
	switch (m_playerData->getSceneInterface().pl_currentScene)
	{
	case StarScene:
		break;
	case ConScene:
		break;
	case SetScene:
		break;
	case TileScene:
		break;
	case SaveScene:
		break;
	case TalkScene:
		break;
	case InforScene:
		break;
	case ComputerScene:
		break;
	case CubeGameScene:
		break;
	default:
		break;
	}
	*/

	switch (m_computerWindow)
	{
	case FirstScene: {
		if (ClickButton->getName().compare(FIRSTSCENE_TEXT))
			break;
		m_moniterBackground->getChildren().back()->removeFromParent();
		m_createCubeWindow();

		//프로그램 아이콘
		ImageView* ProgramIcon = ImageView::create(CUBE_PNG);
		m_windowOperate->addChild(ProgramIcon);
		ProgramIcon->ignoreContentAdaptWithSize(false);
		ProgramIcon->setContentSize(
			m_moniterBackground->getContentSize() * 0.1f);
		ProgramIcon->setName("OperatateCube");

		break;
	}
	case CubeFirstScene: 
		if (ClickButton->getName().compare(CUBEFIRSTSECNC_TEXT))
			break;
		m_moniterBackground->getChildren().back()->removeFromParent();
		m_createGetFileWindow();

		break;
	case BusinessScene:
		if (ClickButton->getName().compare(BUSINISSCENE_TEXT))
			break;
		m_createBisinessCubeWindow(ClickButton->getTag());
		break;
	case MessegeScene:
		if (ClickButton->getName().compare(MESSEGESCENE_TEXT))
			break;
		m_moniterBackground->getChildren().back()->removeFromParent();
		break;
	case FileScene:
		if (ClickButton->getName().compare(FILESCENE_TEXT))
			break;
		m_moniterBackground->getChildren().back()->removeFromParent();
		break;
	default:
		break;
	}
	return;
	auto ParentTag = ((Button*)ref)->getParent()->getParent()->getParent()->getTag();
	CCLOG("Tag  = %d ", ParentTag);
	if (ParentTag != m_windowTag)
		return;
	cocos2d::experimental::AudioEngine::setVolume(
		cocos2d::experimental::AudioEngine::play2d(CLICK_SOUND)
		, Game_Player::getInstance()->getGameSetting()->m_ui_Sound*VOLUME_PS);
	
	int IconCount = m_shortXCount + m_shortYCount * ICON_WIDTH;
	switch (m_computerWindow)
	{
		//바탕화면에 배치된 아이콘들의 종류
	case ComputerWindow::FirstScene:
		switch (m_playerData->getHavIcon().at(m_shortXCount).Icon_Kind)
		{
		case Computer_IconKind::Icon_Cube:
			m_createCubeWindow();
			break;
		case Computer_IconKind::Icon_Messenger:
			m_createDownLoadFileWinodw();
			break;
		default:
			break;
		}
		break;
		//큐브씬일경우
	case ComputerWindow::CubeFirstScene:
		switch (((Node*)ref)->getTag())
		{
		case 0:
			m_createGetFileWindow();
			break;
			m_createCubePlay();
			break;
		case 1:
			m_createGetFileWindow();
			break;
		default:
			break;
		}
		break;
	case ComputerWindow::BusinessScene:
		if (m_playerData->getSceneInterface().pl_currentScene
			 !=SceneInterface::CubeGameScene)
		m_createBisinessCubeWindow(((Node*)ref)->getTag());
		
		break;
	default:
		break;
	}
}

//뒤로 씬 이동
void Computer_Tool::m_backWindow() {

	if (m_playerData->getSceneInterface().pl_currentScene != ComputerScene)
		return;

	m_moniterBackground->getChildren().back()->removeFromParent();
	switch (m_computerWindow)
	{
	case CubeFirstScene:
		m_windowOperate->removeChildByName(OPERTATE_CUBE);
		m_createShortcutsWindow();
		break;
	case BusinessScene:
		m_createCubeWindow();
		break;
	case MessegeScene:
		break;
	case FileScene:
		break;
	default:
		break;
	}
	return;

	cocos2d::experimental::AudioEngine::setVolume(
		cocos2d::experimental::AudioEngine::play2d(CLICK_SOUND)
		, Game_Player::getInstance()->getGameSetting()->m_ui_Sound*VOLUME_PS);
	m_shortXCount = 0;
	m_shortYCount = 0;
	m_windowTag--;

	//마우스 만들기
	m_moniterBackground->getChildren().back()->removeFromParent();
	if (m_moniterBackground->getChildrenCount() > 0) {
		Node* NodeChil = m_moniterBackground->getChildren().back();
		//아이콘이 나열된 Layout의 childent들의 갯수들의 사이즈를 잰다.
		for (auto i = NodeChil->getChildren().begin(); i < NodeChil->getChildren().end(); i++) {
			CCLOG("Layout Name : %s", (*i)->getName().c_str());
			if ((*i)->getName().compare(ICON_LAYOUT) != 0)
				continue;
			if ((*i)->getChildrenCount() == 0)
				break;
			Node* ChildNod = (*i)->getChildren().at(0);
			if (ChildNod->getChildrenCount() > 0)
				ChildNod->getChildren().at(0)->addChild(m_shortcutsPoint);
			break;
		}
	}

	switch (m_computerWindow)
	{
	case ComputerWindow::FirstScene:
		m_removeTool();
		break;
	case ComputerWindow::CubeFirstScene:
	case ComputerWindow::FileScene:
		m_computerWindow = ComputerWindow::FirstScene;
		break;
	case ComputerWindow::BusinessScene:
		m_computerWindow = ComputerWindow::CubeFirstScene;
		break;
	default:
		break;
	}

}

//프로그램 닫기
void Computer_Tool::m_closeWindow() {

	if (m_playerData->getSceneInterface().pl_currentScene != ComputerScene)
		return;

	m_moniterBackground->getChildren().back()->removeFromParent();

	//작업줄의 아이콘 표시를 없앤다.
	switch (m_computerWindow)
	{
	case ComputerWindow::CubeFirstScene:
	case ComputerWindow::BusinessScene:
		m_windowOperate->removeChildByName(OPERTATE_CUBE);
		break;
	default:
		break;
	}

	m_computerWindow = CubeFirstScene;
	m_createShortcutsWindow();
}

/*
버튼 클릭시
키보드 입력시
*/
//툴제거
void Computer_Tool::m_removeTool() {
	m_moniterBackground->removeFromParent();
	
	m_shortcutsPoint->release();
	m_moniterBackground = nullptr;
	m_shortcutsPoint = nullptr;
	m_playerData->setSceneInterface(SceneInterface::TileScene);
	cocos2d::experimental::AudioEngine::uncache(CLICK_SOUND);
	cocos2d::experimental::AudioEngine::uncache(MOUSE_SOUND);
}

//컴퓨터 만들기
void Computer_Tool::m_createComputer() {

	m_playerData = Game_Player::getInstance();
	auto WinSize = m_playerData->getGameSetting()->m_window_Size;

	cocos2d::experimental::AudioEngine::preload(CLICK_SOUND);
	cocos2d::experimental::AudioEngine::preload(MOUSE_SOUND);
	
	m_moniterBackground = Layout::create();
	m_moniterBackground ->setContentSize(WinSize);



	m_shortcutsPoint = Sprite::create(MOUSE_PNG);
	m_shortcutsPoint->setAnchorPoint(Vec2::ANCHOR_BOTTOM_LEFT);
	m_shortcutsPoint->retain();

	m_createShortcutsWindow();
}


//----------------------------Scene만드는 함수----------------------------

//icon과 버튼을 만드는 함수
void Computer_Tool::m_iconAndMouseSetting(std::vector<Game_ComputerIcon> vecIcon
	, std::vector<Layout*> HorizonIcon){
	
	Size MoniterScreenSize = m_moniterBackground->getContentSize();
	Size OperationSize = Size(MoniterScreenSize.width, MoniterScreenSize.height*0.1f);
	
	//배경화면
	Layout* WinodwBackGround = Layout::create();
	m_moniterBackground->addChild(WinodwBackGround);
	WinodwBackGround->setName(ICON_LAYOUT);
	WinodwBackGround->setBackGroundImageScale9Enabled(true);
	WinodwBackGround->setBackGroundColorType(LayoutBackGroundColorType::SOLID);
	WinodwBackGround->setBackGroundImageColor(Color3B::WHITE);
	WinodwBackGround->setContentSize(Size(MoniterScreenSize.width,
		MoniterScreenSize.height - OperationSize.height * 2.0f));
	WinodwBackGround->setPosition(OperationSize);
	WinodwBackGround->setAnchorPoint(Vec2::ANCHOR_BOTTOM_RIGHT);

	//프로그램 위에 있는잡업표시줄
	Layout* WindowOperation = Layout::create();
	WinodwBackGround->addChild(WindowOperation);
	WindowOperation->setBackGroundImage(WINOPERATION_PNG);
	WindowOperation->setBackGroundImageScale9Enabled(true);
	WindowOperation->setContentSize(OperationSize);
	WindowOperation->setPosition(WinodwBackGround->getContentSize());
	WindowOperation->setAnchorPoint(Vec2::ANCHOR_BOTTOM_RIGHT);
	WindowOperation->setLayoutType(Layout::Type::HORIZONTAL);

	//버튼에 이름을 넣는다.
	char* ButtonName;
	char* ProgramImageFile; 
	char* ProgramName;
	switch (m_computerWindow)
	{
	case CubeFirstScene:
		ButtonName = CUBEFIRSTSECNC_TEXT;
		ProgramImageFile = CUBE_PNG;
		ProgramName = "Cube";
		break;
	case BusinessScene:
		ButtonName = BUSINISSCENE_TEXT;
		ProgramImageFile = CUBE_PNG;
		ProgramName = "Cube";
		break;
	case MessegeScene:
		ButtonName = MESSEGESCENE_TEXT;
		break;
	case FileScene:
		ButtonName = FILESCENE_TEXT;
		break;
	default:
		break;
	}

	float OperateIcon = WindowOperation->getContentSize().height;

	//뒤로가기버튼
	Button* WindowBackButton = Button::create(WINCLOSE_PNG);
	WindowOperation->addChild(WindowBackButton);
	WindowBackButton->ignoreContentAdaptWithSize(false);
	WindowBackButton->setContentSize(Size(OperateIcon, OperateIcon));
	WindowBackButton->addClickEventListener(
		std::bind(&Computer_Tool::m_backWindow, this));

	//프로그램 아이콘
	ImageView* ProgramIcon = ImageView::create(ProgramImageFile);
	WindowOperation->addChild(ProgramIcon);
	ProgramIcon->ignoreContentAdaptWithSize(false);
	ProgramIcon->setContentSize(Size(OperateIcon, OperateIcon));
	//프로그램 이름
	Text* ProgramNameText = Text::create(ProgramName,FONT_NAME,OperateIcon -1);
	WindowOperation->addChild(ProgramNameText);
	ProgramNameText->setTextAreaSize(Size(OperateIcon * 7,OperateIcon));
	
	//프로그램 닫기 버튼
	Button* WindowCloseButton = Button::create(WINCLOSE_PNG);
	WindowOperation->addChild(WindowCloseButton);
	WindowCloseButton->ignoreContentAdaptWithSize(false);
	WindowCloseButton->setContentSize(Size(OperateIcon, OperateIcon));
	WindowCloseButton->addClickEventListener(
		std::bind(&Computer_Tool::m_closeWindow, this));
	

	
	//아이콘이 없을경우 화면 IconWindow를 안 만든다.
	if (vecIcon.size() == 0 && HorizonIcon.size() == 0)
		return;

	auto PlayerIcon = vecIcon.begin();
	std::string BodyFile;
	Size IconSize = HorizonIcon.at(0)->getContentSize() * 0.8f;
	IconSize.width = IconSize.height;
	
	for (int i = 0; i < HorizonIcon.size(); i++) {
		WinodwBackGround->addChild(HorizonIcon.at(i));
		for (int k = 0; k < ICON_WIDTH  && PlayerIcon != vecIcon.end(); k++, PlayerIcon++) {
			//파일의 경로가 맞는지 확인한다.
			BodyFile = (*PlayerIcon).Body;
			if (BodyFile.find(IMAGE_ROUTE) == std::string::npos)
				BodyFile = ICON_ROUTE + (*PlayerIcon).Body;


			Button* shortcuts = Button::create(BodyFile);
			HorizonIcon.at(i)->addChild(shortcuts);
			shortcuts->ignoreContentAdaptWithSize(false);
			shortcuts->setContentSize(IconSize);
			shortcuts->setAnchorPoint(Vec2::ANCHOR_TOP_LEFT);
			shortcuts->addClickEventListener(
				std::bind(&Computer_Tool::m_changeWindow, this, std::placeholders::_1));
			shortcuts->setTag(i * 5 + k);
			shortcuts->setName(ButtonName);

			Label* ShortLabel = Label::create((*PlayerIcon).Name, FONT_NAME
				, shortcuts->getContentSize().width*LABEL_SIZE);
			shortcuts->addChild(ShortLabel);
			ShortLabel->setAnchorPoint(Vec2::ANCHOR_MIDDLE_TOP);
			ShortLabel->setTextColor(Color4B::BLACK);
			ShortLabel->setMaxLineWidth(shortcuts->getContentSize().width);
			ShortLabel->setAlignment(TextHAlignment::CENTER);
			ShortLabel->setPosition(Size(IconSize.width*0.5f, 0.0f));
		}
		if (PlayerIcon == vecIcon.end())
			break;
	}

	m_shortXCount = 0;

	Node* ChildChildChild = WinodwBackGround->getChildren().at(0);
	
	while (true) {
		if (ChildChildChild->getChildrenCount() == 0)
			return;
		ChildChildChild = ChildChildChild->getChildren().at(0);
		if (ChildChildChild->getName().compare(ICON_TAGNAME) == 0)
			break;
	}

	ChildChildChild->addChild(m_shortcutsPoint);

}

//----------------------------컴퓨터에 있는 씬 함수들----------------------------

//바탕화면에 바로가기 창 만들기
void Computer_Tool::m_createShortcutsWindow() {
	m_computerWindow = ComputerWindow::FirstScene;

	Size MoniterScreenSize = m_moniterBackground->getContentSize();
	Size OperationSize = Size(MoniterScreenSize.width, MoniterScreenSize.height*0.1f);
	
	//작얼줄 표시
	m_windowOperate = Layout::create();
	m_moniterBackground->addChild(m_windowOperate);
	m_windowOperate->setBackGroundImage(WINOPERATION_PNG);
	m_windowOperate->setBackGroundImageScale9Enabled(true);
	m_windowOperate->setLayoutType(Layout::Type::HORIZONTAL);
	m_windowOperate->setContentSize(OperationSize);

	Button* WindowPower = Button::create(WINPOWER_PNG);
	m_windowOperate->addChild(WindowPower);
	WindowPower->ignoreContentAdaptWithSize(false);
	WindowPower->setContentSize(Size(OperationSize.height, OperationSize.height));
	WindowPower->addClickEventListener(
		std::bind(&Computer_Tool::m_removeTool, this));

	//배경화면
	Layout* WinodwBackGround = Layout::create();
	m_moniterBackground->addChild(WinodwBackGround);
	WinodwBackGround->setName(ICON_LAYOUT);
	WinodwBackGround->setBackGroundImageScale9Enabled(true);
	WinodwBackGround->setBackGroundColorType(LayoutBackGroundColorType::SOLID);
	WinodwBackGround->setBackGroundImageColor(Color3B::WHITE);
	WinodwBackGround->setContentSize(Size(MoniterScreenSize.width,
		MoniterScreenSize.height - OperationSize.height));
	WinodwBackGround->setPosition(OperationSize);
	WinodwBackGround->setAnchorPoint(Vec2::ANCHOR_BOTTOM_RIGHT);
	
	//아이콘 생성
	std::vector<Layout*> HorizonIcon;
	auto BackIcon = m_playerData->getHavIcon();
	auto PlayerIcon = BackIcon.begin();
	std::string BodyFile;
	float IconSize = WinodwBackGround->getContentSize().height * 0.2f;

	//아이콘이 없을경우 화면 IconWindow를 안 만든다.
	if (BackIcon.size() == 0)
		return;

	//바탕화면에 아이콘 배치
	for (int i = 0; i < 4; i++) {
		HorizonIcon.push_back(Layout::create());
		WinodwBackGround->addChild(HorizonIcon.back());
		HorizonIcon.back()->setLayoutType(LayoutType::HORIZONTAL);
		HorizonIcon.back()->setAnchorPoint(Vec2::ANCHOR_TOP_LEFT);
		HorizonIcon.back()->setContentSize(
			Size(WinodwBackGround->getContentSize().width, IconSize * 1.2f));
		HorizonIcon.back()->setPosition(Vec2(0.0f, IconSize * 1.2f *( 4 - i)));
		for (int k = 0; k < ICON_WIDTH  && PlayerIcon != BackIcon.end(); k++, PlayerIcon++) {
			BodyFile = (*PlayerIcon).Body;
			if (BodyFile.find(IMAGE_ROUTE) == std::string::npos)
				BodyFile = ICON_ROUTE + (*PlayerIcon).Body;

			Button* shortcuts = Button::create(BodyFile);
			shortcuts->setContentSize(Size(IconSize, IconSize));
			shortcuts->ignoreContentAdaptWithSize(false);
			shortcuts->setAnchorPoint(Vec2::ANCHOR_TOP_LEFT);
			shortcuts->addClickEventListener(
				std::bind(&Computer_Tool::m_changeWindow, this, std::placeholders::_1));
			shortcuts->setTag(i * 5 + k);
			shortcuts->setName(FIRSTSCENE_TEXT);
			HorizonIcon.back()->addChild(shortcuts);

			Label* ShortLabel = Label::create((*PlayerIcon).Name, FONT_NAME
				, IconSize*LABEL_SIZE);
			ShortLabel->setMaxLineWidth(IconSize);
			ShortLabel->setAnchorPoint(Vec2::ANCHOR_MIDDLE_TOP);
			ShortLabel->setTextColor(Color4B::BLACK);
			ShortLabel->setPosition(Size(IconSize*0.5f, 0.0f));
			shortcuts->addChild(ShortLabel);
		}
		if (PlayerIcon == BackIcon.end())
			break;
	}

	m_shortXCount = 0;
	m_shortcutsPoint->removeFromParent();

	//Icon의 마우스 배치하기 at(0)인이유는 0이 operation이 뒤에 있기 때문이다.

	Node* ChildChildChild = m_windowOperate->getChildren().at(0);

	//Icon의 마우스 배치하기
	while (true) {
		if (ChildChildChild->getChildrenCount() == 0)
			return;
		ChildChildChild = ChildChildChild->getChildren().at(0);
		if (ChildChildChild->getName().compare(ICON_TAGNAME) == 0)
			break;
	}

	ChildChildChild->addChild(m_shortcutsPoint);

}
//큐브 프로그램 시작화면
void Computer_Tool::m_createCubeWindow() {
	m_computerWindow = CubeFirstScene;
	std::vector<Game_ComputerIcon> CubeWindowIcon
		= std::vector<Game_ComputerIcon>(1);

	Size MoniterSize = m_moniterBackground->getContentSize();
	MoniterSize.height = MoniterSize.height * 0.8f;

	CubeWindowIcon.at(0).Name = u8"새로시작";
	CubeWindowIcon.at(0).Unique_Number= 1;
	CubeWindowIcon.at(0).Body = CUBE_PNG;
	CubeWindowIcon.at(0).Icon_Kind = Computer_IconKind::Icon_Cube;

	CubeWindowIcon.at(0).Name = u8"폴더";
	CubeWindowIcon.at(0).Unique_Number = 2;
	CubeWindowIcon.at(0).Body = FORDER_PNG;
	CubeWindowIcon.at(0).Icon_Kind = Computer_IconKind::Icon_Cube;
	
	std::vector<Layout*> IconLayout;
	IconLayout.push_back(Layout::create());
	IconLayout.back()->setBackGroundImageScale9Enabled(true);
	IconLayout.back()->setContentSize(Size(MoniterSize.width,
		MoniterSize.height*0.8f));
	IconLayout.back()->setPosition(MoniterSize *0.5f);
	IconLayout.back()->setAnchorPoint(Vec2::ANCHOR_MIDDLE);
	IconLayout.back()->setLayoutType(Layout::Type::HORIZONTAL);

	m_iconAndMouseSetting(CubeWindowIcon, IconLayout);
}
//받은 게임파일
void Computer_Tool::m_createGetFileWindow() {
	m_computerWindow = BusinessScene;
	auto PlayerFile = m_playerData->getHavBusiness();
	std::vector<Game_ComputerIcon> CubeFile;

	std::vector<Layout*> IconLayout;

	Size MoniterSize = m_moniterBackground->getContentSize();
	MoniterSize.height = MoniterSize.height * 0.8f;
	for (int i = 0,k = 0; i < PlayerFile.size(); i++){
		
		Game_ComputerIcon Files;
		Files.Body = PlayerFile.at(i).Body;
		Files.Icon_Kind = Computer_IconKind::Icon_Cube;
		Files.Name = PlayerFile.at(i).Name;
		Files.Unique_Number = PlayerFile.at(i).Unique_Number;

		CubeFile.push_back(Files);

		if (i % 5 != 0)continue;

		IconLayout.push_back(Layout::create());
		IconLayout.back()->setBackGroundImageScale9Enabled(true);
		IconLayout.back()->setContentSize(
			Size(MoniterSize.width, MoniterSize.height *0.25f));
		IconLayout.back()->setPosition(
			Size(0.0f, MoniterSize.height * 0.25f * (4 - k)));
		IconLayout.back()->setAnchorPoint(Vec2::ANCHOR_TOP_LEFT);
		IconLayout.back()->setLayoutType(Layout::Type::HORIZONTAL);
		k++;
	}

	m_iconAndMouseSetting(CubeFile, IconLayout);
}
//컴퓨터에 저장된 파일들 사진, 메모장 등등 
void Computer_Tool::m_createDownLoadFileWinodw() {
	m_computerWindow = ComputerWindow::FileScene;
	auto GameFileNum = m_playerData->getHavItem();

	for (int i = 0; i < GameFileNum.size(); i++) {
		Button* Files = Button::create(GameFileNum[i].Body.c_str());
		Files->ignoreContentAdaptWithSize(false);
		Files->setContentSize(m_iconSize);
		Files->setPosition(
			Vec2(Files->getContentSize().width *i
				+ Files->getContentSize().width * 0.3f *(i + 1)
				, m_moniterScreen->getContentSize().height *0.5f));
		for (int k = i; k > 4; k -= i) {
			Files->setPosition(Files->getPosition()
				- Vec2(0.0f, Files->getContentSize().height*1.3f));
		}
		Files->setAnchorPoint(Vec2::ANCHOR_TOP_LEFT);
		Files->setTag(i);
		m_moniterScreen->addChild(Files);

		Label* FileName = Label::create(GameFileNum[i].Name.c_str(), FONT_NAME
			, Files->getContentSize().width*LABEL_SIZE);
		FileName->setAnchorPoint(Vec2::ANCHOR_MIDDLE_TOP);
		FileName->setTextColor(Color4B::BLACK);
		FileName->setPosition(Vec2(Files->getContentSize().width*0.5f, 0.0f));
		Files->addChild(FileName);

	}
	m_shortXCount = 0;
	m_shortcutsPoint->removeFromParent();
		m_moniterScreen->getChildren().at(0)->
		getChildren().at(0)->addChild(m_shortcutsPoint);

}

//---------------------------------메신저---------------------------------//



//-----------------------------게임 플레이 화면------------------------------//

//큐브 프로그램 화면
void Computer_Tool::m_createCubePlay() {
	m_playerData->setSceneInterface(SceneInterface::CubeGameScene);
	m_shortXCount = 0;

	Layout* NewWindow = Layout::create();
	NewWindow->setBackGroundColorType(Layout::BackGroundColorType::SOLID);
	NewWindow->setBackGroundColor(Color3B::GREEN);
	NewWindow->setLayoutType(LayoutType::VERTICAL);
	NewWindow->setContentSize(m_moniterScreen->getContentSize());
	m_moniterScreen->addChild(NewWindow);

}
//큐브 이어받기
void Computer_Tool::m_createBisinessCubeWindow(int NodeTag) {
	m_playerData->setSceneInterface(SceneInterface::CubeGameScene);
	m_moniterBackground->addChild(m_cubeWindow.getBackGround(
		m_playerData->getHavBusiness().at(NodeTag).AddFileNum));
	m_shortXCount = 0;
}
void Computer_Tool::m_ComputerKeyEvent(EventKeyboard::KeyCode kcode) {
}