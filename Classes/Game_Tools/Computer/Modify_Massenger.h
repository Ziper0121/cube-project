#pragma once
#include <ui\CocosGUI.h>

#include "Game_Data\Player_Data.h"

using namespace cocos2d;
using namespace cocos2d::ui;

enum MassengerWinodw {FriendsWindow,TlakRoomWinodw};

class Massenger_Tool {
public:
	//메신저를 리턴한다.
	Layout* getMaseengerBackGround();

	//키보드 이벤트
	void MassengerKeyboardEvent(EventKeyboard::KeyCode KCode);
private:
	
	//메신저 뒷배경만들기
	void m_createBackground();


	//메신저의 친구목록
	void m_createFirstWinodw();

	//누군가와 대화창
	void m_createTalkRoomWinodw();


	//프로필 상세 정보
	void m_createProfileInformation(int FriendNum);
private:
	//Massenger background
	Layout* m_background;

	std::vector<Layout*> m_profileBack;

	//메신저의 씬 상황
	MassengerWinodw m_massengerWinodw;

	//플레이어 데이타
	Game_Player* m_playerData;

	//메신저의 마우스
	Sprite* m_messengerMouse;
};
/*
친구와 대화방을 따로둔다.
*/