
#include "Game_Tools\Manager\LanguageManager.h"
#include "Game_Library\Game_FileRoute.h"
#include "Game_Library\G_ConstLibrary\Cube_CCollection.h"
#include "Game_Data\Player_Data.h"
#include "Game_Data\TalkObject_Data.h" 

LanguageManager* LanguageManager::_instance = nullptr;

LanguageManager::LanguageManager()
{
	std::string FileName;
	// detect current language

	switch (Game_Player::getInstance()->getGameSetting()->m_userLanguage)
	{
	case PlayerLanguage::Korean:
		FileName = KO_JSON;
		break;
	case PlayerLanguage::English:
		FileName = EN_JSON;
		break;
	default:
		CCLOG("Unknown language. Use Korean");
		FileName = EN_JSON;
		break;
	}

	std::string FileData = CCFileUtils::sharedFileUtils()->getStringFromFile(FileName.c_str());
	document.Parse(FileData.c_str());
	if (document.HasParseError())
	{
		CCLOG("Language file parsing error! Code", document.GetParseError());
		return;
	}
}

LanguageManager* LanguageManager::getInstance()
{
	if (!_instance)
		_instance = new LanguageManager();
	return _instance;
}

std::string LanguageManager::getStringForKey(std::string key) {
	return document[key.c_str()].GetString();
}

//fileName에 해당하는 json파일을 받아서 파싱후 리턴한다.
Document GetPaserDocument(const char* fileName) {
	std::string name = FileUtils::getInstance()->fullPathForFilename(fileName);
	std::ifstream ReadItemData(name);
	
	if (ReadItemData.fail())
	{
		throw std::out_of_range("Read Item Data가 fail이다.");
	}
	IStreamWrapper ItemData(ReadItemData);

	EncodedInputStream<UTF8<>, IStreamWrapper> EnchodeData(ItemData);

	Document PasingData;
	PasingData.ParseStream<0, UTF8<>>(EnchodeData);


	if (PasingData.HasParseError()) {
		CCLOG("Error Code : %d", PasingData.GetParseError());
		assert(false);
	}
	ReadItemData.close();
	return PasingData;
}

//fileName에 해당하는 json파일에 변경된 데이터를 저장한다.
void SaveDocument(const char* fileName, Document& SaverData) {

	std::ofstream OutWrite(fileName);
	OStreamWrapper WriteData(OutWrite);
	PrettyWriter<OStreamWrapper, UTF8<>> PrettyJson(WriteData);

	SaverData.Accept(PrettyJson);

}

//플레이어가 가지고있는 아이템 정보를 리턴한다.
Game_Item GetObjectItemData(int FileNum) {

	FileNum -= 1;
	if (FileNum<0)
		FileNum = 0;

	Document PasingData = GetPaserDocument(ITEM_JSON);

	if (PasingData.HasParseError()) {
		CCLOG("Error Code : %d", PasingData.GetParseError());
		assert(false);
	}

	auto ItemArray = PasingData["GameItem"].Begin()+ FileNum;

	Game_Item ReturnItem;
	ReturnItem.Name = (*ItemArray)["Name"].GetString();
	ReturnItem.use = Object_Rating((*ItemArray)["UseRank"].GetInt());
	ReturnItem.Unique_Number = FileNum + 1;
	ReturnItem.Body = ITEM_ROUTE;
	ReturnItem.Body.append((*ItemArray)["CharBody"].GetString());
	ReturnItem.information = (*ItemArray)["Information"].GetString();
	ReturnItem.Keep = (*ItemArray)["Keep"].GetBool();
	
	return ReturnItem;
}

//번호를 받아 아이템값을 받아온다.
Game_AddFile GetAddFileData(int FileNum) {
	
	FileNum -= 1;
	if (FileNum<0)
		FileNum = 0;

	Document PasingData = GetPaserDocument(ADDFILE_JSON);

	if (PasingData.HasParseError()) {
		CCLOG("Error Code : %d", PasingData.GetParseError());
		assert(false);
	}

	auto ItemArray = PasingData["GameItem"].Begin();
	ItemArray += FileNum;

	Game_AddFile ReturnAddFile;
	ReturnAddFile.Name = (*ItemArray)["Name"].GetString();
	ReturnAddFile.use = Object_Rating((*ItemArray)["UseRank"].GetInt());
	ReturnAddFile.Unique_Number = FileNum + 1;
	ReturnAddFile.Body = ITEM_ROUTE;
	ReturnAddFile.Body.append((*ItemArray)["CharBody"].GetString());
	ReturnAddFile.information, (*ItemArray)["Information"].GetString();
	ReturnAddFile.Keep = (*ItemArray)["Keep"].GetBool();
	ReturnAddFile.AddFileNum = (*ItemArray)["AddFileNum"].GetInt();
	ReturnAddFile.AddFileKind
		= (GameObjectKind)(*ItemArray)["AddFileKind"].GetInt();

	return ReturnAddFile;

}

//computer IconFile
Game_ComputerIcon GetComputerIconData(int IconNum) {

	IconNum -= 1;
	if (IconNum < 0)
		IconNum = 0;

	Document PasingData = GetPaserDocument(ICON_JSON);

	if (PasingData.HasParseError()) {
		CCLOG("Error Code : %d", PasingData.GetParseError());
		assert(false);
	}

	auto ItemArray = PasingData["GameIcon"].Begin();
	ItemArray += IconNum;

	Game_ComputerIcon ReturnIcon;
	ReturnIcon.Name = (*ItemArray)["Name"].GetString();
	ReturnIcon.Unique_Number = ((*ItemArray)["Unique_Number"].GetInt());
	ReturnIcon.Body = ICON_ROUTE;
	ReturnIcon.Body.append((*ItemArray)["Body"].GetString());
	ReturnIcon.Icon_Kind
		= (Computer_IconKind)(*ItemArray)["Icon_Kind"].GetInt();

	return ReturnIcon;

}

//플레이어가 가지고있는 대화 내용을 받는 함수
MessengerRoom  GetMessengerData(int MessengerNum) {
	MessengerNum -= 1;
	if (MessengerNum <0)
		MessengerNum = 0;

	Document PasingData = GetPaserDocument(MESSENGER_JSON);

	if (PasingData.HasParseError()){
		CCLOG("Error Code : %d", PasingData.GetParseError());
		assert(false);
	}
	/*
	이거 꼭 봐라
	messenger파일을 맞어 만들어라
	Json파일 만들고
	파싱함수 만들고
	메세지 대화 방만들어라
	*/

	auto MessengerPasing = PasingData["Game_Messenger"].Begin();
	MessengerPasing += MessengerNum;
	MessengerPasing = (*MessengerPasing)["MessengerRoom"].Begin();

	MessengerRoom ReturnMessenger;

	Messege_Talk MessengerTalk;
	MessengerTalk.SayObjectEncho = (FaceEncho)(*MessengerPasing)["SayObjectEncho"].GetInt();
	MessengerTalk.SayObjectTexture= (*MessengerPasing)["SayObjectTexture"].GetString();
	MessengerTalk.TalkText= (*MessengerPasing)["TalkText"].GetString();

	ReturnMessenger.RoomTalk.push_back(MessengerTalk);

	return ReturnMessenger;
}

//플레이어의 음악파일의 정보를 받아온다.
Game_Music GetMusicData(int MusicNum) {
	MusicNum -= 1;
	if (MusicNum<0)
		MusicNum= 0;

	Document PasingData = GetPaserDocument(MUSIC_JSON);

	if (PasingData.HasParseError()) {
		CCLOG("Error Code : %d", PasingData.GetParseError());
		assert(false);
	}
	/*
	이거 꼭 봐라
	messenger파일을 맞어 만들어라
	Json파일 만들고
	파싱함수 만들고
	메세지 대화 방만들어라
	*/

	auto MusicPasing = PasingData["Game_Music"].Begin();
	MusicPasing += MusicNum;
	
	Game_Music MusicData;
	MusicData.Name = (*MusicPasing)["Name"].GetString();
	MusicData.Unique_Number = (*MusicPasing)["Unique_Number"].GetInt();
	MusicData.MusicFile = (*MusicPasing)["MusicFile"].GetString();
	MusicData.Body = (*MusicPasing)["Body"].GetString();
	MusicData.Kind = (MusicKind)(*MusicPasing)["Kind"].GetInt();

	return MusicData;
}

//플레이어가 가지고있는 이메일 정보를 리턴한다.
MessengerFrien  GetMessengerFrienData(int FileNum, MessengerFrienKind FriendFile) {

	FileNum -= 1;
	if (FileNum<0)
		FileNum = 0;

	std::string FriendFileName = MESSENGER_FRIEN_ROUTE;
	switch (FriendFile)
	{
	case Cube_Intern:
		FriendFileName.append("Cube_Intern.json");
		break;
	case Computer_Manager:
		FriendFileName.append("Computer_Manager.json");
		break;
	default:
		break;
	}

	Document PasingData = GetPaserDocument(FriendFileName.c_str());

	if (PasingData.HasParseError()) {
		CCLOG("Error Code : %d", PasingData.GetParseError());
		assert(false);
	}

	auto ItemArray = PasingData["Friend"].Begin();
	ItemArray += FileNum;

	MessengerFrien ReturnItem;
	ReturnItem.Name = (*ItemArray)["Name"].GetString();
	ReturnItem.use = Object_Rating((*ItemArray)["UseRank"].GetInt());
	ReturnItem.Unique_Number = (*ItemArray)["Unique_Number"].GetInt();
	ReturnItem.Body = (*ItemArray)["CharBody"].GetString();
	ReturnItem.FriendKind 
		= (MessengerFrienKind)(*ItemArray)["FriendKind"].GetInt();
	return ReturnItem;
}

Cube_Misstion GetMissionData(int businessNum) {

	businessNum -= 1;

	if (businessNum < 0)
		businessNum = 0;

	Document PasingData = GetPaserDocument(MISSION_JSON);

	auto ItemArray = PasingData["GameBusiness"].Begin();
	ItemArray += businessNum;

	Cube_Misstion ReturnBusiness;
	ReturnBusiness.BusinessClear = (*ItemArray)["Clear"].GetBool();
	
	for (auto i = (*ItemArray)["RewardItem"].Begin();
		i < (*ItemArray)["RewardItem"].End(); i++) {
		RewardItem Push_Reward;
		Push_Reward.ItemKind = (GameObjectKind)(*i)["ItemKind"].GetInt();
		Push_Reward.ItemNum = (*i)["ItemNum"].GetInt();
		ReturnBusiness.Cube_Reward.push_back(Push_Reward);
	}

	for (auto i = (*ItemArray)["OriginalCube"].Begin();
		i < (*ItemArray)["OriginalCube"].End(); i++) {
		Mix_CubeMap CubeMap;
		CubeMap.Encho = (Cube_EnchoMove)(*i)["Encho"].GetInt();
		CubeMap.Grab = (Grab_Key)(*i)["Grab"].GetInt();
		CubeMap.NumTime = (*i)["NumTime"].GetInt();
		ReturnBusiness.OriginalCube.push_back(CubeMap);
	}

	for (auto i = (*ItemArray)["VecBusiness"].Begin();
		i < (*ItemArray)["VecBusiness"].End(); i++) {
		std::vector<Mix_CubeMap> VecCubeMap;
		for (auto k = (*i)["Business"].Begin(); k < (*i)["Business"].End(); k++) {
			Mix_CubeMap CubeMap;
			CubeMap.Encho = (Cube_EnchoMove)(*k)["Encho"].GetInt();
			CubeMap.Grab = (Grab_Key)(*k)["Grab"].GetInt();
			CubeMap.NumTime = (*k)["NumTime"].GetInt();
			VecCubeMap.push_back(CubeMap);
		}
		ReturnBusiness.Business.push_back(VecCubeMap);
	}

	return ReturnBusiness;

}

//npc번호를 순서에 맞는곳에서 데이터를 가지고온다. 
Game_NPC GetNPCData(int NPC_Num, std::string NPCFile) {
	
	NPC_Num -= 1;
	if (NPC_Num< 0)
		assert(false);

	std::string FileName = GetMapFileName();
	if (NPCFile.find(MAPNPCDATA_ROUTE) == std::string::npos)
		if (NPCFile.size() > 0)
			FileName = MAPNPCDATA_ROUTE + NPCFile;
	
	//맵json리스트에서 값을 받는것인지 자체적인 json에서 받는것인지 확인한다.
	Document NpcPasing; 
	if (FileName.find("_List.json") == std::string::npos){
		NpcPasing = GetPaserDocument(FileName.c_str());
	}
	else
	NpcPasing = GetPaserDocument(
		GetTileMapList(false,GameMapPlace(0),FileName.c_str()).back().c_str());

	//파싱된 데이터를 사용한다.
	auto NpcData = NpcPasing["MapNpc"].Begin();

	NpcData += NPC_Num;


	Game_NPC Return_Npc;
	Return_Npc.Name = (*NpcData)["Name"].GetString();
	Return_Npc.Unique_Number = (*NpcData)["Unique_Number"].GetInt();
	Return_Npc.Body = IMAGE_ROUTE;
	Return_Npc.Body.append((*NpcData)["Body"].GetString());
	Return_Npc.walk_Speed = (*NpcData)["Walk_Speed"].GetFloat();

	for (auto i = (*NpcData)["Text"].Begin(); i < (*NpcData)["Text"].End(); i++) {
		Game_Text NPC_Text;

		NPC_Text.name = (*i)["Name"].GetString();
		NPC_Text.unique_Number = (*i)["Unique_Number"].GetInt();
		CCLOG("TextName%s", NPC_Text.name.c_str());
		NPC_Text.is_Talk = (*i)["Is_Talk"].GetBool();

		for (auto k = (*i)["Limited"].Begin(); k < (*i)["Limited"].End(); k++)
		{
			for (auto m = (*k)["Ago_Quest"].Begin(); m < (*k)["Ago_Quest"].End(); m++) {
				NpcQuest_Num AgeQuest;
				AgeQuest.QuestFile = (*m)["QuestFile"].GetString();
				AgeQuest.Npc = (*m)["Npc"].GetInt();
				AgeQuest.Quest = (*m)["Quest"].GetInt();
				NPC_Text.limited.Ago_Quest.push_back(AgeQuest);
			}
			for (auto m = (*k)["Item"].Begin(); m < (*k)["Item"].End(); m++) {
				NPC_Text.limited.Item.push_back((*m).GetInt());
			}

			NPC_Text.limited.Rank = (Rank_List)(*k)["Rank"].GetInt();
		}
		
		//--------------------------------보상-------------------------------
		for (auto k = (*i)["Reward_Item"].Begin();
			k < (*i)["Reward_Item"].End(); k++) {
			ObjectKind PasingItem = { };
			PasingItem.Kind 
				= (GameObjectKind)(*k)["Reward_ItemKind"].GetInt();
			PasingItem.ItemNumber
				= (GameObjectKind)(*k)["Reward_ItemNum"].GetInt();
			NPC_Text.reward_Item.push_back(PasingItem);
		}

		for (auto k = (*i)["TalkInteface"].Begin(); k < (*i)["TalkInteface"].End(); k++)
		{
			TalkWindow_Kind PushTalk;
			PushTalk.OnButton = (*k)["OnButton"].GetBool();
			PushTalk.TalkText = (*k)["TalkText"].GetString();
			int TextureNum 	= (*k)["SayObjectTexutre"].GetInt();
			if (TextureNum > OBJECT_FACE_TEXTURE.size() || TextureNum <= 0) {
				NPC_Text.talkInteface.push_back(PushTalk);
				break;
			}
			PushTalk.SayObjectTexture = OBJECT_FACE_TEXTURE.at(TextureNum-1);
			PushTalk.SayObjectEmotion
				= (EmotionKind)(*k)["SayObjectEmotion"].GetInt();
			PushTalk.SayObjectEncho
				= (FaceEncho)(*k)["SayObjectEncho"].GetInt();

			NPC_Text.talkInteface.push_back(PushTalk);
		}
		Return_Npc.Text.push_back(NPC_Text);
	}

	return Return_Npc;
}
