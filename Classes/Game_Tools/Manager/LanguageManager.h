#pragma once
#include <string>
#include <json\rapidjson.h>
#include <json\encodedstream.h>
#include <json\istreamwrapper.h>
#include <json\ostreamwrapper.h>
#include <json\prettywriter.h>
#include <json\document.h>

#include "Game_Data\Game_TextSpecimen_List.h"
#include "Game_Library\Game_FileRoute.h"

using namespace rapidjson;

struct Game_ComputerIcon;
struct Cube_Misstion;

class LanguageManager
{
    Document document; // current document with language data
    LanguageManager(); // constructor is private
    static LanguageManager* _instance;
public:
    static LanguageManager* getInstance();
	std::string getStringForKey(std::string key);
};

//fileName에 해당하는 json파일을 받아서 파싱후 리턴한다.
Document GetPaserDocument(const char* fileName);

//fileName에 해당하는 json파일에 변경된 데이터를 저장한다.
void SaveDocument(const char* fileName,Document& SaverData);

//번호를 받아 아이템값을 받아온다.
Game_Item GetObjectItemData(int ItemNum);

//번호를 받아 아이템값을 받아온다.
Game_AddFile GetAddFileData(int ItemNum);

//computer IconFile
Game_ComputerIcon GetComputerIconData(int IconNum);

//플레이어가 가지고있는 이메일 정보를 리턴한다.
MessengerRoom  GetMessengerData(int MessengerNum);

//플레이어의 음악파일의 정보를 받아온다.
Game_Music GetMusicData(int MusicNum);

//플레이어가 가지고있는 이메일 정보를 리턴한다.
MessengerFrien  GetMessengerFrienData(int FrienNum, MessengerFrienKind FileKind);

//json으로 저장된 Cube미션을 받는다.
Cube_Misstion GetMissionData(int);

//NPC의 json파일을 파싱을 하여 return 을 하는 함수
Game_NPC GetNPCData(int NPC_Num,std::string NPCFile = "\0");


