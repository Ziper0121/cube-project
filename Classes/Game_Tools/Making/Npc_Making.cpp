#include "Game_Tools\Making\Npc_Making.h"
NpcController* NpcController::npcControl = nullptr;

NpcController::NpcController() {
	m_allNpc.push_back(Npc_1());
	m_allNpc.push_back(Npc_2());
	m_allNpc.push_back(Npc_3());
}

Sprite* NpcController::getNpcBody(int NpcNumber) {
	return Sprite::create();
}
std::vector<Sprite*> NpcController::getNpcBody(GameMapPlace NpcMapPlace) {
	std::vector<Sprite*> MapNpc = std::vector<Sprite*>(0);
	for (auto i = m_allNpc.begin(); i < m_allNpc.end(); i++)
		if ((*i).MapPlace == NpcMapPlace)
			MapNpc.push_back((*i).Char_Body);

	return MapNpc;
}

//Npc의 위치를 찾아주는 함수
Npc_Char NpcController::getNpcBlock(Point PlayerPoint, GameMapPlace PlayerPlace) {
	
	//반환 스케줄
	Npc_Char Retrun(0,"","",0,Point(0,0));

	//위칙 검사
	for (auto i = m_allNpc.begin(); i < m_allNpc.end(); i++) {
		if ((*i).MapPlace != PlayerPlace)continue;
		if ((*i).MapPoint != PlayerPoint)continue;
		(*i).MovingPoint.clear();
		Retrun = (*i);
	}

	return Retrun;
}


//도착장소를 말하면 그곳으로 이동하게 만든다.
// 1. 길정보를 받아 이동하는 것
// 2. 길 이동중 이벤트가 생길때의 처리

//출발하는 위치들이 다 다르다.
//포탈위치를 기점으로 만들어야할지 고민이다.

//도착장소를 찾고 이동시키는 함수
void NpcController::m_navigation(
	int NpcNum, GameMapPlace NpcMapPlace, std::vector<Vec2> ArrivalPoint) {
	m_allNpc.at(NpcNum).MovingPoint = ArrivalPoint;
	m_checkMoveEncho(&m_allNpc.at(NpcNum));
}

//vector을 사용할경우 이동해야하는 vector값을 가진 맵버변수가 필요하다

//이동을 시킬 위치를 알려주는 함수
void NpcController::m_checkMoveEncho(Npc_Char* MoveNpc) {
	
	if (MoveNpc->MovingPoint.size() == 0)
		return;

	int Moves[2] = { MoveNpc->MovingPoint.at(0).x,MoveNpc->MovingPoint.at(0).y };

	//이동할방향을 찾는다. 0은 X, 1은 Y
	for (int i = 0; i < 2; i++) {

		int MoveEncho[2] = { 0,0 };

		//이동하는 값이 마이너스인지 플러스인지 확인한다.
		if (Moves[i] < 0)
			MoveEncho[i] = -1;
		else if (Moves[i] > 0)
			MoveEncho[i] = 1;
		else continue;

		//충돌처리(충돌했을경우 true를 리턴한다.)
		if (!m_checkBlock(MoveNpc, Vec2(MoveEncho[0], MoveEncho[1])))
			m_moveChar(MoveNpc, Vec2(MoveEncho[0], MoveEncho[1]));
		return;
	}
}

/*
작동 방식
NPC가 부딪힐경우 그사람에게 용무가 있는 사람이 말을한다.
용무가 있는 npc의 가장 첫번째 용무를 전달한다.at(0)
npc는 용무를 보고 자신의 머리속에서 받아들일지를 검사하고 답변한다.
*/

//충돌처리
bool NpcController::m_checkBlock(Npc_Char* MoveNpc, Vec2 MoveEncho) {
	//이동할 위치
	Vec2 MovePoint = MoveNpc->MapPoint + MoveEncho;
	
	auto TileMap =
		MapController::createMapController()->getTileMap(MoveNpc->MapPlace);
	auto MetaInfo = 
		MapController::createMapController()->getTileLayer(MoveNpc->MapPlace);
	auto Objets =
		MapController::createMapController()->getTileObject(MoveNpc->MapPlace);
	//tilemap의 y값이 반대이기때문에 반전시키는것이다. 
	Vec2 TileGidat = MovePoint;
	TileGidat.y = TileMap->getMapSize().height - TileGidat.y - 1;

	//플래이어가 움직일 위치에 블럭이 있는지 확인한다
	int TileGid = MetaInfo->getTileGIDAt(TileGidat);
	//if (m_checkNpcBlock(MoveNpc, MovePoint))
	//	return true;
	if (TileGid == 0)
		return false;
	

	//부딪혔을때 그곳에 Object가 있는지 확인한다. 
	Vec2 ObejctPoint;
	for (auto k = Objets.begin(); k < Objets.end(); k++) {
		ObejctPoint.x = (*k).asValueMap()["x"].asFloat() / 16;
		ObejctPoint.y = (*k).asValueMap()["y"].asFloat() / 16;

		if (MovePoint == ObejctPoint)
			m_checkEventBlock(MoveNpc,(*k).asValueMap());
	}

	return false;
}
//Map의 npc를 체크한다.
bool NpcController::m_checkNpcBlock(Npc_Char* MoveNpc, Vec2 MoveEncho) {
	bool CheckBlock = true;
	//다른 NPC와 부딪혔을때
	Npc_Char NpcSchedule = getNpcBlock(MoveEncho, MoveNpc->MapPlace);
	NpcSchedule.checkNpcThink();
	return CheckBlock;
}


//애니메이션도 추가할것이다.
void NpcController::m_moveChar(Npc_Char* MoveNpc, Vec2 MoveEncho) {

	//NPC가 목적지에 도착했으면 다음 목적지를 정해준다.
	if (MoveNpc->MovingPoint.at(0).x == 0 && MoveNpc->MovingPoint.at(0).y == 0) {
		//이번이 마지막 목적지일경우 움직임을 멈춘다.
		MoveNpc->MovingPoint.erase(MoveNpc->MovingPoint.begin());
		if (MoveNpc->MovingPoint.empty())
			return;
	}

	CCLOG("MapPoint(%f,%f)", MoveNpc->MapPoint.x, MoveNpc->MapPoint.y);
	//MapPoint도 수정한다.
	MoveNpc->MapPoint += MoveEncho;
	MoveNpc->MovingPoint.at(0) -= MoveEncho;

	CCLOG("+MoveEncho(%f,%f)", MoveEncho.x, MoveEncho.y);
	CCLOG("=MapPoint(%f,%f)", MoveNpc->MapPoint.x, MoveNpc->MapPoint.y);

	//MoveBy로 이동시킨다.
	auto Char_MoveBy = MoveBy::create(MoveNpc->walk_Speed, MoveEncho* 16.0f);
	
	auto ActionCallback = CallFunc::create(
		std::bind(&NpcController::m_checkMoveEncho, this, MoveNpc));
	
	//자연스러운 움직임을 만든다.
	auto Repeat_WalkAction = Sequence::create(Char_MoveBy, ActionCallback, NULL);
	Repeat_WalkAction->setTag(MOVE_ACT);
	MoveNpc->Char_Body->runAction(Repeat_WalkAction);
}

//충돌 이벤트 확인
void NpcController::m_checkEventBlock(Npc_Char* EventNpc, ValueMap ValueBlock) {

	TileSetKind TileKind =	(TileSetKind)ValueBlock["TileCode"].asInt();
	switch (TileKind)
	{
	case Portal_Tile:
		m_eventPoartalBlock(EventNpc,ValueBlock);
		break;
	default:
		break;
	}
}

//충돌 워프 이벤트
void NpcController::m_eventPoartalBlock(Npc_Char* EventNpc, ValueMap ValueBlock) {
	
	//포탈의 종류
	const int PortalKind =	ValueBlock["PortallKind"].asInt();
	
	//이동한 맵의 Objects값
	cocos2d::ValueVector Objects;

	//map kind가 0인 값들은 맵내에서 이동하는 포탈이다.
	switch (PortalKind) {
	case 0:
		break;
	default:
		//이동할 위치로 맵의 정보를 변경한다. 맵의 정보는 편의상 1을 더해놓았다.
		EventNpc->MapPlace = (GameMapPlace)ValueBlock[PTLLMAP_TEXT].asInt();
		EventNpc->Char_Body->removeFromParent();
		MapController::createMapController()->
			getTileMap(EventNpc->MapPlace)->addChild(EventNpc->Char_Body);

		break;

	}

	//Npc가 이동할 맵의 Object값을 받는다.
	Objects = MapController::createMapController()
		->getTileObject(EventNpc->MapPlace);


	//위프가 된 곳의 포인터
	cocos2d::ValueMap WarpPoint;

	for (auto i = Objects.begin(); i < Objects.end(); i++) {
		//타일코드가 포탈코드인지 확인한다.
		if ((*i).asValueMap()[TILECODE_TEXT].asInt()
			!= TileSetKind::Portal_Tile)
			continue;
		CCLOG(" potral : %s", (*i).asValueMap()["name"].asString().c_str());
		// i = begin을 만들고싶다. 그런데 i!= valueNum이 걸린다.
		if ((*i).asValueMap()[PTLLCODE_TEXT].asInt()
			== ValueBlock[PTLLCODE_TEXT].asInt()
			&& (*i).asValueMap()[PTLLEXIT_TEXT].asBool()) {
			WarpPoint = (*i).asValueMap();
			break;
		}
	}

	int x = WarpPoint[X_TEXT].asInt();
	int y = WarpPoint[Y_TEXT].asInt();
	
	
	//이동할 장소의 포인트 값
	EventNpc->MapPoint = Vec2(x,y)/16.0f;
	EventNpc->Char_Body->setPosition(EventNpc->MapPoint * 16.0f);

	
}
