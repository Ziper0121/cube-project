#pragma once

#include "Game_Data\Npc_Data\NPC_Data.h"
#include "Game_Data\Npc_Data\Npc_1Data.h"
#include "Game_Data\Player_Data.h"
#include "Game_Data\Game_MapData.h"
#include "Game_Library\G_ConstLibrary\TileMap_CCollection.h"
using namespace cocos2d;

class NpcController {
public:
	static NpcController* createNpcControl() {
		if (npcControl == nullptr){
			npcControl = new NpcController();
		}
		return npcControl;
	}

	//NpcNumber번호에 해당하는 Npc의 Sprite값을 전달한다.
	Sprite* getNpcBody(int NpcNumber);

	//NpcMapFrom해당 위치의 Npc들의 Sprite값을 전달한다.
	std::vector<Sprite*> getNpcBody(GameMapPlace NpcMapPlace);

	//Npc의 위치를 찾아주는 함수
	Npc_Char getNpcBlock(Point PlayerPoint,GameMapPlace PlayerPlace);

	//도착장소로 이동시키는 함수
	void m_navigation(
		int Npc_Num,GameMapPlace NpcMapPlace,std::vector<Vec2> ArrivalPoint);

private :

	NpcController();

	//임시 움직일 방향을 정한다,
	void m_checkMoveEncho(Npc_Char* MoveNpc);
	
	//*********************충돌 체크*********************//

	//Map에 모든 정보들과의 충돌 체크를한다.
	bool m_checkBlock(Npc_Char* MoveNpc, Vec2 MoveEncho);
	//Map의 npc를 체크한다.
	bool m_checkNpcBlock(Npc_Char* MoveNpc, Vec2 MoveEncho);

	//NPC를 이동시키는 함수
	void m_moveChar(Npc_Char* MoveNpc, Vec2 MoveEncho);

	//충돌 이벤트 확인
	void m_checkEventBlock(Npc_Char* EventNpc,cocos2d::ValueMap ValueBlock);

	//충돌 이벤트
	void m_eventPoartalBlock(Npc_Char* EventNpc,cocos2d::ValueMap ValueBlock);

private:

	//Npc의 콘솔값
	static NpcController* npcControl;

	//모든 NPC데이터
	std::vector<Npc_Char> m_allNpc;
};

