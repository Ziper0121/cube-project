#pragma once

#include <cocos2d.h>
#include <ui/CocosGUI.h>

#include "Game_Data\Game_Specimen_List.h"
#include "Game_Library\Game_FileRoute.h"
#include "Game_Data\Npc_Data\NPC_Data.h"
#include "Game_Data\Npc_Data\Npc_1Data.h"

using namespace cocos2d;
using namespace cocos2d::ui;

enum TestInformationKind{NOInformation, Infor1, Infor2};

struct TestTellTextrue {
	int CharUniNum;

	std::string CharTell;
};


class TestTextureWindow {

private:

	TestTextureWindow() {};
	~TestTextureWindow() {};

	static TestTextureWindow* TextureWindow;
public:
	static TestTextureWindow* createTextureWindow() {
		if (TextureWindow == nullptr){
			TextureWindow = new TestTextureWindow();
		}
		return TextureWindow;
	};

	//대화 내용
	void getTextureWindow(std::vector<Game_Char> TalkChars );
	
	//대화 내용
	void getTextureNpcWindow(std::vector<Npc_Char> TalkChars);

private:
	//TextureWidnow를 만든다.
	void m_createWindow();

	//말풍선을 만든다.
	Sprite* m_createBubble(Size);

	//말풍선안에 말을 만들기
	Label* m_createTexture(int,Size);
	
private:
	//말풍선
	std::vector<Sprite*> TextureBubble;
	
	//대화 내용
	std::vector<TestTellTextrue> m_talk;
	//대화중인 Char들
	std::vector<Game_Char> m_talkChar;
};
