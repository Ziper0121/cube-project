
#include  "Game_Tools\Making\Interface_Making.h" 

//캐릭터 움직임 제어
//캐릭터 이벤트 설정
//타일 배치
Layout* Interface_Making::CreateInterface_Making(std::vector<std::vector<std::function<void(int)>>> uiFunction) {

	//가장 기준이 되는 layout
	Layout* BackLayout = Layout::create();
	BackLayout->setLayoutType(Layout::Type::RELATIVE);
	BackLayout->setBackGroundColorType(Layout::BackGroundColorType::SOLID);
	BackLayout->setBackGroundImageColor(Color3B::GRAY);
	BackLayout->setZOrder(BACK_LAYOUT_NUM);

	//Menu layout들의 위치를 설정하는 layout;
	Layout* LayoutLayout = Layout::create();
	LayoutLayout->setLayoutType(Layout::Type::VERTICAL);
	BackLayout->addChild(LayoutLayout);

	/*
	ui만드는 알고리즘에
	Eventlister을 어떡해 설정할지
	*/

	Layout* MenuLayout = Layout::create();
	MenuLayout->setLayoutType(Layout::Type::HORIZONTAL);
	BackLayout->addChild(MenuLayout, MENU_LAYOUT_NUM);

	RelativeLayoutParameter* MenuLayout_Parameter = RelativeLayoutParameter::create();
	MenuLayout_Parameter->setAlign(RelativeAlign::CENTER_IN_PARENT);
	MenuLayout->setLayoutParameter(MenuLayout_Parameter);

	Button* MenuICon = Button::create();
	MenuLayout->addChild(MenuICon, MENU_LAYOUT_NUM);

	LinearLayoutParameter* MenuLayoutParameter = LinearLayoutParameter::create();
	MenuLayoutParameter->setGravity(LinearGravity::RIGHT);
	MenuLayoutParameter->setMargin(Margin(10.0f, 0.0f, 10.0f, 0.0f));
	return BackLayout;
}

Layout* Interface_Making::getMakingInterface(char*) {
	return Layout::create();
}

bool CreateIconLayout(Node* IconParent
	, std::vector<Game_ComputerIcon> ComputerIcon
	,std::function<void(Ref*)> IconClickEvent) {

	return true;

}