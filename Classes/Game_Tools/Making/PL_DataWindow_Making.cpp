﻿
#include "Game_Tools\Making\PL_DataWindow_Making.h" 
#include "Game_Project\MainMenu_Screen.h" 

Layout* InformationWindow::getInformationWindow() {
	m_createBackGround();

	return m_backGroundLayer;
}

//----------------------------제작 함수----------------------------

//바탕화면 아이콘과 마우스 생성
void InformationWindow::m_createIconAndMouse(const char* TagName) {

	m_iconXCount  = 0;
	m_iconYCount = 0;
	m_pointIcon->removeFromParent();

	Node* ChildChildChild = m_backGroundLayer->getChildren().back();
	if(m_informationScene == InformationScene::OperationButton){
		ChildChildChild = *(m_backGroundLayer->getChildren().begin());
	}

	while (true) {
		if (ChildChildChild->getChildrenCount() == 0)
			return;
		ChildChildChild = ChildChildChild->getChildren().at(0);
		if (ChildChildChild->getName().compare(TagName) == 0)
			break;
	}

	ChildChildChild->addChild(m_pointIcon);
}

//기본 배이스 만들기
void InformationWindow::m_createBackGround() {

	m_informationScene = InformationScene::OperationButton;
	m_playerData = Game_Player::getInstance();
	Size WindowSize = m_playerData->getGameSetting()->m_window_Size;

	m_backGroundLayer = Layout::create();

	m_backGroundLayer->setPosition(WindowSize*0.5f);
	m_backGroundLayer->setAnchorPoint(Vec2::ANCHOR_MIDDLE);
	m_backGroundLayer->setTag(INFORWINDOW_TAG);
	m_backGroundLayer->setBackGroundColorType(LayoutBackGroundColorType::SOLID);
	m_backGroundLayer->setBackGroundColor(Color3B::GRAY);
	m_backGroundLayer->setContentSize(WindowSize * 0.7f);

	Size BackGroundSize = m_backGroundLayer->getContentSize();

	Layout* DataWinLayout = Layout::create();
	m_backGroundLayer->addChild(DataWinLayout);
	DataWinLayout->setContentSize(
		Size(BackGroundSize.width * 0.5f, BackGroundSize.height * 0.1f));
	DataWinLayout->setPosition(Vec2(0.0f, BackGroundSize.height));
	DataWinLayout->setAnchorPoint(Vec2::ANCHOR_BOTTOM_LEFT);
	DataWinLayout->setName(ICON_LAYOUT);

	Layout* DataWinButtonLayout = Layout::create();
	DataWinLayout->addChild(DataWinButtonLayout);
	DataWinButtonLayout->setContentSize(DataWinLayout->getContentSize());
	DataWinButtonLayout->setAnchorPoint(Vec2::ANCHOR_BOTTOM_LEFT);
	DataWinButtonLayout->setLayoutType(Layout::Type::HORIZONTAL);
	auto VecItem = m_playerData->getHavItem();
	std::vector<char*> ButtonText = { u8"소지품",u8"설정",u8"MP3" };
	for (auto i = VecItem.begin(); i < VecItem.end(); i++){
		if ((*i).Unique_Number != 1)
			continue;
		//ButtonText.push_back(u8"MP3");
	}
	
	Size DataLayoutSize = DataWinButtonLayout->getContentSize();

	std::vector<Button*> DataWinButton;

	for (int i = 0; i < ButtonText.size(); i++){
		DataWinButton.push_back(Button::create(WHITEBLOCK_PNG));
		DataWinButtonLayout->addChild(DataWinButton.back());
		DataWinButton.back()->ignoreContentAdaptWithSize(false);
		DataWinButton.back()->setContentSize(
			Size(DataLayoutSize.width *0.5f
				, DataLayoutSize.height));
		DataWinButton.back()->setColor(Color3B::GRAY);
		DataWinButton.back()->setTitleFontName(FONT_NAME);
		DataWinButton.back()->setTitleText(ButtonText[i]);
		DataWinButton.back()->setTitleColor(Color3B::BLACK);
		DataWinButton.back()->addClickEventListener(
			std::bind(&InformationWindow::m_changeWindow, this, std::placeholders::_1));
		DataWinButton.back()->setTag(i);
		DataWinButton.back()->setName(DATA_BUTTON);
		if (i == 0) {
			continue;
		}
		LinearLayoutParameter* ButtonParameter = LinearLayoutParameter::create();
		DataWinButton.back()->setLayoutParameter(ButtonParameter);
		ButtonParameter->setMargin(
			Margin(-DataLayoutSize.width * 0.1f, 0.0f, 0.0f, 0.0f));

	}

	m_iconSize = Size(m_backGroundLayer->getContentSize().width*0.2f
		, m_backGroundLayer->getContentSize().width*0.2f);
	m_pointIcon = Sprite::create(MOUSE_PNG);
	m_pointIcon->setAnchorPoint(Vec2::ANCHOR_BOTTOM_LEFT);
	m_pointIcon->retain();

	m_createIconAndMouse(DATA_BUTTON);

	//음악의 바탕화면 layout을 메모리에 할당한다.
	//m_musicBackLayout = Layout::create();
	//m_musicBackLayout->retain();
	//m_musicBackLayout->setLayoutType(Layout::Type::VERTICAL);
	//m_musicBackLayout->setBackGroundColorType(Layout::BackGroundColorType::SOLID);
	//m_musicBackLayout->setBackGroundColor(Color3B::GRAY);
	//m_musicBackLayout->setLayoutType(Layout::Type::VERTICAL);
	//m_musicBackLayout->setAnchorPoint(Vec2::ANCHOR_BOTTOM_LEFT);


	m_createItemWindow();
}
//플레이어 아이템 창 만들기( 기본베이스에서 불러와야하기 때문에 따로만들었다)
void InformationWindow::m_createItemWindow(){
	std::vector<Game_Item> PlayerItemList = m_playerData->getHavItem();


	Size MoniterScreenSize = m_backGroundLayer->getContentSize();
	Size IconSize = MoniterScreenSize * 0.2f;

	Layout* NewWindow = Layout::create();
	m_backGroundLayer->addChild(NewWindow);
	NewWindow->setBackGroundColorType(Layout::BackGroundColorType::SOLID);
	NewWindow->setBackGroundColor(Color3B::GRAY);
	NewWindow->setLayoutType(LayoutType::VERTICAL);
	NewWindow->setContentSize(MoniterScreenSize);
	NewWindow->setName(ICON_LAYOUT);

	std::vector<Layout*> HorizonIcon;
	auto PlayerIcon = PlayerItemList.begin();
	std::string BodyFile;

	if (PlayerItemList.size() == 0)
		return;

	for (int i = 0; i < 4; i++) {
		HorizonIcon.push_back(Layout::create());
		NewWindow->addChild(HorizonIcon.back());
		HorizonIcon.back()->setLayoutType(LayoutType::HORIZONTAL);
		HorizonIcon.back()->setContentSize(Size(MoniterScreenSize.width
			, IconSize.height *1.2f));
		for (int k = 0; k < 5 && PlayerIcon != PlayerItemList.end();
			k++, PlayerIcon++) {
			BodyFile = (*PlayerIcon).Body;
			if (BodyFile.find(IMAGE_ROUTE) == std::string::npos)
				BodyFile = ITEM_ROUTE + (*PlayerIcon).Body;

			Button* shortcuts = Button::create(BodyFile);
			HorizonIcon.back()->addChild(shortcuts);
			shortcuts->setContentSize(IconSize);
			shortcuts->ignoreContentAdaptWithSize(false);
			shortcuts->setAnchorPoint(Vec2::ANCHOR_TOP_LEFT);
			shortcuts->addClickEventListener(std::bind(
				&InformationWindow::m_changeWindow,this,std::placeholders::_1));
			shortcuts->setTag(i * 5 + k);
			shortcuts->setName(ICON_TAGNAME);

			Label* ShortLabel = Label::create((*PlayerIcon).Name, FONT_NAME
				, shortcuts->getContentSize().width*LABEL_SIZE);
			shortcuts->addChild(ShortLabel);
			ShortLabel->setAnchorPoint(Vec2::ANCHOR_MIDDLE_TOP);
			ShortLabel->setTextColor(Color4B::BLACK);
			ShortLabel->setPosition(Size(IconSize.width*0.5f, 0.0f));
		}
		if (PlayerIcon == PlayerItemList.end())
			break;
	}
}


//----------------------------화면열기----------------------------

//플레이어 아이템창
void InformationWindow::m_openItemWindow(){
	m_informationScene = InformationScene::ItemWindow;
	m_createItemWindow();
	m_createIconAndMouse(ICON_TAGNAME);
}
//플레이어 MP3창
void InformationWindow::m_openMusicWindow() {

	m_informationScene = InformationScene::MusicWinodw;

	Size BackSize = m_backGroundLayer->getContentSize();

	// ui정리 용도 Layout 생성.
	m_musicBackLayout = Layout::create();
	m_musicBackLayout->setLayoutType(Layout::Type::VERTICAL);
	m_musicBackLayout->setBackGroundColorType(Layout::BackGroundColorType::SOLID);
	m_musicBackLayout->setBackGroundColor(Color3B::GRAY);
	m_musicBackLayout->setLayoutType(Layout::Type::VERTICAL);
	m_musicBackLayout->setAnchorPoint(Vec2::ANCHOR_BOTTOM_LEFT);
	m_musicBackLayout->setContentSize(BackSize);
	m_backGroundLayer->addChild(m_musicBackLayout);

	Layout* MusicPlayerLayout = Layout::create();
	m_musicBackLayout->addChild(MusicPlayerLayout);
	MusicPlayerLayout->setLayoutType(Layout::Type::HORIZONTAL);
	MusicPlayerLayout->setContentSize(
		Size(BackSize.width, BackSize.height * 0.2f));

	Size PlayerUISize = Size(BackSize.width * 0.2f, BackSize.width * 0.2f);

	ImageView* MusicProfile = ImageView::create(CUBEPROFILE_PNG);
	MusicPlayerLayout->addChild(MusicProfile);
	MusicProfile->ignoreContentAdaptWithSize(false);
	MusicProfile->setContentSize(PlayerUISize);

	LinearLayoutParameter* MusicLayoutParamter = LinearLayoutParameter::create();
	MusicProfile->setLayoutParameter(MusicLayoutParamter);
	MusicLayoutParamter->setGravity(LinearGravity::CENTER_VERTICAL);

	char* MusicPlayerUIImage[4] =
	{ MOVEMUSIC_PNG ,PLAYBUTTON_PNG ,MOVEMUSIC_PNG ,REPEATEBUTTON_PNG };

	/*
	mp3버그들
	노래 하나가 끝이나면 다음걸로 안넘어간다.
	무한재생을 하고 다음곳으로 넘어가면 기능하지 않는다.

	*/
	std::vector<Button*>MusicPlayerUIs;
	for (int i = 0; i < 4; i++) {
		MusicPlayerUIs.push_back(Button::create(MusicPlayerUIImage[i]));
		MusicPlayerLayout->addChild(MusicPlayerUIs.back());
		MusicPlayerUIs.back()->ignoreContentAdaptWithSize(false);
		MusicPlayerUIs.back()->setContentSize(PlayerUISize);
		MusicPlayerUIs.back()->setLayoutParameter(MusicLayoutParamter);
		if (i == 2) {
			MusicPlayerUIs.back()->setRotation(180.0f);
		}
	}
	MusicPlayerUIs.at(0)->addClickEventListener([=](Ref* ref) {
		int num = Game_Player::getInstance()->getPlayMusicPoint();
		m_moveMusicPointButtonEvent(ref, num - 1);
	});
	MusicPlayerUIs.at(1)->addClickEventListener(std::bind(
		&InformationWindow::m_musicPlayButtonEvent,this
		, MusicPlayerUIs.at(3)));
	MusicPlayerUIs.at(2)->addClickEventListener([=](Ref* ref) {
		int num = Game_Player::getInstance()->getPlayMusicPoint();
		m_moveMusicPointButtonEvent(ref, num+1);
	});
	MusicPlayerUIs.at(3)->addClickEventListener(
		[=](Ref* ref) {
		if (
			cocos2d::experimental::AudioEngine::getState(m_mp3MusicNum) != cocos2d::experimental::AudioEngine::AudioState::ERROR) {
			bool MusicLoop = !cocos2d::experimental::AudioEngine::isLoop(m_mp3MusicNum);
			cocos2d::experimental::AudioEngine::setLoop(m_mp3MusicNum, MusicLoop);
			if (!MusicLoop) ((Button*)ref)->setColor(Color3B::WHITE);
			else((Button*)ref)->setColor(Color3B::GREEN);
		}
	});

	/*
	음악재생은 어떡해 할것인가?
	addclickeventlister로 만든다.
	누른 text의 tag값을 가지고 MusicList에서 찾는다.
	재생목록을 만드는 방법
	노래의 분류기준을 하나를 더만든다. 장르 : 신나는 곡, 웅장한 곡, 분위기 있는 곳
	노래 음향에 따라 대화 텍스트가 크기를 변경하자
	*/
	Size ListSize = Size(BackSize.width, BackSize.height*0.1f);

	Layout* PlayListLayout = Layout::create();
	PlayListLayout->setLayoutType(Layout::Type::HORIZONTAL);
	PlayListLayout->setContentSize(ListSize);
	m_musicBackLayout->addChild(PlayListLayout);

	char* ButtonName[4] = { u8"모든",u8"신나는",u8"웅장한",u8"분위기 있는" };
	for (auto i = 0; i < 4; i++) {
		Button* PlayListButton = Button::create(CUBE_PIECE_PNG);
		PlayListLayout->addChild(PlayListButton);
		PlayListButton->setScale9Enabled(true);
		PlayListButton->setContentSize(
			Size(ListSize.width * 0.25f, ListSize.height));
		PlayListButton->setColor(Color3B::GRAY);
		PlayListButton->setTitleFontName(FONT_NAME);
		PlayListButton->setTitleColor(Color3B::BLACK);
		PlayListButton->setTitleText(ButtonName[i]);
	}
	
	auto PlayerListButtons = PlayListLayout->getChildren();
	((Button*)PlayerListButtons.at(0))->addClickEventListener(std::bind(
		&InformationWindow::m_openAllMusicListWindow,this, MusicKind::ALL));
	((Button*)PlayerListButtons.at(1))->addClickEventListener(std::bind(
		&InformationWindow::m_openAllMusicListWindow,this, MusicKind::ExcitingMusic));
	((Button*)PlayerListButtons.at(2))->addClickEventListener(std::bind(
		&InformationWindow::m_openAllMusicListWindow,this, MusicKind::GrandMusic));
	((Button*)PlayerListButtons.at(3))->addClickEventListener(std::bind(
		&InformationWindow::m_openAllMusicListWindow,this, MusicKind::MagnificentMusic));

	m_openAllMusicListWindow(MusicKind::ALL);
}

//플레이어의 mp3에 있는 모든 음악을 나열한곳
void InformationWindow::m_openAllMusicListWindow(MusicKind musicKind) {
	if (m_musicBackLayout->getChildByName("MusicScrool") != nullptr)
		m_musicBackLayout->getChildByName("MusicScrool")->removeFromParent();

	auto MusicList = m_playerData->getHavMusic();

	Size BackSize = m_musicBackLayout->getContentSize();
	BackSize.height = BackSize.height*0.1f;

	float UIMargin = 0.0f;

	ScrollView* MusicScroll = ScrollView::create();
	m_musicBackLayout->addChild(MusicScroll);
	MusicScroll->setContentSize(Size(BackSize.width, BackSize.height *7));
	MusicScroll->setScrollBarEnabled(false);
	MusicScroll->setInertiaScrollEnabled(false);
	MusicScroll->setLayoutType(Layout::Type::VERTICAL);
	MusicScroll->setInnerContainerSize(
		Size(BackSize.width, (BackSize.height + UIMargin) * MusicList.size()));
	MusicScroll->setDirection(ScrollView::Direction::VERTICAL);
	MusicScroll->setName("MusicScrool");

	LinearLayoutParameter* Parameter = LinearLayoutParameter::create();
	Parameter->setMargin(Margin(0.0f, UIMargin, 0.0f, 0.0f));

	for (int i = 0; i < MusicList.size(); i++) {
		if (musicKind != MusicList.at(i).Kind && musicKind != MusicKind::ALL)
			continue;
		
		//음악의 이름과 프로필사진들의 레이아웃
		Layout* UIInformationLayout = Layout::create();
		MusicScroll->addChild(UIInformationLayout);
		UIInformationLayout->setLayoutParameter(Parameter);
		UIInformationLayout->setLayoutType(Layout::Type::HORIZONTAL);
		UIInformationLayout->setContentSize(BackSize);
		UIInformationLayout->setTouchEnabled(true);
		UIInformationLayout->setTag(i);
		UIInformationLayout->addClickEventListener([=](Ref* ref) {
			CCLOG("Music UI Click at(%d)", ((Node*)ref)->getTag());
			m_musicBackLayout->getChildren().at(0)->getChildren().at(0);
			m_moveMusicPointButtonEvent(m_musicBackLayout->getChildren().at(0)->getChildren().at(0)
				, ((Node*)ref)->getTag());
		});
		//음악의 프로필사진
		ImageView* MusicProFile = ImageView::create(MusicList.at(i).Body);
		UIInformationLayout->addChild(MusicProFile);
		MusicProFile->ignoreContentAdaptWithSize(false);
		MusicProFile->setContentSize(Size(BackSize.height, BackSize.height));
		
		//음악의 이름
		Text* MusicName = Text::create(MusicList.at(i).Name,FONT_NAME, BackSize.height);
		UIInformationLayout->addChild(MusicName);
		MusicName->setTextAreaSize(Size(BackSize.width - BackSize.height, BackSize.height));
	}

}

//플레이어 옵션창
void InformationWindow::m_openOptionWindow() {

	m_informationScene = InformationScene::OptionWindow;
	
	//옵션 UI이름들설정들
	std::vector<std::string> OptionUIName = {
		"Background Music",
		"Action Sound" ,
		"UI Sound",
		"FullScreen",
		"Start screen"
	};
	std::vector<std::string> SiderUIName = {
		"Background Music",
		"Action Sound" ,
		"UI Sound"
	};
	std::vector<std::string> BigSizeUIName = {
		"FullScreen",
		"Start screen"
	};
	std::vector<int> SliderPersent = {
		m_playerData->getGameSetting()->m_background_Music,
		m_playerData->getGameSetting()->m_action_Sound,
		m_playerData->getGameSetting()->m_ui_Sound
	};

	// ui정리 용도 Layout 생성.
	m_otionBackLayout = Layout::create();
	m_otionBackLayout->setLayoutType(Layout::Type::VERTICAL);
	m_otionBackLayout->setBackGroundColorType(Layout::BackGroundColorType::SOLID);
	m_otionBackLayout->setBackGroundColor(Color3B::GRAY);
	m_otionBackLayout->setContentSize(m_backGroundLayer->getContentSize());
	m_otionBackLayout->setAnchorPoint(Vec2::ANCHOR_BOTTOM_LEFT);
	m_backGroundLayer->addChild(m_otionBackLayout);

	//UI의 Click Event들
	std::vector<std::function<void(Ref* ref)>> UiClick_Event = {
		std::bind(&InformationWindow::m_fullScreen_CallBack, this, std::placeholders::_1),
		std::bind(&InformationWindow::m_back_Callback  ,   this)
	};

	//layout item 위치 설정 
	LinearLayoutParameter* OptionMainName_Margin = LinearLayoutParameter::create();
	OptionMainName_Margin->setGravity(LinearGravity::CENTER_HORIZONTAL);
	//퍼센트적으로 해야한다.
	OptionMainName_Margin->setMargin(ui::Margin(0.0f, 10.0f, 0.0f, 10.0f));
	std::vector<Text*> OptionText;
	//UI 제작  i 전체 ui 갯수, SliderNum slider 이벤트, clicknum click이벤트
	for (int i = 0, SliderNum = 0, ClickNum = 0; i < OptionUIName.size(); i++) {
		//ui text 생성

		OptionText.push_back(Text::create(OptionUIName[i], FONT_NAME, 15.0f));
		m_otionBackLayout->addChild(OptionText.back(), i);
		OptionText.back()->setTag(i);
		OptionText.back()->setTextColor(Color4B::BLACK);
		OptionText.back()->setLayoutParameter(OptionMainName_Margin);
		if (ClickNum < BigSizeUIName.size()) {

			if (OptionUIName.at(i).compare(BigSizeUIName.at(ClickNum)) == 0) {

				OptionText.back()->setFontSize(20.0f);
				OptionText.back()->addClickEventListener(UiClick_Event.at(ClickNum));
				OptionText.back()->setTouchEnabled(true);
				OptionText.back()->setTouchScaleChangeEnabled(true);
				ClickNum++;
				continue;
			}
		}
		if (SliderNum < SiderUIName.size()) {
			if (OptionUIName.at(i).compare(SiderUIName.at(SliderNum)) == 0) {

				//layout에 slider생성
				Slider* slider = Slider::create(SLIDER_BAR_PNG, SLIDE_THUMB_PNG);
				slider->setTag(SLIDER_ADDTAG + i);
				slider->setScale(1.0f);
				slider->addEventListener(std::bind(
					&InformationWindow::m_music_SliderEvent
					, this, std::placeholders::_1, std::placeholders::_2));
				m_otionBackLayout->addChild(slider, i);
				//Slider Layout 
				slider->setLayoutParameter(OptionMainName_Margin);
				slider->setPercent(SliderPersent.at(i));
				SliderNum++;
				continue;

			}
		}
	}
}


//아이템 정보창과 선택창 만들기
void InformationWindow::m_oepnItemInformationWindow() {

	m_informationScene = InformationScene::ItemInforWindow;
	std::vector<Game_Item> PL_Item = m_playerData->getHavItem();

	Size BackGround_Size = m_backGroundLayer->getContentSize();
	Layout* InformationLayout = Layout::create();
	m_backGroundLayer->addChild(InformationLayout);
	InformationLayout->setBackGroundColor(Color3B::GRAY);
	InformationLayout->setBackGroundColorType(LayoutBackGroundColorType::SOLID);
	InformationLayout->setAnchorPoint(Vec2::ANCHOR_BOTTOM_LEFT);
	InformationLayout->setContentSize(Size(BackGround_Size.width
		, BackGround_Size.height));
	InformationLayout->setName(ICON_LAYOUT);

	Layout* HoriLayout = Layout::create();
	InformationLayout->addChild(HoriLayout);
	HoriLayout->setContentSize(
		Size(BackGround_Size.width, BackGround_Size.height*0.2f));
	HoriLayout->setLayoutType(Layout::Type::HORIZONTAL);
	HoriLayout->setAnchorPoint(Vec2::ANCHOR_BOTTOM_LEFT);

	std::vector<char*> TitleText = { u8"닫기" };
	int k = 0;
	for (auto i = TitleText.begin(); i < TitleText.end(); i++) {
		Button* but = Button::create(WHITEBLOCK_PNG);
		HoriLayout->addChild(but);
		but->ignoreContentAdaptWithSize(false);
		but->setColor(Color3B::GRAY);
		but->setContentSize(BackGround_Size*0.2f);
		but->setTitleText((*i));
		but->setTitleFontName(FONT_ROUTE);
		but->setTitleFontSize(BackGround_Size.width*0.05f);
		but->setTitleColor(Color3B::BLACK);
		but->setName(ICON_TAGNAME);
		but->setTag(k++);
	}
	((Button*)HoriLayout->getChildren().back())->addClickEventListener(
		std::bind(&InformationWindow::m_backWindow, this));

	Label* ItemInformation
		= Label::create(PL_Item.at(m_iconXCount).information.c_str(),
			FONT_NAME, m_iconSize.width * 0.5f);
	InformationLayout->addChild(ItemInformation);
	ItemInformation->setPosition(HoriLayout->getContentSize());
	ItemInformation->setAnchorPoint(Vec2::ANCHOR_BOTTOM_RIGHT);
	ItemInformation->setContentSize(
		Size(BackGround_Size.width, BackGround_Size.height*0.8f));
	ItemInformation->setVerticalAlignment(TextVAlignment::TOP);
	ItemInformation->setDimensions(
		BackGround_Size.width, BackGround_Size.height*0.8f);
	ItemInformation->setMaxLineWidth(BackGround_Size.width);

	m_createIconAndMouse(ICON_TAGNAME);
}

/*
키보드로 아이템 화면의 마우스를 이동시키는것을 생각해보자

PreesedKeyboard는 키우스를 제어하는 것에 초점이 맞춰져있다.
대부분의 버튼 변수들은 m_backWindow나 m_changeWindow를 불러온다.

*/

//키보드 이벤트
//----------------------------씬 변환함수----------------------------

//point를가지고 열 창을 설정하는 함수 OnPressed와 연결되어있다.
void InformationWindow::m_changeWindow(Ref* ref) {

	m_iconXCount = 0;
	m_iconYCount = 0;
	
	Button* Retton = ((Button*)ref);
	CCLOG("Tag  = %d ", Retton ->getParent()->getParent()->getTag());

	Retton->getParent();
	// 클릭한 버튼이 아이템 화면일경우
	if (Retton->getName().compare(ICON_TAGNAME) == 0) {
		if (m_informationScene == InformationScene::ItemWindow)
			m_oepnItemInformationWindow();
		else if(m_informationScene == InformationScene::ItemInforWindow)
			m_backWindow();
		else if(m_informationScene == InformationScene::OperationButton)
			m_oepnItemInformationWindow();
	}
	//클릭한 버튼이 인터페이스 선택 화면일경우
	else if (Retton->getName().compare(DATA_BUTTON) == 0){
		m_backGroundLayer->getChildren().back()->removeFromParent();
		switch (Retton->getTag())
		{
		case 0:
			m_openItemWindow();
			break;
		case 1:
			m_openOptionWindow();
			break;
		case 2:
			m_openMusicWindow();
			break;
		}
	}
}

void InformationWindow::m_backWindow() {

	
	switch (m_informationScene) {
	case InformationScene::OperationButton:
		m_removeImageChild();
		m_playerData->setSceneInterface(SceneInterface::TileScene);
		return;
	case InformationScene::MusicWinodw:
	case InformationScene::OptionWindow:
	case InformationScene::ItemWindow:
		m_informationScene = InformationScene::OperationButton;
		m_createIconAndMouse(DATA_BUTTON);
		break;
	case InformationScene::ItemInforWindow:
		m_backGroundLayer->getChildren().back()->removeFromParent();
		m_openItemWindow();
		break;
	default:
		break;
	}

}

//----------------------------이벤트 함수----------------------------

// 키보드로 아이템 화면의 마우스를 이동시키는것함수
void InformationWindow::OnPreesedDataWindow(EventKeyboard::KeyCode kcode) {

	int ShortCount = m_pointIcon->getReferenceCount();
	auto ShortParent = m_pointIcon->getParent();
	m_pointIcon->removeFromParent();

	//문제가 생길수 있는것
	/*
	ShortParent->getParent()->getParent() 이기때문에
	InformationScene::Operation일경우 오류가 생길수있다.
	*/
	int CountSize = m_backGroundLayer->getChildren().back()->getChildrenCount();
	std::vector<Node*> IconLayout;
	if (ShortCount > 1) {
		Node* NodeChil = ShortParent;
		while (true) {
			if (NodeChil->getName().compare(ICON_LAYOUT) == 0) {
				for (auto i = NodeChil->getChildren().begin(); i < NodeChil->getChildren().end(); i++) {
					IconLayout.push_back((*i));
				}
				break;
			}
			NodeChil = NodeChil->getParent();
		}
	}

	switch (kcode)
	{
	case EventKeyboard::KeyCode::KEY_LEFT_ARROW:
		m_iconXCount--;
		break;
	case EventKeyboard::KeyCode::KEY_RIGHT_ARROW:
		m_iconXCount++;
		break;
	case EventKeyboard::KeyCode::KEY_UP_ARROW:
		m_iconYCount--;
		break;
	case EventKeyboard::KeyCode::KEY_DOWN_ARROW:
		m_iconYCount++;
		break;
	case EventKeyboard::KeyCode::KEY_Z:
		if (ShortCount > 1)
			m_changeWindow(ShortParent);
		return;
	case EventKeyboard::KeyCode::KEY_X:
		m_backWindow();
		return;
	default:
		break;
	}
	if (IconLayout.empty())
		return;

	if (m_iconYCount < 0)
		m_iconYCount = IconLayout.size() - 1;
	else if (m_iconYCount >= IconLayout.size())
		m_iconYCount = 0;
	if (m_iconYCount < 0 || m_iconYCount >= IconLayout.size())
		assert(false);

	//최대(소)사이즈를 넘을경우
	if (m_iconXCount < 0) {
		--m_iconYCount;
		if (m_iconYCount < 0)
			m_iconYCount = IconLayout.size() - 1;
		m_iconXCount = IconLayout.at(m_iconYCount)->getChildrenCount() - 1;
	}
	else if (m_iconXCount >= IconLayout.at(m_iconYCount)->getChildrenCount()) {
		++m_iconYCount;
		if (m_iconYCount >= IconLayout.size())
			m_iconYCount = 0;
		m_iconXCount = 0;
	}
	if (m_iconXCount < 0 || m_iconXCount >= IconLayout.at(m_iconYCount)->getChildrenCount())
		assert(false);

	IconLayout.at(m_iconYCount)->getChildren().at(m_iconXCount)->addChild(m_pointIcon);
}


//사운드 조절 Slider 이벤트
void InformationWindow::m_music_SliderEvent(Ref *pSender, Slider::EventType type) {
	Slider* slider = dynamic_cast<Slider*>(pSender);
	int percent = slider->getPercent();
	//text slider에 조절 
	Text* OptionText = (Text*)m_otionBackLayout->
		getChildByTag(slider->getTag() - SLIDER_ADDTAG);
	if (type == Slider::EventType::ON_PERCENTAGE_CHANGED) {
		int maxPercent = slider->getMaxPercent();
		OptionText->
			setString(StringUtils::format("%d", 100 * percent / maxPercent));
	}
	if (type == Slider::EventType::ON_SLIDEBALL_UP) {
		switch (OptionText->getTag())
		{
		case 0:
			OptionText->setString("Background Music");
			m_playerData->getGameSetting()->m_background_Music = percent;
			
			if(cocos2d::experimental::AudioEngine::getPlayingAudioCount > 0)
				cocos2d::experimental::AudioEngine::setVolume(m_backMusicID,percent * VOLUME_PS);
			break;
		case 1:
			OptionText->setString("Action Sound");
			m_playerData->getGameSetting()->m_action_Sound = percent;
			break;
		case 2:
			OptionText->setString("UI Sound");
			m_playerData->getGameSetting()->m_ui_Sound = percent;
			break;
		default:
			break;
		}
		m_playerData->getGameSetting()->m_save_GameData();
	}
}

void InformationWindow::m_setBackGroundMusic(std::string MusicName) {
	if (MusicName.size() > 0) {
		m_backMusicID = cocos2d::experimental::AudioEngine::play2d(MusicName, true);
		cocos2d::experimental::AudioEngine::setVolume(m_backMusicID
			, m_playerData->getGameSetting()->m_background_Music * VOLUME_PS);
	}
}

//풀스크린 이벤트
void InformationWindow::m_fullScreen_CallBack(Ref *pSender) {
	Text* text = (Text*)pSender;
	Size WindowSize = m_playerData->getGameSetting()->m_window_Size;

	m_playerData->getGameSetting()->m_fullScreen
		= !m_playerData->getGameSetting()->m_fullScreen;

	GLViewImpl* view = (GLViewImpl*)Director::getInstance()->getOpenGLView();
	if (m_playerData->getGameSetting()->m_fullScreen) {
		text->setString("Full/on");

		//view->setWindowed(WindowSize.width*2, WindowSize.height*2);
		//view->setDesignResolutionSize(
		//	WindowSize.width*2, WindowSize.height*2, ResolutionPolicy::SHOW_ALL);
		view->setFullscreen();
		CCLOG("ViewSIze %f %f", view->getFrameSize().width, view->getFrameSize().height);
		//m_backGroundLayer->getParent()->getParent()->getParent()->setScale(2.0f);
	}
	else {
		text->setString("Full/off");
		GLViewImpl* view = (GLViewImpl*)Director::getInstance()->getOpenGLView();
		view->setWindowed(WindowSize.width, WindowSize.height);
		view->setDesignResolutionSize(
			WindowSize.width, WindowSize.height, ResolutionPolicy::SHOW_ALL);
	}
	Game_Player::getInstance()->getGameSetting()->m_save_GameData();

}
//뒤로가기 이벤트
void InformationWindow::m_back_Callback() {
	cocos2d::experimental::AudioEngine::stopAll();
	m_playerData->setSceneInterface(SceneInterface::TileScene);
	m_removeImageChild();
	Director::getInstance()->replaceScene(MainMenu_Scene::create());
}

//음악을 실행하거나 멈추는 함수
void InformationWindow::m_musicPlayButtonEvent (Ref* ref) {
	auto PlayMusic = 
		m_playerData->getHavMusic().at(m_playerData->getPlayMusicPoint());
	switch (cocos2d::experimental::AudioEngine::getState(m_mp3MusicNum))
	{
	case cocos2d::experimental::AudioEngine::AudioState::ERROR:
		m_mp3MusicNum = cocos2d::experimental::AudioEngine::play2d(PlayMusic.MusicFile, false
			, m_playerData->getGameSetting()->m_background_Music);
		cocos2d::experimental::AudioEngine::setFinishCallback(m_mp3MusicNum, [=](int, std::string) {
	m_moveMusicPointButtonEvent(ref, m_playerData->getPlayMusicPoint()+1);
		});
		break;
	case cocos2d::experimental::AudioEngine::AudioState::PAUSED:
		cocos2d::experimental::AudioEngine::resume(m_mp3MusicNum);
		break;
	case cocos2d::experimental::AudioEngine::AudioState::PLAYING:
		cocos2d::experimental::AudioEngine::pause(m_mp3MusicNum);
		break;
	default:
		break;
	}

}
/*
musicPoint를 전달방법을 생각해보자
*/
//다른 음악을 실행시키는 함수
void InformationWindow::m_moveMusicPointButtonEvent(Ref* ref,int Position) {

	auto PlayMusicData = m_playerData->getHavMusic();
	m_playerData->setPlayMusicPoint(Position);

	if (!ref->_rooted) {
		//프로필을 바꾸고 반복재생을 끈다.
		if (((Node*)ref)->getTag() < 0) {
			Node* ParentRef = ((Node*)ref)->getParent();
			((ImageView*)ParentRef->getChildren().at(0))->loadTexture(
				PlayMusicData.at(m_playerData->getPlayMusicPoint()).Body);
			//반복재생을 끈다.(이미지 색상만 변경하는 것이지만)
			ParentRef->getChildren().back()->setColor(Color3B::WHITE);
		}
	}
	// 다음 곡으로 넘어간다.
	cocos2d::experimental::AudioEngine::stop(m_mp3MusicNum);
	m_musicPlayButtonEvent(ref);

}

//----------------------------삭제함수----------------------------

//informationWindow를 모두 제거한다.
void InformationWindow::m_removeImageChild() {
	if (m_backGroundLayer == nullptr)
		return;
	m_pointIcon->release();
	m_backGroundLayer->removeFromParent();
}

//enum Rank_List의 값들을 char* 로 변경한다.
std::string RankChangeText(Rank_List EnumRank) {
	char* TextRank;
	switch (EnumRank)
	{
	case Intern:
		TextRank = "인턴";
		break;
	case Clerk:
		TextRank = "사원";
		break;
	case Senior_Clerk:
		TextRank = "주임";
		break;
	case Assistant_Manager:
		TextRank = "대리";
		break;
	case Manager:
		TextRank = "과장";
		break;
	case Deputy_General_Manager:
		TextRank = "차장";
		break;
	case General_Manager:
		TextRank = "부장";
		break;
	case Managing_Director:
		TextRank = "상무";
		break;
	case Execurive_Managing_Director:
		TextRank = "전무";
		break;
	case Vice_President:
		TextRank = "부사장";
		break;
	case President:
		TextRank = "사장";
		break;
	default:
		TextRank = "";
		break;
	}
	return TextRank;
}
	