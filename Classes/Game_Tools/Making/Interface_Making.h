#pragma once

#include <cocos2d.h>
#include <ui/CocosGUI.h>

#include "Game_Data\Player_Data.h"


#define BACK_LAYOUT_NUM 1
#define MENU_LAYOUT_NUM 0

#define LABEL_SIZE 0.2f

#define ICON_TAGNAME "is Button"

#define ICON_LAYOUT "Icon_Layout"


class Interface_Making {
	
	Layout* CreateInterface_Making(std::vector<std::vector<std::function<void(int)>>>);

	Layout* getMakingInterface(char*);

	
};

bool CreateIconLayout(Node* IconParent, std::vector<Game_ComputerIcon> ComputerIcon
	, std::function<void(Ref* ref)> IconClickEvent);