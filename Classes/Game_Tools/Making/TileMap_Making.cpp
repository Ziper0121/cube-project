﻿#include <json\document.h>
#include <json\istreamwrapper.h>
#include <fstream>
#include <AudioEngine.h>
//#include <SimpleAudioEngine.h>


#include "Game_Library\G_ConstLibrary\TileMap_CCollection.h" 
#include "Game_Library\Game_FileRoute.h"
#include "Game_Tools\Making\TalkeWindow_Making.h" 
#include "Game_Tools\Making\TileMap_Making.h" 

TileMap_Tool* TileMap_Tool::m_tileTool = nullptr;
/*
연동 이벤트와 합동 이벤트를 만들고싶다.
대화후 맵이동, 대화후 이벤트씬, 대화와 같이 이벤트씬
스케줄을 사용하여 씬이 변할때를 체크한다.
그리고 다음 이벤트가 있을경우 
아니면 함수가 끝이 날때 공동 schedule을 사용해서 만들자
*/
/*
해야할것
npc의 고유 값들을 가지고있는다. 아니면 각지 ttf파일을 가지고있는다.
게임들어가는 장면바꾸기
흑백전환을 한다.
게임 화면을 정확하게 맞춘다.풀스크린까지
이동하는 방식을 고정 방향키 값으로 바꾸자
*/

void TileMap_Tool::m_createTool() {

	//게임 화면상태를 바꾼다.
	Game_Player::getInstance()->setSceneInterface(SceneInterface::TileScene);
	Director::getInstance()->setProjection(Director::Projection::_2D);
	
	//m_playerData->m_loardPlayerData();
	//m_playerData->getPlace();
	//Camera::createOrthographic(WinSize.width, WinSize.height,1,1000);
	
	m_playerData = Game_Player::getInstance();
	m_winScale = Director::getInstance()->getContentScaleFactor();
	m_winSize = m_playerData->getGameSetting()->m_window_Size;
	m_npcData = NpcController::createNpcControl();

	m_gameTime = Time_Controller::createTimeController();

	m_tileMapLayer = Node::create();
	m_tileMapLayer->setContentSize(m_winSize* 0.5f);
	m_tileMapLayer->setAnchorPoint(Vec2::ANCHOR_BOTTOM_LEFT);
	m_tileMapLayer->setPosition(Vec2(0.0f, 0.0f));

	m_playerData->setPlayerMap(GameMapPlace::TestMap);
	m_createAllTileMap();
	m_createTileMap();
	m_createPlayer();
	m_createNPC();
	auto GameKeyboardEvent = EventListenerKeyboard::create();
	GameKeyboardEvent->onKeyPressed = std::bind(&TileMap_Tool::m_keyboardPressed
		, this, std::placeholders::_1, std::placeholders::_2);
	GameKeyboardEvent->onKeyReleased = std::bind(&TileMap_Tool::m_keyboardReleased
		, this, std::placeholders::_1, std::placeholders::_2);
	Director::getInstance()->getEventDispatcher()->addEventListenerWithSceneGraphPriority
	(GameKeyboardEvent, m_tileMapLayer);
	Director::getInstance()->getScheduler()->schedule(
		[&](float dt) { m_gameTime->ScheduleTime(dt); }, this, 2.0f, false, "TimeControl");
	
	//schedule(schedule_selector(TileMap_Tool::keyPress_Char_Walking));
}

//맵의 데이터 전달
Node* TileMap_Tool::getTileMapLayer() {
	return m_tileMapLayer;
}

//다음 이벤트를 실행시킨다.
void TileMap_Tool::nextTileEvent() {
	auto TileValue = m_nowTileValue.find(NEXTTILE_TEXT);
	if (TileValue != m_nowTileValue.end()) {
		m_checkTileEvent(m_nowTileValue);
	}
}

void TileMap_Tool::m_keyboardPressed(EventKeyboard::KeyCode kcode, Event* et) {

	switch (m_playerData->getSceneInterface().pl_currentScene)
	{
	case SceneInterface::TileScene:
		switch (kcode) {

			//정보창 열기
		case EventKeyboard::KeyCode::KEY_X:
			m_PL_InterfaceEvent();
			break;
		case EventKeyboard::KeyCode::KEY_T: {
			std::vector<Vec2> Npc_Move;
			Npc_Move.push_back(Vec2(4.0f, 0.0f));
			m_npcData->m_navigation(0, GameMapPlace::Balcony, Npc_Move);
			break;
		}
		default:
			charMovePressed(kcode);
			break;
		}
		break;
	case SceneInterface::SaveScene:

		break;
	case SceneInterface::CubeGameScene:
	case SceneInterface::ComputerScene:
		m_computerTool.computerKeyPressed(kcode);
		break;
	case SceneInterface::TalkScene:
		m_talkeWindow.TalkWindowKeyEvent(kcode);
		break;
	case SceneInterface::InforScene:
		m_inforWindow.OnPreesedDataWindow(kcode);
		break;
	default:
		break;
	}

}
void TileMap_Tool::m_keyboardReleased(EventKeyboard::KeyCode kcode, Event* et) {

	switch (m_playerData->getSceneInterface().pl_currentScene)
	{
	case SceneInterface::TileScene:
		charMoveReleased(kcode);
		break;
	default:
		break;
	}

}
/*
맵에 조건 이동 만들기
json에 조건을 만들어 놓는다.
안될경우 json경로를 적어 놓는다.
해당 맵에 리스트들에 json경로들을 따로 적어놓는다.
*/
//인벤토리 생성
void TileMap_Tool::m_PL_InterfaceEvent() {
	for (int i = 0; i < MOVEKEY_SIZE; i++)
		m_pressedArrow[i] = EventKeyboard::KeyCode::KEY_NONE;
	m_emptyArrow = 0;

	m_playerData->setSceneInterface(SceneInterface::InforScene);
	m_tileMapLayer->addChild(m_inforWindow.getInformationWindow(), INFORMATION_TAG);
}

//모든 타일맵을 다 불러 와놓자
void TileMap_Tool::m_createAllTileMap() {
	auto MapCont = MapController::createMapController();
	auto AllTileMap = MapCont->getAllTileMap();
	for (auto i = AllTileMap.begin(); i < AllTileMap.end(); i++){
		m_tileScale = m_winSize.width / 128;
		(*i)->setScale(m_tileScale);
		m_tileMapLayer->addChild((*i));
	}
}
//타일맵 생성 함수
void TileMap_Tool::m_createTileMap() {

	//음악을 모두 끈다.
	//cocos2d::experimental::AudioEngine::uncacheAll();
	//걷는 소리를 준비하고 배경음악을 실행한다.
	//cocos2d::experimental::AudioEngine::preload(WalkingSound());
	//m_inforWindow.m_setBackGroundMusic(MapBackGroundMusic());

	/*
	std::string TMXTileMapFile = TILEMAP_TMX;
	Document MapListPasing
		= GetPaserDocument(GetTileMapList(
			false, m_playerData->getPlace()).back().c_str());

	if (MapListPasing.HasParseError()) {
		CCLOG("Pasing Error Code%d", MapListPasing.GetParseError());
		assert(false);
	}
	else
		TMXTileMapFile = MapListPasing["MapBody"].GetString();
	*/

	auto MapCont = MapController::createMapController();

	GameMapPlace PlayerMap = m_playerData->getPlace();
	//이유는 모르겠는데 이곳에서 scale값을 지정해야 오류가 안생긴다.
	m_tmap = MapCont->getTileMap(PlayerMap);
	m_metainfo = MapCont->getTileLayer(PlayerMap);
	m_objects = m_tmap->getObjectGroup("Objects");

	//맵을 삭제하고 다시만드는것이 아니라 다불러와놓자
	m_tmap->setVisible(true);
	CCLOG("Print Map Num = %d", m_playerData->getPlace());
}

//플레이어에 만드는 함수
void TileMap_Tool::m_createPlayer() {
	m_texture = Director::getInstance()->getTextureCache()->addImage(PLAYER_PNG);
	/*
	타일맵 좌표계
	사분면에 앤초포인터는
	타일 맵 기준 왼쪽 상단
	코코스의 기준 왼쪽하단
	asInt()는 현재 cocos의 기준에 맞춰주는 함수이다.(아니면 타일맵에 저장될때 그렇게 저장될수도 있다.)
	*/
	for (auto i = m_objects->getObjects().begin();
		i < m_objects->getObjects().end(); i++) {
		if ((*i).asValueMap()[TILECODE_TEXT].asInt() 
			!= TileSetKind::Spawn_Tile)
			continue;
		int x = (*i).asValueMap()[X_TEXT].asInt();
		int y = (*i).asValueMap()[Y_TEXT].asInt();

		m_player_StartPosition = Vec2(x, y);
		m_playerTilePoint = Vec2(x, y) / TILE_SIZE;
		CCLOG("SpawnTile Vec(%d,%d)", x, y);
		break;
	}
	m_playerData->setMapPoint(m_playerTilePoint);

	CCLOG("playerStartPosition(%f,%f)", m_playerTilePoint.x, m_playerTilePoint.y);

	CCLOG("playerStartPoint(%f,%f)", m_playerTilePoint.x, m_playerTilePoint.y);
	//m_player와 동기화를 시킨다
	//플레이어 생성
	m_player = Sprite::createWithTexture(m_texture, Rect(Vec2(0, 0), Size(
		m_tmap->getTileSize().width , m_tmap->getTileSize().height * 2)));
	//CharBody를 설정한다.
	m_playerData->Char_Body = m_player;
	m_player->setPosition(m_player_StartPosition);
	m_player->setScale(0.5f);
	
	//m_player->setGLProgramState(GLProgramState::getOrCreateWithGLProgramName(
	//	GLProgram::SHADER_NAME_POSITION_GRAYSCALE));
	m_player->setAnchorPoint(Vec2::ANCHOR_BOTTOM_LEFT);
	m_tmap->addChild(m_player, PLAYER_TAG);
	m_settingViewPoint();
}
//타일의 npc들을 만든다.
void TileMap_Tool::m_createNPC() {
	return;
	auto ObjectTile = m_objects->getObjects();
	for (auto i = ObjectTile.begin(); i < ObjectTile.end(); i++){
		if ((*i).asValueMap()[TILECODE_TEXT].asInt() 
			!= TileSetKind::Npc_Tile)
			continue;
			Game_NPC NpcData = 
				GetNPCData((*i).asValueMap()[NPCCODE_TEXT].asInt());
			Sprite* NpcBody = Sprite::create(NpcData.Body);
			int x = (*i).asValueMap()[X_TEXT].asInt();
			int y = (*i).asValueMap()[Y_TEXT].asInt();
			NpcBody->setPosition(Vec2(x, y));
			NpcBody->setAnchorPoint(Vec2::ANCHOR_BOTTOM_LEFT);
			m_tmap->addChild(NpcBody, 5);
	}
}

//플레이어의 시작 화면을 조정해주는 함수
void TileMap_Tool::m_settingViewPoint() {
	
	Vec2 PlayerPosition = m_player->getPosition();
	Vec2 TilePosition = m_tmap->getContentSize();
	Vec2 WinSize = m_winSize * 0.4;
	Vec2 MoveTMap = PlayerPosition*TILE_SCALE - WinSize;
	if (TilePosition.x *TILE_SCALE > m_winSize.width&& TilePosition.y *TILE_SCALE > m_winSize.height) {
		MoveTMap = MoveTMap / 16 / 3;
		MoveTMap.x = (int)MoveTMap.x;
		MoveTMap.y = (int)MoveTMap.y;
		MoveTMap = MoveTMap * TILE_SIZE  * TILE_SCALE;
		m_tmap->setPosition(-MoveTMap);
	}
	return;

	
}

/*
지우자
함수에서 받아와야하는것들
해당 json의 이름
갯수의 차이 
tilemap tool은 해당 조건에 맞는 이름
처음부터하기는 버튼의 이벤트는 모든 tool의 이름들을 
*/
//출력할 타일맵을 선택한다.
const char* TileMap_Tool::m_settingTileMap() {
	std::string ReturnString;

	//GetMapFileName을 ListfileName으로 바꾸자
	Document MapListPasing = GetPaserDocument(GetMapFileName());

	if (MapListPasing.HasParseError()) {
		CCLOG("Pasing Error Code%d", MapListPasing.GetParseError());
		assert(false);
	}
	auto MapListArray = MapListPasing["MapList"].GetArray();

	//맵을 만들고 
	for (auto i = MapListArray.begin(); i != MapListArray.end(); i++){
		Quest_Limited MapLimited;
		if (!(*i)["Limited"].Empty())
			continue;
		for (auto k = (*i)["Limited"].Begin(); k < (*i)["Limited"].End(); k++){
			for (auto m = (*k)["Ago_Quest"].Begin(); m < (*k)["Ago_Quest"].Begin(); m++) {
				NpcQuest_Num QuestNum;
				QuestNum.Npc = (*k)["Npc"].GetInt();
				QuestNum.Quest = (*k)["Quest"].GetInt();
				QuestNum.QuestFile = (*k)["QuestFile"].GetString();
				MapLimited.Ago_Quest.push_back(QuestNum);
			}
			for (auto m = (*k)["Item"].GetArray().Begin();
				m < (*k)["Item"].GetArray().end(); m++) {
				MapLimited.Item.push_back((*m).GetInt());
			}
			MapLimited.Rank = (Rank_List)(*k)["Rank"].GetInt();
		}
		if(CheckLimitidFun(MapLimited))
			ReturnString = (*i)["Name"].GetString();

	}
	return ReturnString.c_str();
}

// 키보드 이벤트
void TileMap_Tool::charMovePressed(EventKeyboard::KeyCode kcode ){
	//kcode값이 어느 배열에 저장되있는지 확인하는 for
	switch (kcode)
	{
	case EventKeyboard::KeyCode::KEY_LEFT_ARROW:
	case EventKeyboard::KeyCode::KEY_RIGHT_ARROW:
	case EventKeyboard::KeyCode::KEY_UP_ARROW:
	case EventKeyboard::KeyCode::KEY_DOWN_ARROW:
		m_pressedArrow[m_emptyArrow] = kcode;
		++m_emptyArrow;
		CCLOG("on key Pressed = %d", (int)kcode);
		CCLOG("m_emptyArrow = %d", m_emptyArrow);
		m_player_AllMove(kcode);
		break;
	default:
		break;
	}
}
void TileMap_Tool::charMoveReleased(EventKeyboard::KeyCode kcode) {
	//눌려있는 키보드값중 체크하는 for
	for (int i = 0; i < ARROW_KEY; i++) {

		//kcode값이 어느 배열에 저장되있는지 확인한다
		if (kcode != m_pressedArrow[i])
			continue;

		m_pressedArrow[i] = EventKeyboard::KeyCode::KEY_NONE;	
		--m_emptyArrow;

		//가장늦게 눌린 키보드일경우 || released가 진행후 키보드가 눌려있지 않을경우
		if (i == FNISH_PREESE || m_emptyArrow == NULL) {
			CCLOG("i(%d) == FINSIH_PREESE || m_emptyArrow(%d) == NULL", i, m_emptyArrow);
			return;
		}
		//press를 저장한 배열을 정리하는 if,for 중간에 비어있을경우 정리한다.
		else if (m_pressedArrow[i + 1] != EventKeyboard::KeyCode::KEY_NONE) 
			for (int k = i; k < FNISH_PREESE; k++) 
				m_pressedArrow[k] = m_pressedArrow[k + 1];

		m_player_AllMove(ARR_FILL);

		CCLOG("off key Ressed = %d", (int)kcode);
		CCLOG("m_emptyArrow = %d", m_emptyArrow);
		
		return;
	}
}

//키보드가 눌릴경우 움직일 이벤트들의 값들을 변경
void TileMap_Tool::m_player_AllMove(EventKeyboard::KeyCode kcode) {

	bool Turnanimation = false;
	int AnimationEncho = 0;
	switch (kcode)
	{
	case EventKeyboard::KeyCode::KEY_LEFT_ARROW:
		AnimationEncho = LEVEL_ANI;
		Turnanimation = true;
		m_player_MoveEncho = Vec2(-1, 0.0f);
		break;
	case EventKeyboard::KeyCode::KEY_RIGHT_ARROW:
		AnimationEncho = LEVEL_ANI;
		m_player_MoveEncho = Vec2(1, 0.0f);
		break;
	case EventKeyboard::KeyCode::KEY_UP_ARROW:
		AnimationEncho = UP_ANI;
		m_player_MoveEncho = Vec2(0.0f, 1);
		break;
	case EventKeyboard::KeyCode::KEY_DOWN_ARROW:
		AnimationEncho = DOWN_ANI;
		m_player_MoveEncho = Vec2(0.0f, -1);
		break;
	default:
		return;
	}
	m_ani_Encho = AnimationEncho;
	m_ani_turn = Turnanimation;

	//플레이어가 액션을 진행중일때 && 지금 가는 방향이 벽이거나 오브젝트의 경우
	if (m_player->getActionByTag(MOVE_ACT) 
		|| m_tmap->getActionByTag(MOVE_ACT) 
		|| m_check_TileObject())
		return;

	m_player_MoveByAct();
}

//키보드 이벤트
void TileMap_Tool::m_saveKeyboradPressed() {
}

/*
tilemap의 npc tileset들을 지우고 sprite로 만들어서 moveby를 한다.

이벤트를 진행할때 맵을 따로 만든다.

씬 이벤트는 다른 object 번호들의 tilecode로 실행된다.

json에 이동할 방향과 거리를 저장해놓는다.
같은 json파일에 저장해놓는가? 이름만 바꿔서 "Event_1" :{},"Event_2" :{}..등등

씬 이벤트를 변경해놓는다.

map이 변경되었을 경우를 생각하자
*/

//움직이는 위치 
void TileMap_Tool::m_player_MoveByAct() {

	//눌린 키보드값이 없을때
	if (m_emptyArrow == NULL || m_check_TileObject()) {
		return;
	}

	//걷는소리
	//cocos2d::experimental::AudioEngine::setVolume(
	//	cocos2d::experimental::AudioEngine::play2d(WalkingSound()), 
	//	Game_Player::getInstance()->getGameSetting()->m_action_Sound*VOLUME_PS);

	Vec2 PlayerMoveEncho = m_player_MoveEncho * TILE_SIZE ;
	float Move_Power = 0.4f;
	//m_moveCharMoveView(PlayerMoveEncho);
	m_playerTilePoint += m_player_MoveEncho;
	//플레이어데이터를 변경한다.
	m_playerData->setMapPoint(m_playerTilePoint);
	CCLOG("Player Point(%f,%f)", m_playerTilePoint.x, m_playerTilePoint.y);

	//이동하는 방향 - +를 정한다.
	float MapMoveEncho = 1.0f;

	auto animation = Animation::create();
	animation->setDelayPerUnit(Move_Power* 0.5f);
	static int AniTexture = 3;
	int Minimum = AniTexture - 2;
	//걷는 모션을 뒤에서부터 불러온다.
	for (int k = AniTexture; k > Minimum; k--, AniTexture--)
		animation->addSpriteFrameWithTexture(m_texture, m_playerEncho_Texture(k));
	if (AniTexture <= 0){
		AniTexture = 3;
	}

	auto animate = Animate::create(animation);
	m_player->setFlipX(m_ani_turn);
	m_player->runAction(animate);

	//애니메이션 진행시 자동으로 변경되는 sprite값들을 변경한다. 
	auto Char_MoveBy = MoveBy::create(Move_Power, PlayerMoveEncho );
	// Spawn* Test_Spawn = Spawn::create(Char_MoveBy, animate, NULL);
	auto ActionCallback = CallFunc::create(std::bind(&TileMap_Tool::m_player_MoveByAct, this));
	//CallFUnck를 중간 중간에 끼어 넣었었다. 이유는 잘 모르겠다
	m_repeat_WalkAction = Sequence::create(Char_MoveBy, ActionCallback, NULL);
	m_repeat_WalkAction->setTag(MOVE_ACT);
	m_player->runAction(m_repeat_WalkAction);

	//Map을 이동할지 결정한다.
	auto MoveMap = m_moveCharMoveView(PlayerMoveEncho);
	if (MoveMap != nullptr) {
		auto Char_MoveBy = MoveBy::create(Move_Power, PlayerMoveEncho* -TILE_SCALE);
		MoveMap->runAction(Char_MoveBy);
	}
	
}

//플레이어가 움직일 방향의 texture의 rect를 전달한다.
Rect TileMap_Tool::m_playerEncho_Texture(int encho){
	//Texture의 방향과 위치를 설정한다.
	Vec2 Texture_origin=Vec2(m_tmap->getTileSize().width * encho,
			m_tmap->getTileSize().height * m_texturScale *m_ani_Encho);
	Size Texture_Size = Size(m_tmap->getTileSize().width
		, m_tmap->getTileSize().height * m_texturScale);
	
	Rect Player_Encho = Rect(Texture_origin, Texture_Size);
	
	return Player_Encho;
}

//플레이어가 화면조정 함수 
Node* TileMap_Tool::m_moveCharMoveView(Point moveEncho) {
	Size TilemapSize = m_tmap->getContentSize() *TILE_SCALE ;

	Node* MoveTarget = nullptr;
	if (m_winSize.width < TilemapSize.width) {
		MoveTarget = m_tmap;
	}
	if (m_winSize.height< TilemapSize.height) {
		MoveTarget = m_tmap;
	}
	
	return MoveTarget;
}

//충돌체크 함수   움직일수 없을경우 true반환 
bool TileMap_Tool::m_check_TileObject() {

	bool CheckTile = false;

	//움직일 위치를 가진 변수 그리고 point로 변환 (1,1)(1,0)
	Point Checking_Point = m_playerTilePoint + m_player_MoveEncho;

	//맵의 최대,소 치를 못넘어가게 했다.
	if (Checking_Point.x >= m_tmap->getMapSize().width || Checking_Point.x < 0.0f
		|| Checking_Point.y >= m_tmap->getMapSize().height || Checking_Point.y < 0.0f)
		return true;

	//tilemap은 y값이 반대이고 한칸이 더 낮다.
	Vec2 TileGidat = Checking_Point;
	TileGidat.y = m_tmap->getMapSize().height - TileGidat.y - 1;
	int TileGid = m_metainfo->getTileGIDAt(TileGidat);

	//Npc와 부딪힐경우 움직임을 멈춘다. 테스트 임시
	//if (m_check_NpcObject(Checking_Point)) {
	//	std::vector<Game_Char> Chars = { *m_playerData };
	//	TestTextureWindow::createTextureWindow()->getTextureWindow(Chars);
	//	return true;
	//}
	////tileGid에 아무것도 없을경우
	//else if (TileGid == 0)
	//	return CheckTile;

	auto BlockKind = m_objects->getObjects();
	CCLOG("To Get Hit  Tile(%d)", m_playerEvent);

	Vec2 TilePosition;
	int ObjectTileNum = 0;

	CheckTile = true;

	//플레이어가 벽에 기대는 방향을 정해준다.
	m_player->setTextureRect(m_playerEncho_Texture());
	m_player->setFlipX(m_ani_turn);
	

	// 충돌되었을경우 그곳에 있는 object가 있는지 검사한다.
	for (ObjectTileNum = 0; ObjectTileNum < BlockKind.size(); ObjectTileNum++) {

		//지금 체크하는 블럭
		auto CheckBlock = BlockKind.at(ObjectTileNum);

		//타일 사이즈가 16이다
		TilePosition.x = CheckBlock.asValueMap()[X_TEXT].asFloat() / 16;
		TilePosition.y = CheckBlock.asValueMap()[Y_TEXT].asFloat() / 16;
		// 부딪힌 타일 셋에 object가 있는지 확인한다.
		if (TilePosition== Checking_Point) {

			if (BlockKind.size() == ObjectTileNum)
				return CheckTile;
			//현재 이벤트가 진행중인 타일값
			m_nowTileValue = CheckBlock.asValueMap();
			//부딪힌 곳의 ObjectMap을 보낸다.
			m_checkTileEvent(CheckBlock.asValueMap());

			return CheckTile;
		}
	}
}

//Npc를 체크한다
bool TileMap_Tool::m_check_NpcObject(Point PlayerPoint) {
	bool CheckNpc = true;
	Npc_Char NpcSchedule = NpcController::createNpcControl()
		->getNpcBlock(PlayerPoint, m_playerData->getPlace());
	NpcSchedule.checkNpcThink();
	return CheckNpc;
}


//맵과 맵간의 이동함수
void TileMap_Tool::m_portallEvent(cocos2d::ValueMap  ObjectMap) {

	//맵을 이동하면 그 지역 짝이 맞는 장소로 간다.
	cocos2d::ValueMap WarpPoint;
	
	//check할 map의 objects
	ValueVector Objects;
	//warpStartePoint의 번호 Kind번호
	const int PortalKindNum = ObjectMap["PortallKind"].asInt();
	
	//map kind가 0인 값들은 맵내에서 이동하는 포탈이다.
	switch (PortalKindNum) {
	case 0:
		Objects = m_objects->getObjects();
		break;
	default:
		//이동할 위치로 맵의 정보를 변경한다. 맵의 정보는 편의상 1을 더해놓았다.
		m_playerData->setPlayerMap(
			(GameMapPlace)(ObjectMap[PTLLMAP_TEXT].asInt()));

		//바꿔지는 타일맵을 투명으로 만들고 비운다.
		m_tmap->setVisible(false);
		m_createTileMap();
		//바뀌는 타일맵을 보이게한다.
		m_tmap->setVisible(true);

		//플레이어를 다른tmap으로 옮긴다.
		m_player->retain();
		m_player->removeFromParent();
		m_tmap->addChild(m_player,0);
		m_player->release();
		m_createNPC();
		Objects = MapController::createMapController()->
			getTileObject(m_playerData->getPlace());
		break;
	
	}

	for (auto i = Objects.begin();i < Objects.end(); i++) {
		//타일코드가 포탈코드인지 확인한다.
		if ((*i).asValueMap()[TILECODE_TEXT].asInt() 
			!= TileSetKind::Portal_Tile)
			continue;
		CCLOG(" potral : %s", (*i).asValueMap()["name"].asString().c_str());
		// i = begin을 만들고싶다. 그런데 i!= valueNum이 걸린다.
		if ((*i).asValueMap()[PTLLCODE_TEXT ].asInt()
			== ObjectMap[PTLLCODE_TEXT ].asInt()
			&& (*i).asValueMap()[PTLLEXIT_TEXT].asBool()) {
			WarpPoint = (*i).asValueMap();
			break;
		}
	}

	int x = WarpPoint[X_TEXT].asInt();
	int y = WarpPoint[Y_TEXT].asInt();

	m_playerTilePoint = Point(x, y) / TILE_SIZE ;
	m_player->setPosition(Vec2(x, y));
	m_settingViewPoint();
	nextTileEvent();
	
}
//대화창 생성
void TileMap_Tool::m_talkwindowEvent(cocos2d::ValueMap ObName) {
	for (int i = 0; i < MOVEKEY_SIZE; i++) 
		m_pressedArrow[i] = EventKeyboard::KeyCode::KEY_NONE;
	m_emptyArrow = 0;
	Game_Player::getInstance()->setSceneInterface(SceneInterface::TalkScene);

	//대화창 만들기
	m_tileMapLayer->addChild(m_talkeWindow.getbackTalkWindow(
		ObName[NPCCODE_TEXT].asInt()),TALK_TAG);
}
//컴퓨터 생성	
void TileMap_Tool::m_computerEvent() {

	for (int i = 0; i < MOVEKEY_SIZE; i++) 
		m_pressedArrow[i] = EventKeyboard::KeyCode::KEY_NONE;
	
	m_emptyArrow = 0;
	Game_Player::getInstance()->setSceneInterface(SceneInterface::ComputerScene);
	m_tileMapLayer->addChild(m_computerTool.getBackGround(), COMPUTER_TAG);

	
}
//플레이어의 데이터를 저장한다.
void TileMap_Tool::m_charSaveEvent() {	
	m_charTalkEvent(1);
}

/*
세이브 방법의 문제점들
1. 대화를 마쳤을때마다 바로 값을 변경하고 저장을하는 방식
문제점 : 세이브를 하지 않았는데도 데이터가 저장되있다.
2. 모든 정보를 다 가지고 있다가 세이브할때 한번에 저장하는 방식
문제점 : 다음 대화로 넘어가지를 못한다.
*/

//플레이어의 이벤트 타일
void TileMap_Tool::m_charTalkEvent(int Num) {
	for (int i = 0; i < MOVEKEY_SIZE; i++)
		m_pressedArrow[i] = EventKeyboard::KeyCode::KEY_NONE;
	m_emptyArrow = 0;
	Game_Player::getInstance()->setSceneInterface(SceneInterface::TalkScene);

	m_tileMapLayer->addChild(
		m_talkeWindow.getbackTalkWindow(1, TALKEVENT_JSON, TALK_SAVEEVENT), TALK_TAG);
}

//타일에 따른 이벤트를 실행하는 함수
void TileMap_Tool::m_checkTileEvent(cocos2d::ValueMap Objects) {
	
	TileSetKind TileKind =
		(TileSetKind)Objects[TILECODE_TEXT].asInt();

	//충돌한 object가 종류를 검사한다.
	switch (TileKind)
	{
	case TileSetKind::Save_Tile:
		CCLOG("Save Tile");
		m_charSaveEvent();
		break;
	case TileSetKind::Npc_Tile:
		CCLOG("NPC Tile");
		m_talkwindowEvent(Objects);
		break;
	case TileSetKind::Event_Tile:
		CCLOG("Event Tile");
		m_computerEvent();
		break;
	case TileSetKind::Portal_Tile:
		CCLOG("Portal Tile");
		m_portallEvent(Objects);
		break;
	default:
		break;
	}

}


//필요 없는 함수들
Point TileMap_Tool::m_tileCoordForPosition(Point position) {
	int x = position.x / m_tmap->getTileSize().width;
	//tilemap은 1사분면이 아닌 4사분면이기 때문에 tilemap의 좌표를 얻을려면
	//현재 플레이어 위치와 tilemap의 위치를 빼야한다.
	int y = ((m_tmap->getMapSize().height * m_tmap->getTileSize().height) - position.y)
		/ m_tmap->getTileSize().height;
	//cocos2d좌표계는 tilemap보다 높이가 1높다.
	y -= 1;
	CCLOG("(%d,%d)", x, y);
	return Point(x, y);
}
void TileMap_Tool::keyPress_Char_Walking(float dt) {
#pragma region REGION_WAKING_ACTION
	//방향키가 눌리지 않았을 때
	if (!m_emptyArrow) return;

	Vec2 playerMoving = m_player->getPosition();
	Vec2 ArrowEncho = Vec2(0, 0);

	//Player가 움직일 방향과 힘을 정한다.
	switch (ARR_FILL)
	{
	case EventKeyboard::KeyCode::KEY_DOWN_ARROW:
		ArrowEncho.y = M_ONE;
		break;
	case EventKeyboard::KeyCode::KEY_UP_ARROW:
		ArrowEncho.y = P_ONE;
		break;
	case EventKeyboard::KeyCode::KEY_RIGHT_ARROW:
		ArrowEncho.x = P_ONE;
		break;
	case EventKeyboard::KeyCode::KEY_LEFT_ARROW:
		ArrowEncho.x = M_ONE;
		break;
	default:
		break;
	}

	//윈도우 사이즈에 비례해서 크기 설정
	playerMoving.x += ArrowEncho.x  * m_tmap->getTileSize().width *  TILE_PER * m_winSize.width / 1000;
	playerMoving.y += ArrowEncho.y *  m_tmap->getTileSize().height * TILE_PER * m_winSize.height / 1000;
	//집에서 해봐야하는 것        CC_FIX_ARTIFACTS_BY_STRECHING_TEXEL  0
	//playerMoving.x += winSize.width / 1000;
	//playerMoving.y += winSize.height / 1000;



#pragma endregion REGION_WAKING_ACTION

	if (m_check_ContactObject(playerMoving))
		m_player->setPosition(playerMoving);

	return;
}
bool TileMap_Tool::m_check_ContactObject(Point position) {

	//비율 맞추기
	position = position / m_tileScale *  m_winScale;

	position.x = position.x / m_tmap->getTileSize().width;
	position.y = position.y / m_tmap->getTileSize().height;

	position.y = (m_tmap->getMapSize().height - 1) - position.y;

	Vec2 tileCoord[2] = { position,position };

	for (int i = 0; i < 2; i++) {

		//Player가 움직일 방향과 힘을 정한다.
		
		//소수점 올림과 내림을 사용
		switch (ARR_FILL)
		{
		case EventKeyboard::KeyCode::KEY_DOWN_ARROW:
			tileCoord[i].y = ceil(tileCoord[i].y);
			if (i)tileCoord[i].x = floor(tileCoord[i].x);
			else tileCoord[i].x = ceil(tileCoord[i].x);
			break;
		case EventKeyboard::KeyCode::KEY_UP_ARROW:
			tileCoord[i].y = floor(tileCoord[i].y);
			if (i)tileCoord[i].x = floor(tileCoord[i].x);
			else tileCoord[i].x = ceil(tileCoord[i].x);
			break;
		case EventKeyboard::KeyCode::KEY_RIGHT_ARROW:
			tileCoord[i].x = ceil(tileCoord[i].x);
			if (i)tileCoord[i].y = floor(tileCoord[i].y);
			else tileCoord[i].y = ceil(tileCoord[i].y);
			break;
		case EventKeyboard::KeyCode::KEY_LEFT_ARROW:
			tileCoord[i].x = floor(tileCoord[i].x);
			if (i)tileCoord[i].y = floor(tileCoord[i].y);
			else tileCoord[i].y = ceil(tileCoord[i].y);
			break;
		default:
			break;
		}
		
		if (tileCoord[i].x < 0 || tileCoord[i].x >= m_tmap->getMapSize().width
			|| tileCoord[i].y < 0 || tileCoord[i].y >= m_tmap->getMapSize().height) {
			return false;
		}

		CCLOG("pos (%f , %f)", position.x, position.y);

		int tileGid = m_metainfo->getTileGIDAt(tileCoord[i]);

		cocos2d::Value properties = m_tmap->getPropertiesForGID(tileGid);

		if (properties.isNull()) {
			continue;
		}

		switch (tileGid)
		{
		case T_WALL:
			CCLOG("Wall...\n");
			return false;
		case T_ITEM:
			m_metainfo->removeTileAt(tileCoord[i]);
			//m_items->removeTileAt(tileCoord[i]);
			CCLOG("GetItems\n");
			continue;
		default:
			continue;
		}
		CCLOG("\n");
	}
	return true;
}
/*
player AnchoPoint 0,0
-1,+A,+A 
-1,+A,+A 
-2,-1,-0 

void TileMap_Tool::MoveSchedule(float dt) {

#pragma region REGION_WAKING_ACTION
	//방향키가 눌리지 않았을 때
	if (!m_emptyArrow) return;

	int animationEncho = 0;
	bool turnanimation = false;

	Vec2 playerMoving = player->getPosition();

	Vec2 ArrowEncho = Vec2(0, 0);

	//Player가 움직일 방향과 힘을 정한다.

	//right
	if (ARR_FILL == ENCHO_R) {
		ArrowEncho.x = P_ONE;
		animationEncho = 3;
	}
	//left
	else if (ARR_FILL == ENCHO_L) {
		ArrowEncho.x = M_ONE;
		animationEncho = 3;
		turnanimation = true;
	}
	//tile좌표를 사용하고 있기때문에 올라가는 방향키일경
	//Down
	else if (ARR_FILL == ENCHO_D) {
		ArrowEncho.y = M_ONE;
		animationEncho = 1;
	}
	//UP
	else if (ARR_FILL == ENCHO_U) {
		ArrowEncho.y = P_ONE;
		animationEncho = 2;
	}

	playerMoving.x += ArrowEncho.x  * m_tmap->getTileSize().width *  TILE_PER;
	playerMoving.y += ArrowEncho.y *  m_tmap->getTileSize().height * TILE_PER;

#pragma endregion REGION_WAKING_ACTION
	if (!m_check_ContactObject(playerMoving)) return;

	m_minursTile += ArrowEncho;

	if (m_minursTile.x > 9) {
		m_player_StartPosition.x += 1;
		m_minursTile.x = 0;
	}
	else if (m_minursTile.x < 0) {
		m_player_StartPosition.x -= 1;
		m_minursTile.x = 9;
	}

	if (m_minursTile.y > 9) {
		m_player_StartPosition.y += 1;
		m_minursTile.y = 0;
	}
	else if (m_minursTile.y <0) {
		m_player_StartPosition.y -= 1;
		m_minursTile.y = 9;
	}

	player->setPosition(playerMoving);

	//애니메이션
	return;
	animation = Animation::create();
	animation->setDelayPerUnit(0.09f);
	for (int i = 2; i >= 0; i--) {
		animation->addSpriteFrameWithTexture(texture, Rect
		(Vec2(m_tmap->getTileSize().width / m_winScale * i, m_tmap->getTileSize().height / m_winScale *animationEncho),
			m_tmap->getTileSize() / m_winScale));
	}

	animate = Animate::create(animation);
	//By값을 설정하자
	moveby = MoveBy::create(0.1f, Vec2(0, 0));

	animate->setTag(ANI_ACT);
	moveby->setTag(CH_NUM);

	player->runAction(animate);
	player->runAction(moveby);

	//애니메이션 진행시 자동으로 변경되는 sprite값들을 변경한다. 
	player->setFlipX(turnanimation);
}
*/

//CocosDenshion::SimpleAudioEngine::getInstance()->stopAllEffects();
//cocos2d::experimental::AudioEngine::stopAll();
