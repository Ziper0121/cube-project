﻿#include <fstream>
#include <AudioEngine.h>

#include "Game_Tools\Making\TalkeWindow_Making.h" 
#include "Game_Data\Player_Data.h"
#include "Game_Data\TalkObject_Data.h" 
#include "Game_Tools\Making\Npc_Making.h" 
#include "Game_Tools\Making\TileMap_Making.h"

#define NOW_VECTTALLK m_nowTalkData.talkInteface.at(m_vecTextNum)

#define IS_NULL 0 >=

using namespace rapidjson;

Layout* TalkeWindow_Tool::getbackTalkWindow(
	int NPC_Num, std::string NPC_File, int EventNum) {

	m_talkingNPC = GetNPCData(NPC_Num, NPC_File);

	m_eventNumber = EventNum;

	//플레이어의 아이템들
	std::vector<Game_Item> PlayerItem
		= Game_Player::getInstance()->getHavItem();
	//플레이어의 직급
	auto PlayerRank = Game_Player::getInstance()->getPLRank();
	//is_Talk(대화를 나누었는지?)여부 저장되지 않은 데이터
	std::vector<Clear_Json>TalkClear= Game_Player::getInstance()->
		getHavClear(JsonKind::Map_Json, NPC_File.c_str(), NPC_Num);

	//제한조건이 충족이 되었는지 확인
	bool Check_LimitedQuest = false;

	//저장된 대화의 처음부분
	auto ClearPosition = TalkClear.begin();
	//대화 조건을 검사한다.
	for (auto& i = m_talkingNPC.Text.begin(); i < m_talkingNPC.Text.end(); i++) {

		//지금 텍스트 번호가 0보다 아래일경우,내 랭크에 맞지않는 텍스트일경우,이후 텍스트로 못넘어갈경우
		if (IS_NULL i->unique_Number || PlayerRank < i->limited.Rank || Check_LimitedQuest)
			if (i == m_talkingNPC.Text.begin()) {
				m_removeOutputTalk();
				return nullptr;
			}
			else
				break;

		//선행조건이 완료되었는지 확인하는 변수
		Check_LimitedQuest = false;

		//NPC의 퀘스트 limited를 받아서 강제로 전환한다.
		//조건이 충족되지 못하면 강제로 나가게 설계되었다.

		//선행 퀘스트를 깻는지 확인한다.
		if (i->is_Talk) {
			m_nowTalkData = *i;
			m_nowTalkData.is_Talk = true;
			continue;
		}
		//이미 나눈 대화인지 확인하고 이미 나눴다면 is_talk를 변환한다.
		else if (ClearPosition != TalkClear.end())
			if (i->unique_Number == (*ClearPosition).JsonNumber.back()) {
				m_nowTalkData = *i;
				m_nowTalkData.is_Talk = true;
				ClearPosition++;
				continue;
			}

		//선행조건의 완수 여부를 확인한다.
		Check_LimitedQuest = CheckLimitidFun((*i).limited);

		if (!Check_LimitedQuest)
			m_nowTalkData = *i;
	}

	m_createTalkWindow();

	return m_talkBackLayout;
}
//talkwindow키보드 이벤트
void TalkeWindow_Tool::TalkWindowKeyEvent(EventKeyboard::KeyCode kCode) {
	switch (kCode)
	{
	case EventKeyboard::KeyCode::KEY_Z:
		if (m_textEnd) {
			if (NOW_VECTTALLK.OnButton)
				m_buttonLayout->removeFromParent();
			m_vecTextNum++;
			m_textEnd = false;
			if (m_nowTalkData.talkInteface.size() > m_vecTextNum) {
				m_resetTextBox();
				break;
			}
			m_removeOutputTalk();
		}
		break;
	case EventKeyboard::KeyCode::KEY_CTRL: {
		if (m_textEnd)
			break;
		for (auto i = 0; i < m_32Text.size(); i++) {
			if (isspace(m_32Text.at(i)))
				continue;
			m_outputTalk->getLetter(i)->setOpacity(255.0f);
			m_outputTalk->getLetter(i)->stopAllActions();
		}
		m_textEnd = true;
		break;
	}
	default:
		break;
	}
	if (NOW_VECTTALLK.OnButton) {

		switch (kCode)
		{
		case EventKeyboard::KeyCode::KEY_LEFT_ARROW:

			m_setEventButton(-1);
			break;
		case EventKeyboard::KeyCode::KEY_RIGHT_ARROW:

			m_setEventButton(+1);
			break;

		default:
			break;
		}
	}
}
//대화창 뒤에 까만 창을 만든다.
bool TalkeWindow_Tool::m_createTalkWindow() {
	if (m_nowTalkData.unique_Number < 0) {
		CCLOG("talkString is empty");
		return false;
	}
	Size WinSize = Game_Player::getInstance()->getGameSetting()->m_window_Size;
	cocos2d::experimental::AudioEngine::preload(TALK_SOUND);


	//대화창에 뒤에 검은색 바탕을 만드는 변수
	m_talkBackLayout = Layout::create();
	m_talkBackLayout->setLayoutType(Layout::Type::VERTICAL);
	m_talkBackLayout->setBackGroundImage(CUBE_PIECE_PNG);
	m_talkBackLayout->setBackGroundImageScale9Enabled(true);
	m_talkBackLayout->setColor(Color3B::GRAY);
	m_talkBackLayout->setContentSize(
		Size(WinSize.width* 0.9f, WinSize.height * 0.3f));
	m_talkBackLayout->setPosition(
		Vec2(WinSize.width*0.5f, WinSize.height *0.1f));
	m_talkBackLayout->setAnchorPoint(Vec2::ANCHOR_MIDDLE_BOTTOM);
	m_talkBackLayout->setTag(TALKTEXT_TAG);
	
	auto FaceCollecateLayout = Layout::create();
	FaceCollecateLayout->setLayoutType(Layout::Type::HORIZONTAL);
	FaceCollecateLayout->setContentSize(m_talkBackLayout->getContentSize());
	FaceCollecateLayout->setTag(FACELAYOUT_TAG);
	m_talkBackLayout->addChild(FaceCollecateLayout);

	m_resetTextBox();
	return true;
}

//대화할때 char이미지
//npc머리번호와 지정 위치,감정 상태 

//출력할 대화내용 설정
void TalkeWindow_Tool::m_resetTextBox() {


	auto ParentLayout = m_talkBackLayout->getChildByTag(FACELAYOUT_TAG);

	if (ParentLayout->getChildrenCount() >0)
		ParentLayout->removeAllChildren();


	if (NOW_VECTTALLK.OnButton) {
		auto FaceLayout =m_talkBackLayout->getChildByTag(FACELAYOUT_TAG);
		FaceLayout->setContentSize(Size(
			FaceLayout->getContentSize().width
			, FaceLayout->getContentSize().height* 0.8f));
	}

	//대화 상대가 없을때(얼굴변수 값이 비었을때)
	if (m_nowTalkData.talkInteface.size()==0){
		m_createTalkText();
	}
	else {
		switch (m_nowTalkData.talkInteface.at(m_vecTextNum).SayObjectEncho)
		{
		case FaceEncho::LeftFace:
			m_createSayFace();
			m_createTalkText();
			break;
		case FaceEncho::RightFace:
			m_createTalkText();
			m_createSayFace();
			break;
		default:
			break;
		}
	}

	m_createChoiceButton();

}
//선택버튼을 만드는
void TalkeWindow_Tool::m_createChoiceButton() {

	if (!NOW_VECTTALLK.OnButton)
		return;
	Size ButLayeSize
		= m_talkBackLayout->getContentSize();
	ButLayeSize.height *= 0.2f;

	m_buttonLayout = Layout::create();
	m_buttonLayout->setLayoutType(Layout::Type::HORIZONTAL);
	m_buttonLayout->setContentSize(ButLayeSize);
	m_buttonLayout->setBackGroundColorType(Layout::BackGroundColorType::SOLID);
	m_buttonLayout->setBackGroundColor(Color3B::BLUE);
	m_buttonLayout->setAnchorPoint(Vec2(0.5f, 0.5f));
	m_talkBackLayout->addChild(m_buttonLayout, 1);

	LinearLayoutParameter* m_buttonLayoutPrameter = LinearLayoutParameter::create();
	m_buttonLayoutPrameter->setGravity(LinearLayoutParameter::LinearGravity::BOTTOM);
	m_buttonLayout->setLayoutParameter(m_buttonLayoutPrameter);

	const char buttonText[2][4] = { "Yes", "No" };

	for (int i = 0; i < 2; i++)
	{

		Button* ChoiceButton = Button::create(WHITEBLOCK_PNG);
		ChoiceButton->ignoreContentAdaptWithSize(false);
		ChoiceButton->setContentSize(
			Size(ButLayeSize.width*0.5f, ButLayeSize.height));
		ChoiceButton->setTitleText(buttonText[i]);
		ChoiceButton->setTitleFontSize(20);
		ChoiceButton->setTitleFontName(FONT_NAME);
		ChoiceButton->setTitleColor(Color3B::BLACK);
		m_buttonLayout->addChild(ChoiceButton, i, buttonText[i]);

	}

	m_setEventButton(0);
}

//말하고있는 대상의 얼굴을 그린다.
void TalkeWindow_Tool::m_createSayFace() {

	//말하는 대상의 얼굴을 만든다.
	if (!NOW_VECTTALLK.SayObjectTexture.empty()) {
		auto ParentLayout
			= (Layout*)m_talkBackLayout->getChildByTag(FACELAYOUT_TAG);
		std::string EmotionFileName = NOW_VECTTALLK.SayObjectTexture;
		switch (NOW_VECTTALLK.SayObjectEmotion)
		{
		case NoEmotion:
			EmotionFileName.append("_Nomal");
			break;
		case Angry:
			EmotionFileName.append("_Angry");
			break;
		case Sad:
			EmotionFileName.append("_Sad");
			break;
		case Happy:
			EmotionFileName.append("_Happy");
			break;
		case Doubt:
			EmotionFileName.append("_Doubt");
			break;
		default:
			break;
		}
		EmotionFileName.append(".png");
		auto FaceImage = ImageView::create(EmotionFileName.c_str());
		ParentLayout->addChild(FaceImage);
		FaceImage->ignoreContentAdaptWithSize(false);
		Size FaceSize = Size(m_talkBackLayout->getContentSize().height
			, m_talkBackLayout->getContentSize().height);
		if (NOW_VECTTALLK.OnButton)
			FaceSize = FaceSize * 0.8f;
		FaceImage->setContentSize(FaceSize);
	}
}
//대화내용을 만든다.
void TalkeWindow_Tool::m_createTalkText() {

	//받은 대화 내용에 타이핑 이미지를 입혀놓는 작업
	CCLOG(u8"talk Text : %s", NOW_VECTTALLK.TalkText.c_str());
	/*
	string의 문장중에서 [event:Num] 찾아 알맞는 이벤트를 적용하고 해당 문자를 삭제한다.
	문자열 자체를 변경해놓고 Label을 만들고 원하는 위치에 이벤트 값들을 저장해놓는다.
	*/
	auto ParentLayout = m_talkBackLayout->getChildByTag(FACELAYOUT_TAG);
	Size OutputTalkSize = m_talkBackLayout->getContentSize();
	
	int WordSegment = 0;
	
	//어절의 이벤트 값들
	//처음을 제외한 다음번 vector값들은 문자열이 줄어든(erase)를 한 다음의 문자열이다.
	std::vector<SegmentEvent> WorlEvent;

	std::u32string m_32Text;
	StringUtils::UTF8ToUTF32(NOW_VECTTALLK.TalkText, m_32Text);
	//한글화? 해주는 함수
	setlocale(LC_ALL, "");
	for (int i = 0; i < m_32Text.size(); i++) {
		//화이트 스페이스의 경우 투명화나 이벤트를 넣어놓으면 버그가 생긴다.
		CCLOG(u8"Word %c",m_32Text.at(i));
		if (isspace(m_32Text.at(i))) {
			CCLOG("TalkString is space");
			WordSegment = i+1;
			continue;
		}
		//Text에서 [size:2]같은 이벤트 설정값들을 설정하는 함수
		if (m_32Text.at(i) == '[') {
			std::string EventKindText;
			std::string EventNumText;
			SegmentEvent PushSegment;
			PushSegment.FirstSegment = WordSegment;
			PushSegment.EndSegment = i-1;
			int FirstNum = i;

			for (++i; m_32Text.at(i) != ':'; i++)
				EventKindText.push_back(m_32Text.at(i));
			for (++i; m_32Text.at(i) != ']'; i++)
				EventNumText.push_back(m_32Text.at(i));

			if (EventKindText.compare("Delay") == 0)
				PushSegment.EventKind = WordSegmentEvent::DelayEvent;
			if (EventKindText.compare("Scale") == 0)
				PushSegment.EventKind = WordSegmentEvent::ScaleEvent;
			if (EventKindText.compare("Speed") == 0)
				PushSegment.EventKind = WordSegmentEvent::SpeedEvent;
			
			PushSegment.EventVlaue = atof(EventNumText.c_str());
			
			int EndNum = i;
			//erase는 First 부터 Last만큼 제거한다.
			m_32Text.erase(FirstNum, EndNum - FirstNum+1);
			i = FirstNum - 1;

			WorlEvent.push_back(PushSegment);
		}
	}

	StringUtils::UTF32ToUTF8( m_32Text, NOW_VECTTALLK.TalkText);

	float FontSize = 0.15f;
	for (auto i = WorlEvent.begin(); i < WorlEvent.end(); i++)
		if ((*i).EventKind == WordSegmentEvent::ScaleEvent) {
			FontSize *= (*i).EventVlaue;
			break;
		}
	//대화내용을 만든다.
	m_outputTalk = ui::Text::create(NOW_VECTTALLK.TalkText, FONT_NAME
		, OutputTalkSize.height*FontSize);
	ParentLayout->addChild(m_outputTalk);

	if (NOW_VECTTALLK.OnButton) {
		OutputTalkSize.height = OutputTalkSize.height* 0.8f;
	}
	if (NOW_VECTTALLK.SayObjectTexture.size()>0){
		OutputTalkSize.width = OutputTalkSize.width - OutputTalkSize.height;
	}
	
	//m_outputTalk->ignoreContentAdaptWithSize(false);
	m_outputTalk->setTextAreaSize(Size(OutputTalkSize.width, OutputTalkSize.height * 0.8f));
	//글자간의 간격을 조정하는 함수
	//((Label*)m_outputTalk->getVirtualRenderer())->setAdditionalKerning(20.0f);
	LinearLayoutParameter* LineTalk = LinearLayoutParameter::create();
	m_outputTalk->setLayoutParameter(LineTalk);
	LineTalk->setGravity(LinearGravity::CENTER_VERTICAL);

	m_createTextEvent(WorlEvent);
}

//이벤트 대화내용을 만든다.
void TalkeWindow_Tool::m_createTextEvent(std::vector<SegmentEvent> SegEvent) {

	float DTime = 0.0f;
	int WordSegment = 0;
	auto SegmentBegin = SegEvent.begin();
	bool OnSegmentEvent = false;
	
	StringUtils::UTF8ToUTF32(NOW_VECTTALLK.TalkText, m_32Text);
	//한글화? 해주는 함수
	setlocale(LC_ALL, "");
	for (int i = 0; i < m_32Text.size(); i++) {
		float ADDDT = 0.2f;
		if (m_32Text.at(i) == '\0')
			break;
		//화이트 스페이스의 경우 투명화나 이벤트를 넣어놓으면 버그가 생긴다.
		if (isspace(m_32Text.at(i))) {
			CCLOG("TalkString is space");
			continue;
		}
		if (SegmentBegin == SegEvent.end());
		//Text에서 [size:2]같은 이벤트 설정값들을 설정하는 함수
		else if (i == (*SegmentBegin).FirstSegment || OnSegmentEvent) {
			OnSegmentEvent = true;
			switch ((*SegmentBegin).EventKind)
			{
			case WordSegmentEvent::DelayEvent:
				if (i == (*SegmentBegin).EndSegment)
				ADDDT = ADDDT *(*SegmentBegin).EventVlaue;
				break;
			case WordSegmentEvent::ScaleEvent:
				break;
			case WordSegmentEvent::SpeedEvent:
				ADDDT = ADDDT *(*SegmentBegin).EventVlaue;
				break;
			default:
				break;
			}
			if (i == (*SegmentBegin).EndSegment) {
				OnSegmentEvent = false;
				if (SegmentBegin+1 != SegEvent.end())
					SegmentBegin++;
			}
		}

		//투명도를 0으로 만든다.
		m_outputTalk->getLetter(i)->setOpacity(0.0f);

		DTime += ADDDT;
		//투명도를 255로 설정한다.
		CallFunc* FadeAction = CallFunc::create(std::bind(&TalkeWindow_Tool::m_opacityAction, this, i));
		Sequence* OpacityAction = Sequence::create(DelayTime::create(DTime), FadeAction, NULL);
		m_outputTalk->getLetter(i)->runAction(OpacityAction);
		CCLOG(u8"Label.at(%d).at(%d) : %c \n Time(%f)", m_vecTextNum
			, i, m_32Text.at(i), DTime);
	}
}

//이벤트 대화내용을 만든다.
void TalkeWindow_Tool::m_settingTextEvent(const char* TextKind, int TextNum) {

}

//글자의 모습을 보이게 하는 함수
void TalkeWindow_Tool::m_opacityAction(int StringNum) {
	m_outputTalk->getLetter(StringNum)->setOpacity(255.0f);
	cocos2d::experimental::AudioEngine::setVolume(
	cocos2d::experimental::AudioEngine::play2d(TALK_SOUND)
		,Game_Player::getInstance()->getGameSetting()->m_action_Sound*VOLUME_PS);

	if (m_32Text.size() -1 <= StringNum) {
		CCLOG("endText");
		m_textEnd = true;
	}

}
//talkwidnow제거
void TalkeWindow_Tool::m_removeOutputTalk() {
	Game_Player::getInstance()->setSceneInterface(SceneInterface::TileScene);
	cocos2d::experimental::AudioEngine::uncache(TALK_SOUND);

	m_vecTextNum = 0;
	m_clickButtonNum = 0;
	m_textEnd = false;

	m_talkBackLayout->removeFromParent();

	//글자 효과 지우기
	m_WordEffect.clear();
	//보상 주기
	m_rewardEvent();
	TileMap_Tool::getInstens()->nextTileEvent();
}

//선택 버튼에 이벤트를 설정한다.
void TalkeWindow_Tool::m_setEventButton(int actEncho) {

	auto Buttons = m_buttonLayout->getChildren();

	Buttons.at(m_clickButtonNum)->setScale(1.0f);
	m_clickButtonNum += actEncho;

	if (m_clickButtonNum < 0)
		m_clickButtonNum = Buttons.size() - 1;
	else if (m_clickButtonNum >= Buttons.size())
		m_clickButtonNum = 0;

	Buttons.at(m_clickButtonNum)->setScale(1.1f);
}
//Text IsTalk를 true로 전환하고 save한다.
void TalkeWindow_Tool::m_saveTextIsTalk() {

	Clear_Json TalkClear;
	TalkClear.JsonKind = JsonKind::Map_Json;
	TalkClear.JsonFile = GetTileMapList(false,GameMapPlace::Balcony).back();
	TalkClear.JsonNumber.push_back(m_talkingNPC.Unique_Number);
	TalkClear.JsonNumber.push_back(m_nowTalkData.unique_Number);

	Game_Player::getInstance()->addPLClear(TalkClear);
}

//보상을 반환한다.
void TalkeWindow_Tool::m_rewardEvent() {

	Game_Player* Player = Game_Player::getInstance();
	
	
	//이벤트 정보일경우 이벤트를 실행한다.
	if (m_eventNumber > 0)
		m_talkEvent.getTalkEvent(m_eventNumber);
	//이미 보상을 받았는지 확인한다.
	else if (m_nowTalkData.is_Talk) {
		return;
	}
	
	//세이브
	m_saveTextIsTalk();

	//보상
	
	std::vector<Game_Object> RewardObjects;
	
	//보상을 종류에 따라 나눠서준다.
	for (auto i = m_nowTalkData.reward_Item.begin();
		i < m_nowTalkData.reward_Item.end(); i++) {
		if ((*i).ItemNumber == NULL)
			break;
		Player->addPLItem((*i).Kind, (*i).ItemNumber);
		switch ((*i).Kind)
		{
		case GameObjectKind::Cube_ExeFile:
			RewardObjects.push_back(Player->getHavBusiness().back());
			break;
		case GameObjectKind::ObjectItem:
			RewardObjects.push_back(Player->getHavItem().back());
			break;
		case GameObjectKind::ComputerIcon:
			RewardObjects.push_back(Player->getHavIcon().back());
			break;
		default:
			break;
		}
	}
	if (RewardObjects.size() > 0)
		AddItemBox(RewardObjects);
}
