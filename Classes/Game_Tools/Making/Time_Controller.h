#pragma once

#include "Game_Data\Game_Specimen_List.h"
#include "Game_Data\Test_Schedule.h"

class Time_Controller {
public:
	

	//흐르는 시간 함수
	void ScheduleTime(float dt);

	//지금 시간을 출력한다.
	Time getNowTime() { return m_oneDayTime; };
private:

	//60진법(1시간 재는 함수)변경 함수
	void m_60th_notation();

	//하루를 재시작하는 함수
	void m_restarteDay();

private:
	//게임 하루의 시간
	Time m_oneDayTime = { 9,30 };

public:


	static Time_Controller* createTimeController() {
		if (m_controller == nullptr)
			m_controller = new Time_Controller;
		return m_controller;
	}
private:
	static Time_Controller* m_controller;
	
	Time_Controller() {};
	~Time_Controller() {};
};