#pragma once
#include <ui\CocosGUI.h>


#include "Game_Library\G_ConstLibrary\PL_DataWindow_CCollection.h" 
#include "Game_Data\Player_Data.h"
#include "Game_Tools\Manager\LanguageManager.h"

enum InformationScene{ 
	ItemWindow = 0, OptionWindow = 1,OperationButton,ItemInforWindow,MusicWinodw };

class InformationWindow
{
public:
	
	//플레이어 정보창 받기
	Layout* getInformationWindow();

	//키보드로 아이템 화면의 마우스를 이동시키는것함수
	void OnPreesedDataWindow(EventKeyboard::KeyCode kcode);

	void m_setBackGroundMusic(std::string MusicName);

private:

	//----------------------------제작 함수----------------------------

	//기본적인 레이아웃을 만든다.
	void m_createBackGround();
	//바탕화면 아이콘과 마우스 생성
	void m_createIconAndMouse(const char* TagName);
	
	//플레이어 소개창
	//void m_openIntroduceWindow();
//아이템창을 만든다.
	void m_createItemWindow();
	
	//----------------------------화면열기----------------------------
	
	//플레이어 아이템창
	void m_openItemWindow();

	//플레이어 MP3창
	void m_openMusicWindow();

	//플레이어의 mp3에 있는 모든 음악을 나열한곳
	void m_openAllMusicListWindow(MusicKind musicKind);

	//플레이어 옵션
	void m_openOptionWindow();

	//아이템 정보창과 선택창 만들기
	void m_oepnItemInformationWindow();

	//----------------------------삭제함수----------------------------

	//기반 이미지 삭제와 상속된것들삭제
	void m_removeImageChild();

	//----------------------------씬 변환함수----------------------------

	//point를가지고 열 창을 설정하는 함수
	void m_changeWindow(Ref* ref);

	void m_backWindow();

	//----------------------------이벤트 함수----------------------------

	//사운드 조절Slideui이벤트 
	void m_music_SliderEvent(Ref *pSender, Slider::EventType type);

	//풀스크린 이벤트
	void m_fullScreen_CallBack(Ref *pSender);
	//뒤로가기 이벤트
	void m_back_Callback();

	//음악을 실행하거나 멈추는 함수
	void m_musicPlayButtonEvent(Ref* ref);

	//음악을 실행하거나 멈추는 함수
	void m_moveMusicPointButtonEvent(Ref* ref,int Encho);

private:
	//기반 레이어
	Layout* m_backGroundLayer;

	//옵션 레이어
	Layout* m_otionBackLayout;

	//음악 배경 레이아웃
	Layout* m_musicBackLayout;

	//마우스 아이콘 (count를 하나올려서 없어지지 않게한다.)
	Sprite* m_pointIcon;

	Size m_iconSize;

	//플레이어 데이터
	Game_Player* m_playerData = Game_Player::getInstance();

	int m_backMusicID = -1;

	//현재 클릭하려는 아이콘 번호
	int m_iconXCount = 0;	
	
	int m_iconYCount = 0;
	//현재 윈도우의 위치
	int m_windowTag = 0;
	
	//현재 mp3음악의 번호
	int m_mp3MusicNum = -1;
	
	//지금 배경상황
	InformationScene m_informationScene = InformationScene::ItemWindow;
};
//enum Rank_List의 값들을 char* 로 변경한다.
std::string RankChangeText(Rank_List EnumRank);
