#include "Time_Controller.h"
#include <cocos2d.h>

Time_Controller* Time_Controller::m_controller = nullptr;


//흐르는 시간 함수
void Time_Controller::ScheduleTime(float dt) {
	
	//정해진 시간 마다 1분씩 늘어난다.
	++m_oneDayTime.Minutes;

	m_60th_notation();

	//하루가 끝이 났는지 확인한다.
	if (m_oneDayTime.Hour >= 18 && m_oneDayTime.Minutes >= 30) {
		//재시작
		m_restarteDay();
	}

	CCLOG("Time( %d : %d )", m_oneDayTime.Hour, m_oneDayTime.Minutes);

}

//60진법(1시간 재는 함수)변경 함수
void Time_Controller::m_60th_notation() {
	while (m_oneDayTime.Minutes >= 60){
		m_oneDayTime.Minutes -= 60;
		m_oneDayTime.Hour += 1;
	}
}

//하루를 재시작하는 함수
void Time_Controller::m_restarteDay(){
	m_oneDayTime.Hour = 9;
	m_oneDayTime.Minutes = 30;
}