#pragma once
#include <cocos2d.h>
#include <ui/CocosGUI.h>

#include "Game_Library\G_ConstLibrary\TalkWindow_CCollction.h"
#include "Game_Data\Game_TextSpecimen_List.h"
#include "Game_Data\Player_Data.h"
#include "Game_Tools\Manager\LanguageManager.h"

using namespace cocos2d;
using namespace cocos2d::ui;

//대화 방식 Npc의 다음 Text의 다음 텍스트의 조건을 충족할수없을때까지 찾는 방식이다.
class TalkeWindow_Tool{
public:
	//대화창을 만드는 함수 
	Layout* getbackTalkWindow(
		int NPC_Num, std::string NPC_File ="\0",int EventNum = 0);

	void TalkWindowKeyEvent(EventKeyboard::KeyCode kCode);

protected:

	//------------------------------생성------------------------------

	//talkwidnow뒤창을 만든다.
	bool m_createTalkWindow();
	//선택버튼을 만드는
	void m_createChoiceButton();	
	
	//말하고있는 대상의 얼굴을 그린다.
	void m_createSayFace();
	//대화내용을 만든다.
	void m_createTalkText();

	//------------------------------재생성------------------------------

	//대화내용을 재 설정한다.
	void m_resetTextBox();

	//------------------------------제거------------------------------

	//지금 출력된 대화들을 지운다.
	void m_removeOutputTalk();

	//------------------------------이벤트------------------------------

	//이벤트 대화내용을 만든다.
	void m_createTextEvent(std::vector<SegmentEvent> SegmentEve);

	//이벤트 대화내용을 만든다.
	void m_settingTextEvent(const char* TextKind, int TextNum);

	//문자의 투명도를 255로 설정한다.
	void m_opacityAction(int StringNum);

	//보상을 반환한다.
	void m_rewardEvent();
	//----------------------------Keyboard Event----------------------------

	//선택 버튼에 이벤트를 설정한다.
	void m_setEventButton(int actEncho);
	//Text IsTalk를 true로 전환하고 save한다.
	void m_saveTextIsTalk();
protected:
	//대화중인 NPC
	Game_NPC m_talkingNPC;

	//지금 출력된 대화내용
	ui::Text* m_outputTalk;

	//대화 단어들의 액션값
	std::vector<WordEffect> m_WordEffect;

	//--------------------------Layout--------------------------

	//talkwindow의 가장 기반이 되는 Layout
	Layout* m_talkBackLayout;

	//선택버튼들의 Layout
	Layout* m_buttonLayout;

	//--------------------------대화 내용정보--------------------------

	//현재 대화 내용
	Game_Text m_nowTalkData;

	//현재 대화진행상태
	int m_vecTextNum = 0;

	//현재 텍스트의 크기
	std::u32string m_32Text;

	//현재 클릭된버튼
	int m_clickButtonNum = 0;

	//텍스트가 모두 출력되었는지 확인한다.
	bool m_textEnd = false;

	//--------------------------이벤트 정보--------------------------

	//대화이후에 실행할 이벤트들
	Game_Event m_talkEvent;

	//이 함수가 이벤트 함수인지 체크한다.
	int m_eventNumber = 0;
};

//------------------------------서브함수------------------------------
