#include "Test_TexterWindow.h"

TestTextureWindow* TestTextureWindow::TextureWindow = nullptr;

void TestTextureWindow::getTextureWindow(std::vector<Game_Char> TalkChars) {
	m_talkChar = TalkChars;
	for (auto i = TalkChars.begin(); i < TalkChars.end(); i++){
		(*i).Char_Body->addChild(m_createBubble((*i).Char_Body->getContentSize()));
	}
}

//대화할 주제를 정한다.
void TestTextureWindow::getTextureNpcWindow(std::vector<Npc_Char> TalkChars) {

	/*
	어떤 대화를 받았는지 확인하고 그에 맞는 대화를 전달하자

	부딪힌 사람이 그 사람과 하는 대화를 가지고있는다.

	대화의 끝을 어떻게 해야할지 고민이다.
	그리고 대화가 통하는 사람과 안통하는 사람의 기준을 나누고 
	호감도로도 또 나눈다.
	*/
	//먼저 말을 꺼낸사람
	auto FirstTell = TalkChars.at(0);
	std::string	TexturFileName = "Game_Data/";
	
	for (auto i = FirstTell.MemorySighting.begin(); 
		i != FirstTell.MemorySighting.end(); i++){
		for (auto k = TalkChars.begin() + 1;k != TalkChars.end(); i++) {
			if ((*i).Who != (CharKind)(*k).Unique_Number)
				continue;
			switch ((*i).What)
			{
			case SmallTalk:
				switch ((*i).With)
				{
				case SmallTalk:

					break;
				case Bringing:
					break;
				case Delivery:
					break;
				case CompanyWork:
					break;
				default:
					break;
				}

				break;
			case Bringing:
				break;
			case Delivery:
				break;
			case CompanyWork:
				break;
			default:
				break;
			}
		}
	}
	

	CharKind FirstCharKind = (CharKind)FirstTell.Unique_Number;
	switch (FirstCharKind)
	{
	case NOChar:
		break;
	case Npc1:
		break;
	case Npc2:
		break;
	default:
		break;
	}
}

Sprite* TestTextureWindow::m_createBubble(Size BubbleSize) {
	TextureBubble.push_back(Sprite::create(WHITEBLOCK_PNG));
	TextureBubble.back()->setPosition(Vec2(BubbleSize.width, BubbleSize.height));
	TextureBubble.back()->setContentSize(BubbleSize);
	TextureBubble.back()->setAnchorPoint(Vec2::ANCHOR_BOTTOM_LEFT);
	return TextureBubble.back();
}

//말풍선안에 말을 만들기
Label* TestTextureWindow::m_createTexture(int NpcNum,Size BubbleSize) {
	float LetterSize = BubbleSize.width*0.5f;

	for (auto i = m_talkChar.begin(); i < m_talkChar.end(); i++){
		
	}
	return Label::create();
}

void TestTextureWindow::m_createWindow() {

}


