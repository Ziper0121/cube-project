#pragma once
#include <ui/CocosGUI.h>
#include <json\encodedstream.h>


#include "Game_Data\Player_Data.h"
#include "Game_Library\AllEffect.h" 
#include "Game_Tools\Making\Npc_Making.h"
#include "Game_Tools\Making\PL_DataWindow_Making.h" 
#include "Game_Tools\Making\TalkeWindow_Making.h" 
#include "Game_Tools\Computer\Modify_Computer.h"
#include "Game_Tools\Making\Test_TexterWindow.h"
#include "Game_Tools\Making\Time_Controller.h"

using namespace cocos2d;
using namespace cocos2d::ui;
class TileMap_Tool
{
private:
	TileMap_Tool() {m_createTool();};
	~TileMap_Tool() {};

	static TileMap_Tool* m_tileTool;
public:

	static TileMap_Tool* getInstens() {
		if (m_tileTool == nullptr) {
			m_tileTool = new TileMap_Tool();
		}
		return m_tileTool;
	};

	Node* getTileMapLayer();
	//해체
	/*
	Director::getInstance()->getEventDispatcher()
	->removeEventListenersForType(EventListener::Type::KEYBOARD);
	*/

	//다음 이벤트를 실행시킨다.
	void nextTileEvent();

	//키보드 이벤트
	void charMovePressed(EventKeyboard::KeyCode kcode);
	void charMoveReleased(EventKeyboard::KeyCode kcode);

protected:

	//--------------------------event함수--------------------------
	void m_keyboardPressed(EventKeyboard::KeyCode kcode, Event* et);
	void m_keyboardReleased(EventKeyboard::KeyCode kcode, Event* et);

	//--------------------------생성 함수--------------------------
	void m_createTool();

	//플레이어에 만드는 함수
	void m_createPlayer();

	//플레이어의 초기 위치
	void m_settingViewPoint();

	//모든 타일맵을 다 불러 와놓자
	void m_createAllTileMap();
	//타일맵 생성 함수
	void m_createTileMap();
	//출력할 타일맵을 선택한다.
	const char* m_settingTileMap();

	//타일의 npc들을 만든다.
	void m_createNPC();
	//------------------------움직임에 관한함수들------------------------
	
	//모든 움직임을 실행시키는 함수
	void m_player_AllMove(EventKeyboard::KeyCode kcod);
	
	//움직임을 만들고 재생하는 함수 --CallBack함수이기도 하다
	void m_player_MoveByAct();
	
	// 충돌채크 함수
	bool m_check_TileObject();

	//Npc를 체크한다
	bool m_check_NpcObject(Point PlayerPoint);

	//움직일때 움직일 방향을 보는 함수
	Rect m_playerEncho_Texture(int encho = 0);
	//------------------------------------------

	//------------------------맵 조작------------------------
	
	//캐릭터 이동시 뷰 포인트 이동
	Node* m_moveCharMoveView(Point position);

	//------------------------블럭 이벤트------------------------

	//인벤토리 생성
	void m_PL_InterfaceEvent();

	//맵을 이동
	void m_portallEvent(cocos2d::ValueMap ObName);
	
	//토크창만들기
	void m_talkwindowEvent(cocos2d::ValueMap ObName);

	//cube게임 생성
	void m_computerEvent();

	//플레이어의 데이터를 저장한다.
	void m_charSaveEvent();
	
	//플레이어의 이벤트 타일
	void m_charTalkEvent(int Num);

	//saveEvent
	void m_saveKeyboradPressed();
	//--------------------------씬연계하는 함수--------------------------

	//타일에 따른 이벤트를 실행하는 함수
	void m_checkTileEvent(cocos2d::ValueMap);

	//---------------------------------
	//------------사용 않함------------
	//TileMap이 Window화면보다 클경우 화면을 센터에 배치해주는 함수 
	//void setViewpointCenter(Point position); 
	//캐릭터가 움직이는 스케줄
	void keyPress_Char_Walking(float dt);
	//window사이즈를 tilemap(1,1)포인트로 바꿔주는 함수 
	Point m_tileCoordForPosition(Point position);
	//TileMap의 블럭과 충돌처리를 해주는 함수
	bool m_check_ContactObject(Point psition);
	//맵 이동시 뷰 포인트 이동
	Node* setViewpointCenter(Point position){}

protected:

	//테스트용 게임 타임
	Time_Controller* m_gameTime;

	//게임데이터
	Game_Player* m_playerData;

	//npc데이터
	NpcController* m_npcData;
	//--------------------------현재 상태를 나타낸 변수--------------------------
	//타일맵 툴의 Layer
	Node* m_tileMapLayer;

	//현재 진행중인 이벤트 타일value값
	cocos2d::ValueMap m_nowTileValue;
	//-----------------------------맵 사이즈 관련-----------------------------
	//프로젝트의 스케일 사이즈
	float m_winScale;
	//윈도위 사이즈
	Size m_winSize;
	//타일 스케일
	float m_tileScale = 4.0f;

	float m_texturScale = 2.0f;
	//---------------------------TileMap툴의 라이아웃---------------------------	
	//플레이어의 현재 상태(움직이는 상태, 대화하는 상태 등등)을 확인하는 함수
	unsigned char m_playerEvent;

	//-----------------------------맵에 관한 변수-----------------------------
	//타일 맵 정보
	TMXTiledMap* m_tmap;
	TMXObjectGroup * m_objects;
	TMXLayer* m_metainfo;

	//맵의 위치(json파일이름)
	GameMapPlace m_tmapScene;

	//맵크기가 윈도우보다 클경우
	bool m_fixTmapX;
	bool m_fixTmapY;

	//플레이어 몸
	Sprite* m_player;
	//애니메이션 이미지
	Texture2D* m_texture;

	//-----------------------------움직임 변수-----------------------------
	//플레이어의 움직일 위치
	Vec2 m_player_MoveEncho;

	//현재 위치의 포인트(openGL 좌표계의 포인트 좌측 '하단'이 Vec2(0,0))	
	Point m_playerTilePoint;

	//플레이어의 지속적인 이동
	Sequence* m_repeat_WalkAction;
	Spawn* m_test_Spawn;

	//방향키 관한 변수
	int m_ani_Encho;
	bool m_ani_turn;
	//플레이어 좌표
	Vec2 m_player_StartPosition;

	//arrow배열에 빈 공산(Key_None)이 있는 위치
	int m_emptyArrow = 0;

	//-----------------------------키보드 값-----------------------------
	//플레이어 눌린 키보드 값
	EventKeyboard::KeyCode m_pressedArrow[4];

	//-----------------------------외부 클래스-----------------------------
	//컴퓨터 창
	Computer_Tool m_computerTool;

	//대화창
	TalkeWindow_Tool m_talkeWindow;
	
	//정보창
	InformationWindow m_inforWindow;
};
