
#include "Game_Library\G_ConstLibrary\MainMenu_CCollection.h" 
#include "Game_Library\KeyboardMap.h"
#include "Game_Library\Game_FileRoute.h"
#include "Game_Data\Player_Data.h"
#include "Game_Project\MainMenu_Screen.h" 

/*
 파일 불러오는 설정창
 마우스 커서 변경
*/

// 메인 UI 생성 : Menu Layeout으로 바꾸자
MainMenu_Layer::MainMenu_Layer() {
	this->setName("MainMenu_Layer");

	CCLOG("Main Menu Layer");
	//윈도우 창 사이즈

	Size WinSize = Game_Player::getInstance()->getGameSetting()->m_window_Size;
	CCLOG("Main Menu %f %f", WinSize.width, WinSize.height);
	//레이아웃을 조절할 레이아웃 생성
	Layout* BackGorund_layout = Layout::create();
	BackGorund_layout->setLayoutType(Layout::Type::RELATIVE);
	BackGorund_layout->setBackGroundColorType(Layout::BackGroundColorType::SOLID);
	BackGorund_layout->setBackGroundColor(Color3B::GRAY);
	BackGorund_layout->setContentSize(WinSize);
	BackGorund_layout->setAnchorPoint(Vec2::ANCHOR_BOTTOM_LEFT);
	addChild(BackGorund_layout, BASE_LAYOUT);


	Layout* MainMenu_layout = Layout::create();
	MainMenu_layout->setLayoutType(Layout::Type::VERTICAL);
	BackGorund_layout->addChild(MainMenu_layout, BASE_LAYOUT);

	RelativeLayoutParameter* BackGround_LayoutParameter = RelativeLayoutParameter::create();
	BackGround_LayoutParameter->setAlign(RelativeAlign::CENTER_IN_PARENT);
	MainMenu_layout->setLayoutParameter(BackGround_LayoutParameter);

	//메인화면의 인터페이스를 설정한다.
	LinearLayoutParameter* MainMenu_LayoutParameter = LinearLayoutParameter::create();
	MainMenu_LayoutParameter->setGravity(LinearGravity::CENTER_HORIZONTAL);
	MainMenu_LayoutParameter->setMargin(Margin(0.0f, 0.0f, 0.0f, 20.0f));

	//Textui 기본 설정 
	Text* MenuNew_Text = Text::create("New", FONT_NAME, 20.0f);

	const char* TextString[4] = {u8"새로시작",u8"이어하기",u8"설정",u8"종료" };
	std::vector<std::function<void(Ref*)>> TextFunction = {
		std::bind(&MainMenu_Layer::m_new_Callback, this, std::placeholders::_1)
	,std::bind(&MainMenu_Layer::m_continue_Callback, this, std::placeholders::_1) 
	,std::bind(&MainMenu_Layer::m_option_Callback, this, std::placeholders::_1) 
	,std::bind(&MainMenu_Layer::m_End_Callback, this, std::placeholders::_1) };
	
	std::vector<Text*> MenuTextUI;
	for (size_t i = 0; i < TextFunction.size(); i++){
		MenuTextUI.push_back(Text::create(TextString[i], FONT_NAME, 20.0f));
		MainMenu_layout->addChild(MenuTextUI.back());
		MenuTextUI.back()->addClickEventListener(TextFunction.at(i));
		MenuTextUI.back()->setTouchEnabled(true);
		MenuTextUI.back()->setTouchScaleChangeEnabled(true);
		MenuTextUI.back()->setLayoutParameter(MainMenu_LayoutParameter);
	}
	
	if (Game_Player::getInstance()->m_loardPlayerData()) {
		MenuTextUI.at(1)->setTouchEnabled(true);
		MenuTextUI.at(1)->setTouchScaleChangeEnabled(true);
	}
	
}
// 화면 이동 이벤트 
void MainMenu_Layer::m_new_Callback(Ref* ref) {
	Game_Player::getInstance()->replayerSetting();
	Director::getInstance()->replaceScene(GamePlay_Layer::createScene());
}
// 화면 이동 이벤트 
void MainMenu_Layer::m_continue_Callback(Ref* ref) {
	//게임 화면으로 변경한다. 
	Director::getInstance()->replaceScene(GamePlay_Layer::createScene());
}
void MainMenu_Layer::m_option_Callback(Ref* ref) {
	//옵션 설정으로 들어간다.
	static_cast<LayerMultiplex*>(_parent)->switchTo(1);
}
void MainMenu_Layer::m_End_Callback(Ref* ref) {
	Director::getInstance()->end();
}

// 옵션 UI 생성 : Layout
Option_Layer::Option_Layer() {
	CCLOG("Option Menu Layer");
	/*
	볼륨 배경, 액션, uion
	윈도우 사이즈와 풀스크린 on\off
	키설정
	뒤로가기
	*/

	//옵션 UI이름들설정들
	std::vector<std::string> OptionUIName = {
		"BackGround Music",
		"Action Sound",
		"UI Sound",
		"Full/off",
		"Back"
	};
	std::vector<std::string> SliderName = {
		"BackGround Music",
		"Action Sound",
		"UI Sound"
	};
	std::vector<std::string> BigSizeUIName = {
		"Full/off",
		"Back"
	};
	std::vector<int> SliderPersent = {
		Game_Player::getInstance()->getGameSetting()->m_background_Music,
		Game_Player::getInstance()->getGameSetting()->m_action_Sound,
		Game_Player::getInstance()->getGameSetting()->m_ui_Sound
	};

	this->setName("OptionLayer");

	Size WinSize = Game_Player::getInstance()->getGameSetting()->m_window_Size;
	CCLOG("Option win %f %f", WinSize.width, WinSize.height);

	// ui정리 용도 Layout 생성.
	m_OptionBackLayout = Layout::create();
	m_OptionBackLayout->setLayoutType(Layout::Type::VERTICAL);
	m_OptionBackLayout->setBackGroundColorType(Layout::BackGroundColorType::SOLID);
	m_OptionBackLayout->setBackGroundColor(Color3B::GRAY);
	m_OptionBackLayout->setContentSize(WinSize);
	m_OptionBackLayout->setAnchorPoint(Vec2::ANCHOR_BOTTOM_LEFT);
	addChild(m_OptionBackLayout, BASE_LAYOUT);
	//UI의 Sldier Event들

	//UI의 Click Event들
	std::vector<std::function<void(Ref* ref)>> UiClick_Event = {
		std::bind(&Option_Layer::m_fullScreen_CallBack, this, std::placeholders::_1),
		std::bind(&Option_Layer::m_back_Callback, this, std::placeholders::_1)
	};

	//layout item 위치 설정 
	LinearLayoutParameter* OptionMainName_Margin = LinearLayoutParameter::create();
	OptionMainName_Margin->setGravity(LinearGravity::CENTER_HORIZONTAL);
	//퍼센트적으로 해야한다.
	OptionMainName_Margin->setMargin(ui::Margin(0.0f, 10.0f, 0.0f, 10.0f));

	std::vector<Text*>OptionText;
	//UI 제작  i 전체 ui 갯수, SliderNum slider 이벤트, clicknum click이벤트
	for (int i = 0, SliderNum = 0, ClickNum = 0; i < OptionUIName.size(); i++) {
		//ui text 생성

		OptionText.push_back(Text::create(OptionUIName[i], FONT_NAME, 15.0f));
		OptionText.back()->setTag(i);
		m_OptionBackLayout->addChild(OptionText.back(), LAYOUT_TEXT);
		OptionText.back()->setLayoutParameter(OptionMainName_Margin);
		if (ClickNum < BigSizeUIName.size()) {
			if (OptionUIName.at(i).compare(BigSizeUIName.at(ClickNum)) == 0) {
				OptionText.back()->setFontSize(25.0f);
				OptionText.back()->addClickEventListener(UiClick_Event.at(ClickNum));
				OptionText.back()->setTouchEnabled(true);
				OptionText.back()->setTouchScaleChangeEnabled(true);
				ClickNum++;
				continue;
			}
		}
		
		if (SliderNum < SliderName.size()) {
			if (OptionUIName.at(i).compare(SliderName.at(SliderNum)) == 0) {
				//layout에 slider생성
				Slider* slider = Slider::create(SLIDER_BAR_PNG, SLIDE_THUMB_PNG);
				slider->setTag(SLIDER_ADDTAG + i);
				slider->setScale(1.0f);
				slider->addEventListener(
					std::bind(&Option_Layer::m_sliderSound_Event
						, this, std::placeholders::_1, std::placeholders::_2));
				m_OptionBackLayout->addChild(slider, LAYOUT_ITEM);
				//Slider Layout 
				slider->setLayoutParameter(OptionMainName_Margin);
				slider->setPercent(SOUND_MAX);
				SliderNum++;
				continue;
			}
		}
	}
}
	
// 음악크기 조절 이벤트 
void Option_Layer::m_sliderSound_Event(Ref *pSender, Slider::EventType type){
	Slider* slider = dynamic_cast<Slider*>(pSender);
	int percent = slider->getPercent();
	//text slider에 조절
	Text* OptionText = (Text*)m_OptionBackLayout->
		getChildByTag(slider->getTag() - SLIDER_ADDTAG);
	if (type == Slider::EventType::ON_PERCENTAGE_CHANGED)
	{
		int maxPercent = slider->getMaxPercent();
		OptionText->
			setString(StringUtils::format("%d", 100 * percent / maxPercent));
	}
	if (type == Slider::EventType::ON_SLIDEBALL_UP) {
		switch (OptionText->getTag())
		{
		case 0:
			OptionText->setString("Background Music");
			m_gameSetting_Data->m_background_Music = percent;
			break;
		case 1:
			OptionText->setString("Action Sound");
			m_gameSetting_Data->m_action_Sound = percent;
			break;
		case 2:
			OptionText->setString("UI Sound");
			m_gameSetting_Data->m_ui_Sound = percent;
			break;
		default:
			break;
		}
	}
}
//풀스크린
void Option_Layer::m_fullScreen_CallBack(Ref* pSender){
	Text* text = (Text*)pSender;

	Size WindowSize = m_gameSetting_Data->m_window_Size;
	m_gameSetting_Data->m_fullScreen = !m_gameSetting_Data->m_fullScreen;

	GLViewImpl* view = (GLViewImpl*)Director::getInstance()->getOpenGLView();
	if(m_gameSetting_Data->m_fullScreen){
		text->setString("Full/on");
		//view->setWindowed(WindowSize.width*2.0f, WindowSize.height*2.0f);
		//view->setDesignResolutionSize(
		//	WindowSize.width*2.0f, WindowSize.height*2.0f
		//	, ResolutionPolicy::SHOW_ALL);
		//_parent->setScale(2.0f);
		view->setFullscreen();
		CCLOG("ViewSIze %f %f", view->getFrameSize().width, view->getFrameSize().height);
		CCLOG("option Parent name : %s", _parent->getName().c_str());
	}
	else {
		text->setString("Full/off");
		view->setWindowed(WindowSize.width, WindowSize.height);
		view->setDesignResolutionSize(
			WindowSize.width, WindowSize.height, ResolutionPolicy::SHOW_ALL);
	}
	Game_Player::getInstance()->getGameSetting()->m_save_GameData();
}

// 화면 이동 이벤트 
void Option_Layer::m_back_Callback(Ref* pSender) {
	m_gameSetting_Data->m_save_GameData();
	static_cast<LayerMultiplex*>(_parent)->switchTo(0);
}

//메인 씬 시작
MainMenu_Scene* MainMenu_Scene::createScene() {
	//Scene 전달
	auto returnscene = MainMenu_Scene::create();
	CCLOG("returnscene");
	return returnscene;
}

//메인 메뉴 멀티 메뉴로 layer들을 이동시킨다.
bool MainMenu_Scene::init() {
	if (!Scene::init()) {
		return false;
	}
	setName("MainScene");
	//게임캐릭터 인터페이스 정보 초기화
	Game_Player::getInstance();

	CCLOG("WindowSize(%3.5f,%3.5f)", Director::getInstance()->getWinSize().width, Director::getInstance()->getWinSize().height);
	CCLOG("Main Menu Sceme");

	//추가할 멀티레이어 nothrow는 new가 실패일시 NULL을 반환한다.
	auto MainMenu = new (std::nothrow) MainMenu_Layer();
	auto Option = new (std::nothrow) Option_Layer();

	//멀티 레이어 생성 switchTo()함수로 이동가능
	auto Mainlayer = LayerMultiplex::create( MainMenu, Option, nullptr);
	Mainlayer->setName("Multiplex");
	addChild(Mainlayer, MULTI_LAYER);
	auto Game_PlayerData = Game_Player::getInstance()->getGameSetting();
	Mainlayer->setAnchorPoint(Vec2::ANCHOR_BOTTOM_LEFT);

	//해체 해준다.
	MainMenu->release();
	Option->release();
	//인터페이스를 조정하는 기능을 제어하는 체크박스 생성
	//m_tools_CheckBoxCreate();

	//인터페이스를 변경할수있게 해주는 이벤트
	auto UIClcik_Event= EventListenerTouchOneByOne::create();
	UIClcik_Event->onTouchBegan = 
		std::bind(&MainMenu_Scene::Click_EventListner, this, std::placeholders::_1, std::placeholders::_2);
	UIClcik_Event->onTouchMoved = 
		std::bind(&MainMenu_Scene::Move_EventListner, this, std::placeholders::_1, std::placeholders::_2);
	UIClcik_Event->onTouchEnded = [&](Touch* tch, Event* et) { m_click_on = false; };
	_eventDispatcher->addEventListenerWithSceneGraphPriority(UIClcik_Event,this);
//main meun Layer이동
	return true;
}

void MainMenu_Scene::m_tools_CheckBoxCreate() {
	
	//설정 툴을 끄고키는 check박스
	CheckBox* toolsCheck = CheckBox::create(CHECKBOX_NORMAL_PNG, CHECKBOX_ACT_PNG);
	toolsCheck->setPosition(Vec2(0.0f, 0.0f));
	toolsCheck->setAnchorPoint(Vec2(0.0f,0.0f));
	toolsCheck->setSelectedState(m_tools_Switch);
	toolsCheck->setScale(4.0f);
	toolsCheck->setColor(Color3B(250.0f, 0.0f, 0.0f));
	addChild(toolsCheck, TOOLS_CHECK);

	toolsCheck->addClickEventListener([&](Ref* ref) {
		if ((m_tools_Switch = !m_tools_Switch)) {
			m_tools_Click_ChildsSetting();
		}
		else {
			for (int i = 0; i < m_tools_Targets.size(); i++) {
				//Widget* widget = (Widget*)m_tools_Targets.at(i);
				//widget->setEnabled(true);
			}
		}
	});
}
void MainMenu_Scene::m_tools_Click_ChildsSetting() {
	cocos2d::Vector<Node*> target_Child = getChildren();

	CCLOG("CHILD SIZE %d", target_Child.size());
	for (int ChildNum = 0; ChildNum < target_Child.size();)
	{
		CCLOG("%d ZORDER(%d)", ChildNum, target_Child.at(ChildNum)->getZOrder());
		switch (target_Child.at(ChildNum)->getZOrder())
		{
			//다음 지금 시작되고있는 layer다.
		case MULTI_LAYER:
			CCLOG("IN MULTI_LAYER");
			target_Child = target_Child.at(ChildNum)->getChildren();
			target_Child = target_Child.at(0)->getChildren();
			ChildNum = 0;
			continue;
			//Layoutobject나 MenuItem를 가지고 있는 함수이다.
		case BASE_LAYOUT:
			CCLOG("IN BASE_LAYOUT");
			target_Child = target_Child.at(ChildNum)->getChildren();
			ChildNum = 0;
			continue;
		case LAYOUT_ITEM: //LAYOUT_TEXT
			CCLOG("IN LAYOUT_ITEM");
			//현재 layer에 ui들 저장
			m_tools_Targets = target_Child;
			for (int i = 0; i < m_tools_Targets.size(); i++) {
				//Widget* widget = (Widget*)m_tools_Targets.at(i);
				//widget->setEnabled(false);
			}
			return;
		default:
			CCLOG("OUT SWITCH");
			break;
		}

		ChildNum++;
	}
}

//마우스 이벤트
bool MainMenu_Scene::Click_EventListner(Touch* tch, Event* et) {
	if (!m_tools_Switch) {
		return false;
	}
	Point Click_Point = tch->getLocationInView();
	Click_Point.y = Director::getInstance()->getWinSize().height - Click_Point.y;

	CCLOG("Click_Point %f %f", Click_Point.x, Click_Point.y);

	for (int i = 0; i < m_tools_Targets.size(); i++) {
		
		Rect Target_Rect = Rect(m_tools_Targets.at(i)->getPosition()
			,m_tools_Targets.at(i)->getContentSize());
		CCLOG("Target_Rect (%f, %f, %f, %f)"
			, Target_Rect.getMinX(), Target_Rect.getMinY(), Target_Rect.getMaxX(), Target_Rect.getMaxY());

		if (Target_Rect.containsPoint(Click_Point)) {
			CCLOG("onMouse Down");
			m_tools_ClickNode = m_tools_Targets.at(i);
			m_click_on = true;
		}
	}
	return true;
}
void MainMenu_Scene::Move_EventListner(Touch* tch, Event* et) {
	if (!m_tools_Switch || m_tools_ClickNode == NULL) {
		return ;
	}
	// 지금 좌표와 이전좌표차를 빼서 이동할거리를 잰다.
	Point Click_Point = tch->getLocationInView() - tch->getPreviousLocationInView();
	//좌표가 반대로 되있기 때문에 마이너스를 해준다.
	Click_Point.y *= -1;
	Click_Point += m_tools_ClickNode->getPosition();

	if (m_click_on) {
		m_tools_ClickNode->setPosition(Click_Point);
		CCLOG("Move Position(%5.5f,%5.5f)", Click_Point.x, Click_Point.y);
		CCLOG("m_tools_ClickNode Position(%5.5f,%5.5f)", m_tools_ClickNode->getPositionX(), m_tools_ClickNode->getPositionY());
	}
	return;
}

Scene* GamePlay_Layer::createScene() {
	GamePlay_Layer* GamePlayer = GamePlay_Layer::create();
	Scene* scene = Scene::create();
	scene->addChild(GamePlayer);
	return scene;
}

bool GamePlay_Layer::init() {

	if (!Layer::init()) {
		return false;
	}
	m_playerData = Game_Player::getInstance();
	auto m_tileMap = TileMap_Tool::getInstens();
	addChild(m_tileMap->getTileMapLayer());
	return true;
}

