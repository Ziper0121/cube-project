#pragma once

#include <ui/CocosGUI.h>

#include "Game_Tools\Making\Npc_Making.h" 
#include "Game_Tools\Making\TileMap_Making.h" 
#include "Game_Tools\Computer\Modify_Cube.h" 
#include "Game_Data\Player_Data.h"

using namespace cocos2d;
using namespace cocos2d::ui;

//메인 메뉴를 전달하는 클레스
class MainMenu_Scene : public Scene {
public :
	static MainMenu_Scene* createScene();

	virtual bool init();

	CREATE_FUNC(MainMenu_Scene);

	bool Click_EventListner(Touch* tch, Event* et);
	void Move_EventListner(Touch* tch, Event* et);

protected:

	cocos2d::Vector<Node*> m_tools_Targets;
	Node* m_tools_ClickNode;

	//현재 레이어의 ui들을 찾는 함수
	void m_tools_Click_ChildsSetting();
	//인터페이스를 설정을 할수있는 기능의 체크박스 생성
	void m_tools_CheckBoxCreate();


	bool m_tools_Switch = false;
	bool m_click_on = false;
};

class MainMenu_Layer : public Layer {
public:
	MainMenu_Layer();
	~MainMenu_Layer(){}

public:

	void m_new_Callback(Ref* ref);
	void m_continue_Callback(Ref* ref);
	void m_option_Callback(Ref* ref);
	void m_End_Callback(Ref* ref);
};

class Option_Layer : public Layer {
public:

	Option_Layer();
	~Option_Layer() {}
public:
	/*
	볼륨 배경, 액션, ui(보류)
	윈도우 사이즈와 풀스크린 on\off
	키설정 이동키와 상호작용키와 큐브키
	뒤로가기
	*/
	void m_sliderSound_Event(Ref *pSender, Slider::EventType type);
	
	void m_fullScreen_CallBack(Ref* pSender);
		  
	void m_back_Callback(Ref* pSender);
protected:

	//기반이되는 레이어
	Layout* m_OptionBackLayout;

	Game_Interface* m_gameSetting_Data = Game_Player::getInstance()->getGameSetting();

	//사용처 불문?
	int m_key_Change_Collback;
};
class GamePlay_Layer : public Layer {
public:
	static Scene* createScene();

	virtual bool init();

	CREATE_FUNC(GamePlay_Layer);

private:

	Game_Player* m_playerData;

	//현재 씬의 tile map
};