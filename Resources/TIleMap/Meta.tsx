<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.2" tiledversion="1.2.0" name="Meta" tilewidth="32" tileheight="32" spacing="1" margin="1" tilecount="2" columns="2">
 <image source="meta_tiles.png" trans="ffffff" width="67" height="34"/>
 <tile id="0">
  <properties>
   <property name="Tile" type="int" value="1"/>
  </properties>
 </tile>
 <tile id="1">
  <properties>
   <property name="Tile" type="int" value="2"/>
  </properties>
 </tile>
</tileset>
